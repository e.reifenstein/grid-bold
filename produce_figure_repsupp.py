#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 15:54:15 2021

@author: eric
"""
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import special,stats,interpolate
import matplotlib.patches as mpatches
from matplotlib.ticker import ScalarFormatter
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

#from scipy.integrate import odeint

def grid(X, Y, grsc=30, angle=0, offs=np.array([0,0]), rect=False):
    sc = 1/grsc * 2./np.sqrt(3)
    if rect:
        rec = lambda x: np.where(x>0, x, 0)
        if np.size(offs) == 2:
            return rec(np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos(angle*np.pi/180)*(Y-grsc*offs[1]))) * rec(np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y-grsc*offs[1]))) * rec(np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y-grsc*offs[1])))
        else:
            assert len(offs[0]) == len(offs[1]), "ox and oy must have same length"
            return rec(
                np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos(angle*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   rec(
                np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   rec(
                np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))
    else:
        if np.size(offs) == 2:
            return (1 + np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos(angle*np.pi/180)*(Y-grsc*offs[1]))) * (1 + np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y-grsc*offs[1]))) * (1 + np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y-grsc*offs[1])))
        else:
            assert len(offs[0]) == len(offs[1]), "ox and oy must have same length"
            return (1 +
                np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos(angle*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   (1 +
                np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   (1 +
                np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))

def convert_to_rhombus(x,y):
    return x+0.5*y,np.sqrt(3)/2*y

def convert_to_square(xr,yr):
    return xr-1/np.sqrt(3)*yr,2/np.sqrt(3)*yr

def map_to_rhombus(x,y,grsc=30):
    xr,yr = x-1/np.sqrt(3)*y,2/np.sqrt(3)*y
    xr, yr = np.mod(xr,grsc),np.mod(yr,grsc)
    xmod,ymod = xr+0.5*yr,np.sqrt(3)/2*yr
    return xmod,ymod

def traj(dt,tmax,sp=2,dphi = 0.5, bound = 3,pwlin=False):
    t = np.arange(0,tmax+dt,dt)
    x,y,direc = np.zeros((3,len(t)))
    olddir = np.pi/4
    for i in range(1,len(t)):
        sigma_theta = dphi;
        newdir = olddir + sigma_theta*np.sqrt(dt)*np.random.randn()
        dx = sp*dt*np.cos(newdir)
        dy = sp*dt*np.sin(newdir)
        x[i] = x[i-1] + dx
        y[i] = y[i-1] + dy
        olddir = np.arctan2(dy,dx)
        direc[i] = olddir

    if pwlin:
        dt2 = 0.1
        t2 = np.arange(0,t[-1]+dt2,dt2)
        fx = interpolate.interp1d(t, x)
        fy = interpolate.interp1d(t, y)
        x2 = fx(t2)
        y2 = fy(t2)
        direc2 = np.concatenate((np.zeros(1),np.repeat(direc[1:],int(dt/dt2))))
        return x2,y2,direc2,t2
    else:
        return x,y,direc,t

def adap_euler(s,tt,tau,w):
    '''
    Euler-solve a simple adaptation model for the repetition-suppression hypothesis
    '''
    v = np.zeros(np.shape(tt))
    a = np.zeros(np.shape(tt))
    v[0] = s[0]
    a[0] = 0
    dt = np.mean(np.diff(tt))
    for it in range(len(v)-1):
        #dv = (-w*a[it]+s[it]-v[it])/0.01*dt
        v[it+1] = max(s[it] - w*a[it],0)
        da = (v[it]-a[it])/tau*dt
        a[it+1] = a[it] + da
    """
    plt.figure()
    plt.plot(tt,s,tt,v)
    plt.legend(['No rep supp','Rep supp'])
    plt.xlabel('Time (s)')
    plt.ylabel('Normalized firing rate')
    """
    return np.where(v>0,v,0) #rectify result

def gridpop(ox, oy, phbins, trajec, Amax = 20, grsc = 30, repsuppcell = False, repsupppop = False, tau_rep = 3.5, w_rep = 50, conj = False, propconj = 0.66, kappa = 50, jitter=10,shufforient=False):
    """
    simulate a population of grid cells for a complex 2d trajectory
    """
    if type(ox)==int or type(ox)==float:
        N = 1
    else:
        N = len(ox)
    x,y,direc,t = trajec
    oxr,oyr = convert_to_rhombus(ox,oy)
    oxr,oyr = np.asarray(oxr), np.asarray(oyr)
    fr = np.zeros(np.shape(x))
    for n in range(N):
        if shufforient:
            ang = 360*np.random.rand()
            if repsuppcell:
                fr += adap_euler(Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n])), t, tau_rep, w_rep)
            elif conj: # not used together at this stage
                if n<= int(propconj*N):
                    mu = np.mod(ang + jitter*np.random.randn(),360) #np.mod(np.random.randint(0,6,1) * 60 + jitter*np.random.randn(), 360)
                    fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n])) * np.exp(kappa*np.cos(direc-np.pi/180.*mu))/(2*np.pi*special.i0(kappa))
                    # scheint nicht ok, immer noch normiert?
                else:
                    fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n]))
            else:
                fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n]))

        else:
            if repsuppcell:
                fr += adap_euler(Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n])), t, tau_rep, w_rep)
            elif conj: # not used together at this stage
                if n<= int(propconj*N):
                    mu = np.mod(np.random.randint(0,6,1) * 60 + jitter*np.random.randn(), 360)
                    fr += Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n])) * np.exp(kappa*np.cos(direc-np.pi/180.*mu))/(2*np.pi*special.i0(kappa))
                    # scheint nicht ok, immer noch normiert?
                else:
                    fr += Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n]))
            else:
                fr += Amax*grid(x,y,grsc=grsc,angle=0, offs=(oxr[n],oyr[n]))

    if repsupppop:
        fr = adap_euler(fr, t, tau_rep, w_rep)

    if False:
        plt.figure()
        plt.scatter(x,y,s = 20, c = fr, edgecolors='face')

    #direc = np.mod(direc,2*np.pi)
    direc_binned = np.linspace(-np.pi,np.pi,phbins+1)
    fr_mean = np.zeros(phbins)
    for id in range(len(direc_binned)-1):
            ind = (direc>=direc_binned[id])*(direc<direc_binned[id+1])
            fr_mean[id] = np.sum(fr[ind])*phbins/len(fr)

    if False:
        plt.figure()
        plt.plot(180./np.pi*(direc_binned[:-1]+direc_binned[1:])/2., fr_mean)
        plt.xlabel('Running direction (deg)')
        plt.ylabel('Firing rate (a.u.)')

    return fr_mean, np.abs(np.sum(fr * np.exp(-6j*direc)))/np.size(direc), np.abs(np.sum(np.exp(-6j*direc)))/np.size(direc)

def gridpop_meanfr(rmax = 3, Amax = 20, bins = 500, phbins = 360, mode = 'randi', N = 100, path = 'linear', dphi = 0.1, clsize = 0.25, meanoff = (0, 0), conj = False, propconj = 1, kappa = 50., jitter = 0, kappacl = 10, repsuppcell = False, repsupppop = False, tau_rep = 1., w_rep = 1., speed = 10., plotres = False):

    if mode == 'clust':
        ox,oy = np.mod(np.linspace(meanoff[0]-clsize/2.,meanoff[0]+clsize/2.,N),1), np.mod(np.linspace(meanoff[1]-clsize/2.,meanoff[1]+clsize/2.,N),1)
        # just a linear stretch of phases, use meshgrid to get a small rhombus
    elif mode == 'clustcirc':
        # sample N cells from a circle of radius clsize around meanoff
        r = clsize/2.*np.sqrt(np.random.rand(N))
        phi = 2*np.pi*np.random.rand(N)
        ox,oy = np.mod(meanoff[0] + r*np.cos(phi),1), np.mod(meanoff[1] + r*np.sin(phi),1)
    elif mode == 'clustvm':
        ox, oy = np.random.vonmises(2*np.pi*(meanoff[0]-0.5), kappacl, N)/2./np.pi + 0.5, np.random.vonmises(2*np.pi*(meanoff[1]-0.5), kappacl, N)/2./np.pi + 0.5
    elif mode == 'uniform':
        ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
        ox = ox.reshape(1,-1)[0]
        oy = oy.reshape(1,-1)[0]
        N = int(np.sqrt(N))**2
    elif mode == 'randi':
        ox,oy = np.random.rand(N), np.random.rand(N)
    else:
        ox,oy = np.array([meanoff[0]]), np.array([meanoff[1]])

    oxr,oyr = convert_to_rhombus(ox,oy)
    if path == 'linear':
        if N==1:
            r,phi = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins))
            X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)
            grids = Amax * grid(X,Y,offs=(oxr,oyr))
            grids2 = grids.copy()
            """
            plt.figure()
            slope = np.tan(np.pi/3)
            for yy in np.arange(-300,301,np.sqrt(3)/2*30):
                plt.plot([-300,300],[yy,yy],'k')
            for xx in np.arange(-500,501,30):
                plt.plot([-2/np.sqrt(3)*300+xx,2/np.sqrt(3)*300+xx],[slope*-2/np.sqrt(3)*300,slope*2/np.sqrt(3)*300],'k')
            plt.scatter(X,Y,zorder=3,s=1)
            plt.axis([-rmax,rmax,-rmax,rmax])
            plt.xlabel('x (cm)')
            plt.ylabel('y (cm)')
            """

            # how does meanfr depend on bins ... Naomi showed that the different peaks in meanfr seem to have different heights for higher number of bins??
        else:
            r,phi,indoff = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins), np.arange(len(ox)))
            X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)

            # calculate integrated firing rate as a function of the movement angle
            grids = Amax * grid(X,Y,offs=(oxr,oyr))[:,:,0,:]
            grids2 = grids.copy()

        if conj: # add head-direction tuning if wanted
            Nconj = int(propconj*N)
            #mu = np.random.randint(0,6,Nconj) * 60 # a multiple of 60deg
            mu = np.mod(np.random.randint(0,6,Nconj) * 60 + jitter*np.random.randn(Nconj), 360)
            vonmi = np.exp(kappa*np.cos(np.pi/180*(np.linspace(0,360,phbins)[:,None]-mu[None,:])))/(2*np.pi*special.i0(kappa))
            grids[:,:,:Nconj] = grids[:,:,:Nconj]*vonmi[:,None,:] # change the first Nconj cells

        if repsuppcell:
            #speed = 1. # grid scale/s
            #tau_rep = 1. #s
            #w_rep = 3.
            #s = grids[0,:,0]

            tt = np.linspace(0,rmax/speed,bins)
            #aa = np.zeros(np.shape(grids))
            for idir in range(phbins):
                for ic in range(N):
                    v = adap_euler(grids[idir,:,ic],tt,tau_rep,w_rep)
                    grids[idir,:,ic] = v
                    #aa[idir,:,ic] = a
            """
            plt.figure()
            plt.subplot(2,1,1)
            plt.plot(np.linspace(0,rmax/speed,bins),grids2[30,:,0])
            plt.plot(np.linspace(0,rmax/speed,bins),grids[30,:,0])
            plt.plot(np.linspace(0,rmax/speed,bins),aa[30,:,0])
            plt.title('30 deg')

            plt.subplot(2,1,2)
            plt.plot(np.linspace(0,rmax/speed,bins),grids2[60,:,0])
            plt.plot(np.linspace(0,rmax/speed,bins),grids[60,:,0])
            plt.plot(np.linspace(0,rmax/speed,bins),aa[60,:,0])
            plt.xlabel('Time (s)')
            plt.ylabel('Normalized firing rate')
            plt.title('60 deg')
            """
        if repsupppop:
            tt = np.linspace(0,rmax/speed,bins)
            meanpop_base = np.sum(grids,axis=2)
            meanpop = np.sum(grids,axis=2)
            for idir in range(phbins):
                v = adap_euler(meanpop[idir,:],tt,tau_rep,w_rep)
                meanpop[idir,:] = v
            meanfr = np.sum(meanpop, axis=1)/bins
            meanfr_base = np.sum(meanpop_base, axis=1)/bins
            meanfr = meanfr/np.mean(meanfr)*np.mean(meanfr_base)

        else:
            if N==1:
                meanfr_base = np.sum(grids2,axis=1)/bins
                meanfr = np.sum(grids,axis=1)/bins
                meanfr = meanfr/np.mean(meanfr)*np.mean(meanfr_base)
            else:
                meanfr_base = np.sum(np.sum(grids2,axis=1)/bins,axis=1)
                meanfr = np.sum(np.sum(grids,axis=1)/bins,axis=1)
                meanfr = meanfr/np.mean(meanfr)*np.mean(meanfr_base)
        gr2 = np.sum(grids, axis=2)
        gr60 = np.abs(np.sum(gr2 * np.exp(-6j*phi[:, :, 0])))/(np.shape(phi)[0]*np.shape(phi)[1])
        gr60_path = np.abs(np.sum(np.exp(-6j*phi[:, :, 0])))/(np.shape(phi)[0]*np.shape(phi)[1])
    else: # random walk trajectory
        tmax = 3600
        dt = 0.1
        trajec = traj(dt,tmax,sp=speed,dphi = 5)
        if repsuppcell or repsupppop:
            meanfr_base, gr60_base = gridpop(oxr,oyr,phbins,trajec,repsuppcell=False,repsupppop=False,tau_rep=tau_rep,w_rep=w_rep, conj = conj, propconj=propconj, kappa=kappa, jitter=jitter)
            meanfr, gr60 = gridpop(oxr,oyr,phbins,trajec,repsuppcell=repsuppcell,repsupppop=repsupppop,tau_rep=tau_rep,w_rep=w_rep, conj = conj, propconj=propconj, kappa=kappa, jitter=jitter)
            meanfr = meanfr/np.mean(meanfr)*np.mean(meanfr_base)
        else:
            meanfr, gr60, gr60_path = gridpop(oxr,oyr,phbins,trajec,repsuppcell=repsuppcell,repsupppop=repsupppop,tau_rep=tau_rep,w_rep=w_rep, conj = conj, propconj=propconj, kappa=kappa, jitter=jitter)

    # detect 60deg periodicity, use DC as baseline
    # fr2 = np.tile(meanfr,3) # tile to account for periodicity
    # power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
    # ff = np.linspace(0,1./(2.), 3*phbins//2)

    #plt.figure()
    #plt.plot(ff,power)
    #plt.xticks(np.linspace(0,ff[-1],10), [str(1./ii) for ii in np.linspace(0,ff[-1],10)])

    # gr60 = np.abs(np.sum(fr * np.exp(-6j*direc)))/np.abs(np.sum(np.exp(-6j*direc)))
    # gr60 = power[np.argmin(abs(ff-1./60*360/phbins))] / np.mean(meanfr) # pick index which is closest to 60deg
    # gr90 = power[np.argmin(abs(ff-1./90*360/phbins))]
    # ph = np.angle(np.fft.fft(fr2))[:len(fr2)//2]
    # p60 = ph[np.argmin(abs(ff-1./60*360/phbins))]/2/np.pi*60
    # p90 = ph[np.argmin(abs(ff-1./90*360/phbins))]/2/np.pi*90

    #gr60_rel = gr60 / power[0]
    #gr60_alt = meanfr[60]/mean(meanfr) # should be similar, but more local measure

    # plot example
    if plotres:
        plt.figure(figsize=(6,10))
        plt.subplot(3,1,1)
        plt.scatter(ox+0.5*oy,np.sqrt(3)/2*oy)
        plt.plot([0,1],[0,0],'k--')
        plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
        plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
        plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
        plt.xlim([-0.05,1.5])
        plt.ylim([-0.05,1])
        plt.xlabel('Grid phases')

        ax = plt.subplot(3,1,2)
        y_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(y_formatter)
        plt.plot(np.linspace(0,360,phbins),meanfr)
        plt.xticks(np.arange(0,361,60))
        plt.xlabel('Direction of movement (deg)')
        plt.ylabel('Integrated population firing rate (spk/s)')
        plt.subplot(3,1,3)
        plt.plot(ff,power)
        plt.arrow(1./60,max(power),0,-max(power)/2,head_length=max(power)/5)
        plt.xlim([0,0.1])
        plt.xlabel('Frequency (1/deg)')
        plt.ylabel('Power')
        plt.xticks([0,1/120,1/60,1/30,1/20,1/10],['0','1/120','1/60','1/30','1/20','1/10'])
        titlestr = ''
        if mode == 'clust':
            titlestr += '%i cells, clustered in square' %N
        elif mode == 'clustcirc':
            titlestr += '%i cells, clustered in a circle' %N
        elif mode == 'clustvm':
            titlestr += '%i cells, vm clustered' %N
        elif mode == 'uniform':
            titlestr += '%i cells, uniformly distributed' %N
        elif mode == 'randi':
            titlestr += '%i cells, randomly distributed' %N
        else:
            titlestr += '1 grid cell'

        if conj:
            titlestr += '; %d' %(propconj*100) + r'$\%$' + ' conjunctive'

        titlestr += '\n H = %f' %gr60
        plt.suptitle(titlestr)

    return gr60, gr60_path, gr60, gr60#, gr90, p60, p90

N = int(1e2)
phbins = 360
w_rep = 1.
taus = np.linspace(0.1, 10, 10)
gr60s_tau = np.zeros(np.shape(taus))
# for it, tau in enumerate(taus):
#     print(it)
#     gr60s_tau[it], _, _, _ = gridpop_meanfr(rmax = 300, mode = 'uniform', path = 'linear', N = N, conj=False, repsuppcell = True, tau_rep = tau, w_rep = 1., speed = 10., plotres = False)

# control60, _, _, _ = gridpop_meanfr(rmax = 300, mode = 'uniform', path='linear', N = N, conj=False, repsuppcell = False, speed = 10. , plotres = False)

ws = np.linspace(0, 1, 10)
gr60s_ws = np.zeros(np.shape(ws))
# for iw, w_rep in enumerate(ws):
#     print(iw)
#     gr60s_ws[iw], _, _, _ = gridpop_meanfr(rmax = 300, mode = 'uniform', path='linear', N = N, conj=False, repsuppcell = True, tau_rep = 5., w_rep = w_rep, speed = 10., plotres = False)

# control60_ws, _, _, _ = gridpop_meanfr(rmax = 300, mode = 'uniform', path='linear', N = N, conj=False, repsuppcell = False, speed = 10. , plotres = False)

fs = 16
Amax = 20
plt.figure(figsize=(20,8))
ax = plt.subplot(3,5,1)
X,Y = np.meshgrid(np.linspace(-50,50,1000),np.linspace(-50,50,1000))
gr = Amax*grid(X,Y,grsc=30, angle=0, offs=np.array([0,0]))
plt.pcolor(X,Y,gr)#,vmin=0,vmax=20)
c = plt.colorbar()#ticks=[0,10,20])
c.set_label('Firing rate (spk/s)',fontsize=fs)
c.ax.tick_params(labelsize=fs)
plt.arrow(0,0,1.5*15,1.5*np.sqrt(3)/2*30,color='b',length_includes_head=True,head_width = 2)
plt.arrow(0,0,1.5*np.sin(np.pi/3)*30,1.5*np.cos(np.pi/3)*30,color='r',length_includes_head=True,head_width = 2)
ax.set_aspect('equal')
plt.xlabel('x',fontsize=fs)
plt.ylabel('y',fontsize=fs)
plt.xticks([])
plt.yticks([])

tau_rep = 5.
w_rep = 1.
speed = 10
bins = 1000
rmax = 300
ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
ox = ox.reshape(1,-1)[0]
oy = oy.reshape(1,-1)[0]
oxr, oyr = convert_to_rhombus(ox,oy)
r,phi,indoff = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins), np.arange(len(ox)))
X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)
grids = Amax * grid(X,Y,offs=(oxr,oyr))[:, :, 0, :]
grids2 = grids.copy()
tt = np.linspace(0,rmax/speed,bins)
for idir in range(phbins):
    for ic in range(N):
        v = adap_euler(grids[idir, :, ic],tt,tau_rep,w_rep)
        grids[idir, :, ic] = v

plt.subplot(3, 5, (2,3))
plt.plot(tt,grids2[30,:,0],'r-')
plt.plot(tt,grids[30,:,0],'r--')
plt.plot(tt,grids2[60,:,0],'b-')
plt.plot(tt,grids[60,:,0],'b--')
plt.xlabel('Time (s)', fontsize=fs)
#plt.ylabel('Firing rate (spk/s)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
plt.xlim([0,17])

ax2 = plt.subplot(3, 5, (4,5))
meanfr = np.sum(np.sum(grids,axis=1)/bins,axis=1)
gr2 = np.sum(grids, axis=2)
gr60 = np.abs(np.sum(gr2 * np.exp(-6j*phi[:, :, 0])))/np.size(gr2)

plt.plot(np.linspace(0,360,phbins),meanfr,'k')
plt.xticks([0,60,120,180,240,300,360],[])
plt.yticks(fontsize=fs)
plt.ylabel('Total firing rate (spk/s)',fontsize=fs)
ax3 = inset_axes(ax2, width='20%', height='30%', loc=4)
plt.plot([0,0],[-1,1],'k')
plt.plot([-1,1],[0,0],'k')
plt.plot([-1/np.sqrt(2),1/np.sqrt(2)],[-1/np.sqrt(2),1/np.sqrt(2)],'k')
plt.plot([-1/np.sqrt(2),1/np.sqrt(2)],[1/np.sqrt(2),-1/np.sqrt(2)],'k')
ax3.set_aspect('equal')
plt.xticks([0])
plt.yticks([0])
plt.axis([-1.25,1.25,-1.25,1.25])
ax3.xaxis.set_ticks_position("top")
plt.text(0.51,0.1,'H = '+str(np.round(gr60,1)),fontsize=fs,transform=ax2.transAxes)

ax_pw = plt.subplot(3, 5, (9, 10))

#mpl.use('Qt5Agg') # turn on GUI for plotting
#plt.figure()
phbins = 360 # maybe less bins here? how many values per bin?
Amax = 20
tmax = 72000
dt = 9
trajec = traj(dt, tmax, sp=8, dphi=2, pwlin=True)
meanfr3, gr60, gr60_path = gridpop(oxr, oyr, phbins, trajec, Amax = 20, grsc = 30, repsuppcell = True, repsupppop = False, tau_rep = 3.5, w_rep = 1, conj = False)
plt.plot(np.linspace(0, 360, phbins), meanfr3, 'k')
plt.xlabel('Movement direction (deg)', fontsize=fs)
plt.ylabel('Total firing \nrate (spk/s)', fontsize=fs)
plt.xticks([0,60,120,180,240,300,360], fontsize=fs)
plt.yticks(fontsize=fs)
plt.ylim([0,1.4*max(meanfr3)])

part = 1000
ax_in = inset_axes(ax_pw, width='15%', height='40%', loc=1)
plt.plot(trajec[0][:part],trajec[1][:part],'k')
ax_in.set_aspect('equal')
plt.xticks([])
plt.yticks([])

grs_pl = np.array([])
grs_path_pl = np.array([])
i = 0
while i < 100:
    print(i)
    trajec = traj(dt, tmax, sp=8, dphi=2, pwlin=True)
    meanfr3, gr60_pl, gr60_path_pl = gridpop(oxr, oyr, phbins, trajec, Amax = 20, grsc = 30, repsuppcell = True, repsupppop = False, tau_rep = 3.5, w_rep = 1, conj = False)
    grs_pl = np.hstack((grs_pl, gr60_pl))
    grs_path_pl = np.hstack((grs_path_pl, gr60_path_pl))
    i += 1

plt.text(0.5, 0.8, 'H = '+str(np.round(np.median(grs_pl), 1)),fontsize=fs,transform=ax_pw.transAxes)



ax4 = plt.subplot(3, 5, (14, 15))
phbins = 360
tmax = 3600
dt = 0.1
trajec = traj(dt,tmax,sp=10,dphi = 5)
meanfr, gr60, gr60_path = gridpop(oxr,oyr,phbins,trajec,repsuppcell=True,tau_rep=5.,w_rep=1.)
plt.plot(np.linspace(0,360,phbins),meanfr,'k')
plt.xlabel('Movement direction (deg)',fontsize=fs)
plt.ylabel('Total firing rate (spk/s)',fontsize=fs)
plt.xticks([0,60,120,180,240,300,360],fontsize=fs)
plt.yticks(fontsize=fs)
ax5 = inset_axes(ax4, width='20%', height='30%', loc=4)
plt.plot(trajec[0][:100],trajec[1][:100],'k')
ax5.set_aspect('equal')
plt.xticks([])
plt.yticks([])

grs_rw = np.array([])
grs_path_rw = np.array([])
i = 0
while i < 100:
    print(i)
    trajec = traj(dt,tmax,sp=10,dphi = 5)
    meanfr, gr60_rw, gr60_path_rw = gridpop(oxr,oyr,phbins,trajec,repsuppcell=True,tau_rep=5.,w_rep=1.)
    grs_rw = np.hstack((grs_rw, gr60_rw))
    grs_path_rw = np.hstack((grs_path_rw, gr60_path_rw))
    i += 1

plt.text(0.51,0.9,'H = '+str(np.round(np.median(grs_rw), 1)),fontsize=fs,transform=ax4.transAxes)

plt.subplot(3, 5, 11)
plt.plot(taus,gr60s_tau,'k-')
#plt.plot(taus,control60*np.ones(np.shape(taus)),'k--')
plt.xlabel('Adaptation time constant (s)', fontsize=fs)
plt.ylabel('Hexasymmetry', fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
#plt.legend(['Repetition suppression', 'No repetition suppression'], fontsize=fs)

plt.subplot(3, 5, 12)
plt.plot(ws,gr60s_ws,'k-')
plt.xlabel('Adaptation strength', fontsize=fs)
plt.ylabel('Hexasymmetry', fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)

plt.tight_layout()

plt.savefig('Figure4_repsupp.png')
#plt.savefig('Figure4_repsupp.eps', dpi=300)
plt.close()

# add panel about length of runs
# add panel example piecewise linear walk
