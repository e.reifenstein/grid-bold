#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 17:34:36 2019

@author: reifenstein
"""

#%%
import numpy as np
import matplotlib.pyplot as plt
from scipy import special,stats,interpolate
import matplotlib.patches as mpatches
import matplotlib
from matplotlib.ticker import ScalarFormatter
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

#from scipy.integrate import odeint

# make sure grid is correct for other offsets than (0,0) ... something was strange about e.g. j(0.4,0.3)
def grid(X, Y, grsc=30, angle=0, offs=np.array([0,0]), rect=False):
    sc = 1/grsc * 2./np.sqrt(3)
    if rect:
        rec = lambda x: np.where(x>0, x, 0)
        if np.size(offs) == 2:
            return rec(np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos(angle*np.pi/180)*(Y-grsc*offs[1]))) * rec(np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y-grsc*offs[1]))) * rec(np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y-grsc*offs[1])))
        else:
            assert len(offs[0]) == len(offs[1]), "ox and oy must have same length"
            return rec(
                np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos(angle*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   rec(
                np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   rec(
                np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))
    else:
        if np.size(offs) == 2:
            return (1 + np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos(angle*np.pi/180)*(Y-grsc*offs[1]))) * (1 + np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y-grsc*offs[1]))) * (1 + np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y-grsc*offs[1])))
        else:
            assert len(offs[0]) == len(offs[1]), "ox and oy must have same length"
            return (1 +
                np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos(angle*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   (1 +
                np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   (1 +
                np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))

def grid_loc(N,v1,v2,offs=(0,0)):
    n1,n2 = np.meshgrid(np.arange(-N,N+1), np.arange(-N,N+1))
    gridx = n1*v1[0] + n2*v2[0] + offs[0]
    gridy = n1*v1[1] + n2*v2[1] + offs[1]
    return gridx.flatten(),gridy.flatten()

def circmean(vec):
    return np.mod(np.angle( sum(np.exp(1j*vec)) / len(vec)), 2*np.pi)

#%%
# uniformly sample from the 2-dimensional phases
sc = 1
N = 10
v1 = (sc,0)
v2 = (0.5*sc,np.sqrt(3)/2*sc)
noff = 11
n1,n2 = np.meshgrid(np.linspace(0,1,noff), np.linspace(0,1,noff))
offsx = (n1*v1[0] + n2*v2[0]).flatten()
offsy = (n1*v1[1] + n2*v2[1]).flatten()
grx = np.zeros((noff**2, (2*N+1)**2))
gry = np.zeros((noff**2, (2*N+1)**2))
for i in range(noff**2):
    gr = grid_loc(N,v1,v2,offs=(offsx[i],offsy[i]))
    grx[i,:] = gr[0]
    gry[i,:] = gr[1]

#%%
# one cell:
"""
plt.figure()
plt.scatter(grx[0,:],gry[0,:],s=1)
plt.axes().set_aspect('equal')
radius = 10
x = np.arange(-radius,radius+0.01,0.01)
plt.plot(x,np.sqrt(radius**2 - x**2),'k--')
plt.plot(x,-np.sqrt(radius**2 - x**2),'k--')
"""
ab_all = abs(grx + 1j*gry)
ang_all = np.angle(grx + 1j*gry, deg=True)

ab = ab_all[0,:]
ang = ang_all[0,:]
plt.figure()
plt.suptitle('One grid cell at phase (0,0), measurement radius in multiples of grid scale')
plt.subplot(2,2,1)
plt.title('radius = 10')
plt.hist(ang[ab<=10],bins=361)
plt.subplot(2,2,2)
plt.title('radius = 20')
plt.hist(ang[ab<=20],bins=361)
plt.subplot(2,2,3)
plt.title('radius = 40')
plt.hist(ang[ab<=40],bins=361)
plt.xlabel('Angle (deg)')
plt.ylabel('Number of grid peaks')
plt.subplot(2,2,4)
plt.title('radius = 80')
plt.hist(ang[ab<=80],bins=361)

# increase "field width"
disc_radius = 1./10
# translate to angle
a = np.mod(np.array([np.linspace(ang_all[0,i] - 180/np.pi*np.arctan2(disc_radius,ab_all[0,i]), ang_all[0,i] + 180/np.pi*np.arctan2(disc_radius,ab_all[0,i]),1001) for i in range(np.shape(ang_all)[1]) if ab_all[0,i]<40]).flatten(),360) -180
plt.hist(a,bins=721)

# plot disc_radius vs 60 modulation
d = np.arange(0,1,0.01)
modu = np.zeros(len(d))
for ii,disc_radius in enumerate(d):
    a = np.mod(np.array([np.linspace(ang_all[0,i] - 180/np.pi*np.arctan2(disc_radius,ab_all[0,i]), ang_all[0,i] + 180/np.pi*np.arctan2(disc_radius,ab_all[0,i]),1001) for i in range(np.shape(ang_all)[1]) if ab_all[0,i]<40]).flatten(),360) -180
    n,bins = np.histogram(a, bins=3601)
    pow60 = n[abs(bins-60).argmin()]
    mea = np.mean(n)
    modu[ii] = n[abs(bins-60).argmin()]/np.mean(n)

plt.figure()
plt.plot(d,modu)
plt.xlabel('Disc radius (grid scale units)')
plt.ylabel('Modulation strength')

"""
figure()
scatter(grx.flatten(),gry.flatten(),s=1)
radius = 87
x = arange(-radius,radius+0.01,0.01)
plot(x,sqrt(radius**2 - x**2),'k--')
plot(x,-sqrt(radius**2 - x**2),'k--')
"""

# translate grid peaks to polar coordinates and bin the angles
ang = np.angle(grx + 1j*gry, deg=True).flatten()
plt.figure()
plt.suptitle('11x11 grid cells cover phase space uniformly, measurement radius in multiples of grid scale')
plt.subplot(2,1,1)
plt.title('radius = 10')
plt.hist(ang[abs(grx + 1j*gry).flatten()<=10],bins=361)
plt.subplot(2,1,2)
plt.title('radius = 80')
plt.hist(ang[abs(grx + 1j*gry).flatten()<=80],bins=7201)
plt.xlabel('Angle (deg)')
plt.ylabel('Number of grid peaks')


# sample space circularly
rmax = 10
bins = 500
phbins = 361 # be careful when changing this

r,phi,offx,offy = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins), np.linspace(0,1,11), np.linspace(0,1,11))
X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)
# calculate integrated firing rate as a function of the run angle

meanfr = np.mean(grid(X,Y,offs=(offx,offy)),axis=1)

grid60 = 1./3 * (meanfr[0,:,:] + meanfr[60,:,:] + meanfr[120,:,:]) - 1./3 * (meanfr[30,:,:] + meanfr[90,:,:] + meanfr[120,:,:])
grid60_20 = meanfr[60,:,:] - meanfr[20,:,:] # firing rate at 60deg as compared to baseline (chose 40deg)
grid60_30 = meanfr[60,:,:] - meanfr[30,:,:] # fr at 60 minus fr at 30
modulation = meanfr.max(axis=0) - meanfr.min(axis=0)

plt.figure()
plt.subplot(3,3,1)
X,Y = np.meshgrid(np.arange(-2,2.01,0.01),np.arange(-2,2.01,0.01))
plt.imshow(grid(X,Y))
plt.xticks(np.arange(0,401,100), ['-2','-1','0','1','2'])
plt.yticks(np.arange(0,401,100), ['-2','-1','0','1','2'])
plt.subplot(3,3,2)
plt.imshow(modulation.T,origin='lower')
c = plt.colorbar()
c.set_label('modulation depth')
plt.xlabel('Offset in x (fraction of grid scale)')
plt.ylabel('Offset in y (fraction of grid scale)')
plt.xticks(np.arange(0,11,5), ['0','0.5','1'])
plt.yticks(np.arange(0,11,5), ['0','0.5','1'])
plt.subplot(3,3,3)
plt.imshow(grid60.T,origin='lower')
c = plt.colorbar()
c.set_label('60s - 30s')
plt.xlabel('Offset in x (fraction of grid scale)')
plt.ylabel('Offset in y (fraction of grid scale)')
plt.xticks(np.arange(0,11,5), ['0','0.5','1'])
plt.yticks(np.arange(0,11,5), ['0','0.5','1'])
plt.subplot(3,1,2)
plt.plot(np.linspace(0,360,phbins), meanfr[:,0,0])
plt.xlabel('Running direction (deg)')
plt.ylabel('Mean firing rate (a.u.)')
plt.ylim([0,0.3])
plt.subplot(3,1,3)
plt.plot(np.linspace(0,360,phbins), meanfr[:,5,0])
plt.xlabel('Running direction (deg)')
plt.ylabel('Mean firing rate (a.u.)')
plt.ylim([0,0.3])

plt.figure()
plt.imshow(grid60_30)

#%%
# calculate the directional tuning for a set of grid cells, distributed/clustered phases
def convert_to_rhombus(x,y):
    return x+0.5*y,np.sqrt(3)/2*y

def convert_to_square(xr,yr):
    return xr-1/np.sqrt(3)*yr,2/np.sqrt(3)*yr

def map_to_rhombus(x,y,grsc=30):
    xr,yr = x-1/np.sqrt(3)*y,2/np.sqrt(3)*y
    xr, yr = np.mod(xr,grsc),np.mod(yr,grsc)
    xmod,ymod = xr+0.5*yr,np.sqrt(3)/2*yr
    return xmod,ymod

def traj(dt,tmax,sp=2,dphi = 0.5, bound = 3, pwlin=False):
    t = np.arange(0,tmax+dt,dt)
    x,y,direc = np.zeros((3,len(t)))
    olddir = np.pi/4
    for i in range(1,len(t)):
        # Tiziano’s approach with X_t = [x_t,y_t] is
        # dX_t/dt = v [cos (theta_t), sin (theta_t)] with theta_t = sigma_theta * W_t
        # where sigma_theta controls the tortuosity of th
        # trajectory and W_t is a standard Wiener process.
        # I guess that the numerical simulation with a time step Delta_t updates the angle in each time step by
        # theta_[t+Delta_t] = theta_t + sigma_theta * DeltaW where DeltaW is a normally distributed random variable with variance Delta_t. [Yes, “variance Delta_t” even though this sounds odd in terms of units because sigma_theta then has unit degrees*sqrt(time) ]

        #newdir = (1-tort)*olddir + tort * (2*pi * rand() - pi)
        sigma_theta = dphi
        newdir = olddir + sigma_theta*np.sqrt(dt)*np.random.randn()
        dx = sp*dt*np.cos(newdir)
        dy = sp*dt*np.sin(newdir)
        # grenzfaelle analytisch
        # mass aus den experimenten? was ist die baseline (Doeller et al.)?
        # dependence on the geometry of the environment?

        # phase dependence of the cluster hypothesis, anfangsbedingung nicht nur bei 0,0
        # repetition suppression auf populations ebene - done, hat keinen Effekt

        # repetition suppression in daten? linear track?

        # conjunctive hypothesis: addon fuer "jedes feld hat eine lieblingsrichtung" ... wie sind die geclustert?
        # preprint anschauen ... fuer conjunctive cells sind die richtungs-tunings der verschiedenen Felder stark korreliert (Fig. 4)

# =============================================================================
# try different environments here - investigate how gridness compares to case without borders
#         if (x[i-1]+dx)**2 + (y[i-1]+dy)**2 > bound**2:
#             tang = arctan2(y[i-1],x[i-1]) # slope of the tangential on the circle
#             if -pi<newdir<pi:
#                 dif = pi + arctan2(dy,dx)
#             else:
#                 dif = pi - arctan2(dy,dx)
#             dx,dy = cos(dif), sin(dif)
        x[i] = x[i-1] + dx
        y[i] = y[i-1] + dy
        olddir = np.arctan2(dy,dx)
        direc[i] = olddir

    if pwlin:
        dt2 = 0.1
        t2 = np.arange(0,t[-1]+dt2,dt2)
        fx = interpolate.interp1d(t, x)
        fy = interpolate.interp1d(t, y)
        x2 = fx(t2)
        y2 = fy(t2)
        direc2 = np.concatenate((np.zeros(1),np.repeat(direc[1:],int(dt/dt2))))
        return x2,y2,direc2,t2
    else:
        return x,y,direc,t

def traj_control(dt,tmax,offs,draw=False):
    # create grid directions to choose from
    sc = 30
    N = 10
    v1 = (1,0)
    v2 = (0.5,np.sqrt(3)/2)
    offsx,offsy = offs[0],offs[1]
    gr = sc * np.asarray(grid_loc(N,v1,v2,offs=(offsx,offsy)))
    dist = np.sqrt(gr[0]**2 + gr[1]**2)
    grx = gr[0][dist<=np.sqrt(3)/2*sc*N]
    gry = gr[1][dist<=np.sqrt(3)/2*sc*N]
    phi = np.arctan2(grx,gry)

    # create trajectory
    t = np.arange(0,tmax+dt,dt)
    x,y,direc = np.zeros((3,len(t)))
    x[0],y[0] = sc * np.asarray(convert_to_rhombus(offs[0], offs[1]))
    for i in range(1,len(t)):
        ind = np.random.randint(len(phi))
        direc[i] = phi[ind]
        x[i] = grx[ind] + offs[0]
        y[i] = gry[ind] + offs[1]

    if draw:
        plt.figure()
        plt.plot(x,y,'.-',lw=2)
        slope = np.tan(np.pi/3)
        for yy in np.arange(-N*np.sqrt(3)/2*sc,N*np.sqrt(3)/2*sc,np.sqrt(3)/2*sc):
            plt.plot([min(grx),max(grx)],[yy,yy],'k--')
        for xx in np.arange(-N*sc,N*sc,30):
            plt.plot([-2/np.sqrt(3)*max(gry)+xx,2/np.sqrt(3)*max(gry)+xx],[slope*-2/np.sqrt(3)*max(gry),slope*2/np.sqrt(3)*max(gry)],'k--')
        plt.xlabel('x (cm)')
        plt.ylabel('y (cm)')
        plt.axis([min(x),max(x),min(y),max(y)])

    # interpolate
    dt2 = 0.1
    t2 = np.arange(0,t[-1]+dt2,dt2)
    fx = interpolate.interp1d(t, x)
    fy = interpolate.interp1d(t, y)
    x2 = fx(t2)
    y2 = fy(t2)
    direc2 = np.concatenate((np.zeros(1),np.repeat(direc[1:],int(dt/dt2))))
    return x2,y2,direc2,t2


# 10000 cells
#

if False:
    x1,y1,d,t = traj(0.02,10,sp=10,dphi=1)
    x2,y2,d,t = traj(0.02,10,sp=10,dphi=2)
    x3,y3,d,t = traj(0.02,10,sp=10,dphi=3)
    fig1, ax1 = plt.subplots()
    X,Y = np.meshgrid(np.arange(-60,61,1),np.arange(-60,61,1))
    ax1.contourf(X,Y,20*grid(X,Y,sc=1./30*2./np.sqrt(3)))
    ax1.plot(x1,y1,'w')
    ax1.plot(x2+20,y2+20,'w')
    ax1.plot(x3+40,y3+40,'w')
    ax1.set_aspect('equal')
    plt.xlabel('x (cm)')
    plt.ylabel('y (cm)')

    x,y,d,t = traj_control(3,30,offs=(0,0))
    fig1, ax1 = plt.subplots()
    X,Y = np.meshgrid(np.arange(-600,601,1),np.arange(-600,601,1))
    ax1.contourf(X,Y,20*grid(X,Y,grsc=30))
    ax1.plot(x,y,'w')
    ax1.set_aspect('equal')
    plt.xlabel('x (cm)')
    plt.ylabel('y (cm)')


    #plt.plot(np.linspace(-3,3,1000),np.sqrt(9-np.linspace(-3,3,1000)**2))
    #plt.plot(np.linspace(-3,3,1000),-np.sqrt(9-np.linspace(-3,3,1000)**2))
    #plt.axis([-4,4,-4,4])

def adap_euler(s,tt,tau,w):
    '''
    Euler-solve a simple adaptation model for the repetition-suppression hypothesis
    '''
    v = np.zeros(np.shape(tt))
    a = np.zeros(np.shape(tt))
    v[0] = s[0]
    a[0] = 0
    dt = np.mean(np.diff(tt))
    for it in range(len(v)-1):
        #dv = (-w*a[it]+s[it]-v[it])/0.01*dt
        v[it+1] = max(s[it] - w*a[it],0)
        da = (v[it]-a[it])/tau*dt
        a[it+1] = a[it] + da
    """
    plt.figure()
    plt.plot(tt,s,tt,v)
    plt.legend(['No rep supp','Rep supp'])
    plt.xlabel('Time (s)')
    plt.ylabel('Normalized firing rate')
    """
    return np.where(v>0, v, 0) #rectify result

#%%
def gridpop(ox, oy, phbins, trajec, Amax = 1, grsc = 30, repsuppcell = False, repsupppop = False, tau_rep = 3.5, w_rep = 50, conj = False, propconj = 1., kappa = 50, jitter=0, shufforient=False):
    """
    simulate a population of grid cells for a complex 2d trajectory
    DISCUSS A_MAX NEXT FRIDAY
    
    """
    if type(ox)==int or type(ox)==float:
        N = 1
    else:
        N = len(ox)
    x,y,direc,t = trajec
    oxr,oyr = convert_to_rhombus(ox,oy)
    oxr,oyr = np.asarray(oxr), np.asarray(oyr)
    fr = np.zeros(np.shape(x))
    for n in range(N):
        if shufforient:
            ang = 360*np.random.rand()
            if repsuppcell:
                fr += adap_euler(Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n])), t, tau_rep, w_rep)
            elif conj: # not used together at this stage
                if n<= int(propconj*N):
                    mu = np.mod(ang + jitter*np.random.randn(),360) #np.mod(np.random.randint(0,6,1) * 60 + jitter*np.random.randn(), 360)
                    grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n]))
                    fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n])) * np.exp(kappa*np.cos(direc-np.pi/180.*mu))/special.i0(kappa) #(2*np.pi*special.i0(kappa))
                    # scheint nicht ok, immer noch normiert?
                else:
                    fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n]))
            else:
                fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n]))

        else:
            if repsuppcell:
                fr += adap_euler(Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n])), t, tau_rep, w_rep)
            elif conj: # not used together at this stage
                if n<= int(propconj*N):
                    mu = np.mod(np.random.randint(0,6,1) * 60 + jitter*np.random.randn(), 360)
                    fr += Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n])) * np.exp(kappa*np.cos(direc-np.pi/180.*mu))/special.i0(kappa) #(2*np.pi*special.i0(kappa))
                    # scheint nicht ok, immer noch normiert?
                else:
                    fr += Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n]))
            else:
                fr += Amax*grid(x,y,grsc=grsc,angle=0, offs=(oxr[n],oyr[n]))

    if repsupppop:
        fr = adap_euler(fr, t, tau_rep, w_rep)

    if False:
        plt.figure()
        plt.scatter(x,y,s = 20, c = fr, edgecolors='face')

    #direc = np.mod(direc,2*np.pi)
    direc_binned = np.linspace(-np.pi,np.pi,phbins+1)
    fr_mean = np.zeros(phbins)
    for id in range(len(direc_binned)-1):
        ind = (direc>=direc_binned[id])*(direc<direc_binned[id+1])
        fr_mean[id] = np.sum(fr[ind])*phbins/len(fr)

    if False:
        plt.figure()
        plt.plot(180./np.pi*(direc_binned[:-1]+direc_binned[1:])/2., fr_mean)
        plt.xlabel('Running direction (deg)')
        plt.ylabel('Firing rate (a.u.)')

    return fr_mean, np.abs(np.sum(fr * np.exp(-6j*direc)))/np.size(direc), np.abs(np.sum(np.exp(-6j*direc)))/np.size(direc), np.mean(fr)

#%%
def gridpop_conjcompare(ox, oy, phbins=360, Amax = 1, grsc = 30, dx = 1., propconj = 0.66, kappa = 50, jitter=10):
    """
    simulate the summed activity G(x,y,phi) of a population of grid cells for a part of space (x,y)
    and the movement angle phi, trajectories can later sample the summed grid-cell
    signal from this part of space. We can exploit that G(x,y,phi) is periodic in x and y
    (with the grid scale) and periodic in phi (with 2 pi).
    """
    if type(ox)==int or type(ox)==float:
        N = 1
    else:
        N = len(ox)
    oxr,oyr = convert_to_rhombus(ox,oy)
    x,y = np.meshgrid(np.arange(0,grsc,dx), np.arange(0,grsc,dx))
    xr,yr = convert_to_rhombus(x,y)

    # G is 4-dim at first, number of cells as extra dimension
    if N==1:
        G1 = Amax*grid(xr, yr, grsc=grsc, angle=0, offs=(oxr,oyr))[:,:,None]
    else:
        G1 = Amax*grid(xr, yr, grsc=grsc, angle=0, offs=(oxr,oyr)) # G1(x,y,N)
    direc_bins = np.pi/180 * np.arange(0,360,360/phbins) # dphi = 1 deg
    """
    plt.figure()
    plt.contourf(xr,yr,G1[:,:,0])
    plt.contourf(xr+grsc,yr,G1[:,:,0])
    plt.contourf(xr-grsc,yr,G1[:,:,0])
    plt.contourf(xr+0.5*grsc,yr+np.sqrt(3)/2*grsc,G1[:,:,0])
    plt.contourf(xr-0.5*grsc,yr-np.sqrt(3)/2*grsc,G1[:,:,0])
    plt.contourf(xr+0.5*grsc,yr-np.sqrt(3)/2*grsc,G1[:,:,0])
    plt.contourf(xr-0.5*grsc,yr+np.sqrt(3)/2*grsc,G1[:,:,0])
    plt.contourf(xr+1.5*grsc,yr+np.sqrt(3)/2*grsc,G1[:,:,0])
    plt.contourf(xr-1.5*grsc,yr-np.sqrt(3)/2*grsc,G1[:,:,0])

    plt.figure()
    plt.contourf(xr,yr,G1.sum(axis=2))
    plt.contourf(xr+grsc,yr,G1.sum(axis=2))
    plt.contourf(xr-grsc,yr,G1.sum(axis=2))
    plt.contourf(xr+0.5*grsc,yr+np.sqrt(3)/2*grsc,G1.sum(axis=2))
    plt.contourf(xr-0.5*grsc,yr-np.sqrt(3)/2*grsc,G1.sum(axis=2))
    plt.contourf(xr+0.5*grsc,yr-np.sqrt(3)/2*grsc,G1.sum(axis=2))
    plt.contourf(xr-0.5*grsc,yr+np.sqrt(3)/2*grsc,G1.sum(axis=2))
    plt.contourf(xr+1.5*grsc,yr+np.sqrt(3)/2*grsc,G1.sum(axis=2))
    plt.contourf(xr-1.5*grsc,yr-np.sqrt(3)/2*grsc,G1.sum(axis=2))

    """

    # include conjunctive HD tuning
    if N==6:
        mu = np.mod(np.arange(0,360,60) + jitter*np.random.randn(N), 360) # different for different cells
    else:
        mu = np.mod(np.random.randint(0,6,N) * 60 + jitter*np.random.randn(N), 360) # different for different cells
    rv = stats.vonmises(kappa,loc=np.pi/180*mu)
    h = np.array([rv.pdf(d) for d in direc_bins]).T
    G2 = G1[:,:,:,None] * h[None,None,:,:]

    # exclude conjunctive HD tuning
    h2 = 1./2/np.pi * np.ones(len(direc_bins))
    G3 = G1[:,:,:,None] * h2[None,None,None,:]

    G2sum = np.sum(G2, axis=2)
    G3sum = np.sum(G3, axis=2)
    # to get back G1:
    # G1back = np.sum(G3sum,axis=2)*2*np.pi/phbins
    return G2sum, G3sum

def apply_traj(G,trajec,grsc=30, dx=1, dt=0.1, phbins=360):
    # map trajectory to rhombus and read firing rate from G
    x,y,di,t = trajec
    """
    plt.figure()
    plt.plot(xr,yr,'.-')
    plt.plot(xr+30,yr,'.-')
    plt.plot(xr+15,yr+np.sqrt(3)/2 * 30,'.-')
    """
    xr,yr = convert_to_square(*map_to_rhombus(x,y,grsc=grsc))

    xedge,yedge = np.arange(0,grsc+dx,dx), np.arange(0,grsc+dx,dx)
    diredge = np.pi/180 * np.linspace(-180,180,phbins+1) # dphi = 1 deg
    sample = np.array(list(zip(xr,yr,di)))
    H,xe = np.histogramdd(sample,[xedge,yedge,diredge],density=True)
    H = np.swapaxes(H,0,1)#/np.mean(H)#*np.cumsum(np.sqrt(np.diff(x)**2+np.diff(y)**2)).max()/t.max() # scaled by speed
    #*np.mean(np.diff(xedge))*np.mean(np.diff(yedge))*np.mean(np.diff(diredge))#*np.size(H) #now H has on average the entry 1

    meanfr = np.sum(np.sum(G * H,axis=0),axis=0)*dx*dx*40 # where does 40 come from?
    return meanfr

def get_hexsym(meanfr, phbins=360):
    ntile = 5
    fr2 = np.tile(meanfr,ntile) - np.mean(meanfr) # tile to account for periodicity
    power = 2./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
    ff = np.linspace(0,1./(2.), ntile*phbins//2)

    if 0:
        plt.figure()
        plt.plot(ff,power)
        plt.xticks([1/360,1/90,1/60,1/45,1/30,1/17,1/15], ['360','90','60','45','30','17','15'])

    gr60 = power[np.argmin(abs(ff-1./60*360/phbins))] # pick index which is closest to 60deg
    gr45 = power[np.argmin(abs(ff-1./45*360/phbins))]
    gr90 = power[np.argmin(abs(ff-1./90*360/phbins))]
    sym5 = power[np.argmin(abs(ff-1./(360/5)*360/phbins))]
    sym7 = power[np.argmin(abs(ff-1./(360/7)*360/phbins))]
    gr17 = power[np.argmin(abs(ff-1./17*360/phbins))]

    ph = np.angle(np.fft.fft(fr2))[:len(fr2)//2]
    p60 = ph[np.argmin(abs(ff-1./60*360/phbins))]/2/np.pi*60
    p45 = ph[np.argmin(abs(ff-1./45*360/phbins))]/2/np.pi*45
    p90 = ph[np.argmin(abs(ff-1./90*360/phbins))]/2/np.pi*90
    return gr90,sym5,gr60,sym7,gr45,gr17,p60,p45,p90

def generate_poisson(rate,max_t,delta):
    t = np.arange(delta,max_t, delta)
    avg_prob = 1 - np.exp(-rate * delta)
    rand_throws = np.random.uniform(size=t.shape[0])
    return t[avg_prob >= rand_throws]

# s = 1
# xoff = 0, yoff = 0
# gamma = 0
# 1+cos()
# check overleaf for full formulation
# steps = 0.01/0.1/1/3/3.2
# 10000 steps
# phbins = 100, 360, 1000

#%%
dt=0.1
# have to decrease dt for the Poisson control
dt = 0.001
tmax=1800
sp = 2.
tort = 0.3

ncell = 100
grsc = 30
dx = 1
phbins = 360
nrep = 10
Amax = 20
#ox,oy = 0.5*np.ones(shape=(2,ncell)) #0.4*np.ones(ncell), 0.3*np.ones(ncell) #np.random.rand(2,ncell)
ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(ncell)),endpoint=False),np.linspace(0,1,int(np.sqrt(ncell)),endpoint=False))
ox,oy = np.ravel(ox),np.ravel(oy)
ox2,oy2 = np.random.rand(2,ncell)
ox2,oy2 = np.ravel(ox2),np.ravel(oy2)
gconj = np.zeros((9,nrep))
gcont = np.zeros((9,nrep))
gconj_sarg = np.zeros((9,nrep))
gcont_sarg = np.zeros((9,nrep))
grep = np.zeros((9,nrep))
gold = np.zeros((9,nrep))
goldc = np.zeros((9,nrep))
gdiff = np.zeros((9,nrep))
gconjcl = np.zeros((9,nrep))
gcl = np.zeros((9,nrep))
gshuff = np.zeros((9,nrep))
gconjshuff = np.zeros((9,nrep))
gtraj = np.zeros((9,nrep))
gpoi = np.zeros((9,nrep))
gconj2 = np.zeros((9,nrep))
gcont2 = np.zeros((9,nrep))
Gconj1, Gnoconj1 = gridpop_conjcompare(ox, oy, phbins, Amax = Amax, grsc = grsc, dx = dx, propconj = 0.66, kappa = 50, jitter=0)
Gconj2, Gnoconj2 = gridpop_conjcompare(ox2, oy2, phbins, Amax = Amax, grsc = grsc, dx = dx, propconj = 0.66, kappa = 50, jitter=0)
Gconj_sarg, Gnoconj_sarg = gridpop_conjcompare(ox, oy, phbins, Amax = Amax, grsc = grsc, dx = dx, propconj = 0.66, kappa = 2.5, jitter=10)
Gconjclust, Gclust = gridpop_conjcompare(np.zeros(ncell), np.zeros(ncell), phbins, grsc = grsc, dx = dx, propconj = 0.66, kappa = 50, jitter=0)
for it in range(nrep):
    print(it)
    trajec = traj(dt,tmax,sp=sp,dphi = tort) #np.zeros(int(tmax/dt)), sp*np.arange(0,int(tmax+dt),dt), np.pi/2*np.ones(int(tmax/dt)), np.arange(0,tmax+dt,dt)

    meanfr_conj_pre, meanfr_noconj = apply_traj(Gconj1,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins), apply_traj(Gnoconj1,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins)
    meanfr_conj_pre2, meanfr_noconj2 = apply_traj(Gconj2,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins), apply_traj(Gnoconj2,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins)
    meanfr_conjcl, meanfr_cl = apply_traj(Gconjclust,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins), apply_traj(Gclust,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins)
    meanfr_conj = meanfr_conj_pre/np.mean(meanfr_conj_pre)*np.mean(meanfr_noconj)
    meanfr_conj2 = meanfr_conj_pre2/np.mean(meanfr_conj_pre2)*np.mean(meanfr_noconj2)
    gconj[:,it],gcont[:,it] = get_hexsym(meanfr_conj, phbins=phbins), get_hexsym(meanfr_noconj, phbins=phbins)
    gconj2[:,it],gcont2[:,it] = get_hexsym(meanfr_conj2, phbins=phbins), get_hexsym(meanfr_noconj2, phbins=phbins)
    gconjcl[:,it],gcl[:,it] = get_hexsym(meanfr_conjcl, phbins=phbins), get_hexsym(meanfr_cl, phbins=phbins)
    meanfr_conj_sarg_pre, meanfr_noconj_sarg = apply_traj(Gconj_sarg,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins), apply_traj(Gnoconj_sarg,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins)
    meanfr_conj_sarg = meanfr_conj_sarg_pre/np.mean(meanfr_conj_sarg_pre)*np.mean(meanfr_noconj_sarg)
    gconj_sarg[:,it],gcont_sarg[:,it] = get_hexsym(meanfr_conj_sarg, phbins=phbins), get_hexsym(meanfr_noconj_sarg, phbins=phbins)
    meanfr_old = gridpop(ox, oy, phbins, trajec, Amax=Amax, grsc=grsc, repsuppcell = False, repsupppop = False, conj = False)
    meanfr_oldc = gridpop(np.zeros(ncell), np.zeros(ncell), phbins, trajec, grsc=grsc, repsuppcell = False, repsupppop = False, conj = False)
    meanfr_rep_pre = gridpop(ox, oy, phbins, trajec, Amax=Amax, grsc=grsc, repsuppcell = True, repsupppop = False, tau_rep = 10., w_rep = 1., conj = False)
    meanfr_rep = meanfr_rep_pre/np.mean(meanfr_rep_pre)*np.mean(meanfr_noconj)
    meanfr_shufforient = gridpop(ox, oy, phbins, trajec, Amax=Amax, grsc=grsc, repsuppcell = False, repsupppop = False, conj = False,shufforient=True)
    meanfr_conj_shufforient = gridpop(ox, oy, phbins, trajec, Amax=Amax, grsc=grsc, repsuppcell = False, repsupppop = False, conj = True,shufforient=True)
    gold[:,it] = get_hexsym(meanfr_old,phbins=phbins)
    goldc[:,it] = get_hexsym(meanfr_oldc,phbins=phbins)
    grep[:,it] = get_hexsym(meanfr_rep,phbins=phbins)
    gdiff[:,it] = get_hexsym(meanfr_old-meanfr_rep,phbins=phbins)
    gshuff[:,it] = get_hexsym(meanfr_shufforient,phbins=phbins)
    gconjshuff[:,it] = get_hexsym(meanfr_conj_shufforient,phbins=phbins)
    x,y,di,t = traj(dt,tmax,sp=sp,dphi = tort)
    direc_binned = np.linspace(-np.pi,np.pi,phbins+1)
    fr_mean_traj = np.zeros(phbins)
    rate = 1.34
    spt = generate_poisson(ncell*rate,tmax,dt)
    spkdi = np.array([di[t==sptime] for sptime in spt])
    fr_mean_poi = np.zeros(phbins)
    for id in range(len(direc_binned)-1):
        ind = (di>=direc_binned[id])*(di<direc_binned[id+1])
        fr_mean_traj[id] = np.sum(ind)*phbins/len(di)
        ind = (spkdi>=direc_binned[id])*(spkdi<direc_binned[id+1])
        fr_mean_poi[id] = np.sum(ind)*phbins/len(spkdi)*rate
    gtraj[:,it] = get_hexsym(fr_mean_traj, phbins=360)
    gpoi[:,it] = get_hexsym(fr_mean_poi, phbins=360)


#%%
alldata = np.zeros((50,10))
f = open("hexasymmetry_data.txt", "r")
for i,x in enumerate(f):
    if i>0:
        alldata[:,i-1] = np.array([float(elem) for elem in x.split(',')[:-1]])
f.close()

gpoi,gcont2,gcont,gold,grep,gdiff,gcl,goldc,gconj_sarg,gconj = alldata[0:5,:], alldata[5:10,:],alldata[10:15,:],alldata[15:20,:],alldata[20:25,:],alldata[25:30,:],alldata[30:35,:],alldata[35:40,:],alldata[40:45,:],alldata[45:50,:]

# mit eintragen: Poisson kontrolle, fluctuation
# paarweise statistik
plt.figure(figsize=(8,14))
plt.subplot(3,1,1)
plt.plot(np.linspace(0,360,phbins),meanfr_noconj)
plt.title(np.mean(meanfr_noconj)/np.mean(meanfr_old))
plt.plot(np.linspace(0,360,phbins),meanfr_old)
plt.plot(np.linspace(0,360,phbins),meanfr_cl)
plt.plot(np.linspace(0,360,phbins),meanfr_oldc)
plt.legend(['Random new','Random old','Clustering new','Clustering old'])
plt.subplot(3,1,2)
plt.plot(np.linspace(0,360,phbins),meanfr_old)
plt.plot(np.linspace(0,360,phbins),meanfr_rep)
plt.plot(np.linspace(0,360,phbins),meanfr_old-meanfr_rep)
plt.legend(['Random old','Rep supp','Difference'])
plt.subplot(3,1,3)
plt.plot(np.linspace(0,360,phbins),meanfr_conj)
plt.legend(['Conj'])
plt.xlabel('Movement direction (deg)')
plt.ylabel('Firing rate (1/s)')

# methoden figures
# Poisson control:


plt.figure(figsize=(10,6))
plt.plot([1.01,2.01,3.01,4.01,5.01,6.01,7.01,8.01,9.01,10.01],[gpoi[0,:],gcont2[0,:],gcont[0,:],gold[0,:],grep[0,:],gdiff[0,:],gcl[0,:],goldc[0,:],gconj_sarg[0,:],gconj[0,:]],'ro--',lw=0.1,label='4fold')
plt.plot([1.02,2.02,3.02,4.02,5.02,6.02,7.02,8.02,9.02,10.02],[gpoi[1,:],gcont2[1,:],gcont[1,:],gold[1,:],grep[1,:],gdiff[1,:],gcl[1,:],goldc[1,:],gconj_sarg[1,:],gconj[1,:]],'go--',lw=0.1,label='5fold')
plt.plot([1,2,3,4,5,6,7,8,9,10],[gpoi[2,:],gcont2[2,:],gcont[2,:],gold[2,:],grep[2,:],gdiff[2,:],gcl[2,:],goldc[2,:],gconj_sarg[2,:],gconj[2,:]],'ko--',lw=0.1,label='6fold')
plt.plot([1.03,2.03,3.03,4.03,5.03,6.03,7.03,8.03,9.03,10.03],[gpoi[3,:],gcont2[3,:],gcont[3,:],gold[3,:],grep[3,:],gdiff[3,:],gcl[3,:],goldc[3,:],gconj_sarg[3,:],gconj[3,:]],'bo--',lw=0.1,label='7fold')
plt.plot([1.04,2.04,3.04,4.04,5.04,6.04,7.04,8.04,9.04,10.04],[gpoi[4,:],gcont2[4,:],gcont[4,:],gold[4,:],grep[4,:],gdiff[4,:],gcl[4,:],goldc[4,:],gconj_sarg[4,:],gconj[4,:]],'yo--',lw=0.1,label='8fold')
#plt.plot([1.03,2.03,3.03,4.03,5.03,6.03,7.03,8.03,9.03,10.03],[gpoi[3,:],gcont2[3,:],gcont[3,:],gold[3,:],grep[3,:],gdiff[3,:],gcl[3,:],goldc[3,:],gconj_sarg[3,:],gconj[3,:]],'go--',lw=0.1,label='17deg')
plt.yscale('log')
#plt.title('One grid cell')
plt.ylabel('Symmetry')
plt.xticks([1,2,3,4,5,6,7,8,9,10],['Poisson','Random','Regular','Regular old','Rep supp raw','Rep supp diff','Cluster new','Cluster old','Conj Sargolini','Conj Doeller'])
leg1 = mpatches.Patch(color='red', label='4-fold symmetry')
leg2 = mpatches.Patch(color='green', label='5-fold symmetry')
leg3 = mpatches.Patch(color='black', label='6-fold symmetry')
leg4 = mpatches.Patch(color='blue', label='7-fold symmetry')
leg5 = mpatches.Patch(color='yellow', label='8-fold symmetry')
#leg4 = mpatches.Patch(color='green', label='17deg')
plt.legend(handles=[leg1,leg2,leg3,leg4,leg5])

# clustering added
plt.figure(figsize=(17,5))
plt.subplot(1,3,1)
plt.plot([1,2,3,4], [np.mod(gcont[6,:],60),np.mod(goldc[6,:],60),np.mod(grep[6,:],60),np.mod(gconj[6,:],60)],'ko--',lw=0.1)
plt.xticks([1,2,3,4],['Regular','Clustering','Rep supp','Conjunctive'])
plt.ylabel('Preferred orientation (deg)')
plt.title('60 deg symmetry')
plt.subplot(1,3,2)
plt.plot([1,2,3,4], [np.mod(gcont[7,:],45),np.mod(goldc[7,:],45),np.mod(grep[7,:],45),np.mod(gconj[7,:],45)],'ko--',lw=0.1)
plt.xticks([1,2,3,4],['Regular','Clustering','Rep supp','Conjunctive'])
#plt.ylabel('Preferred orientation (deg)')
plt.title('45 deg symmetry')
plt.subplot(1,3,3)
plt.plot([1,2,3,4], [np.mod(gcont[8,:],90),np.mod(goldc[8,:],90),np.mod(grep[8,:],90),np.mod(gconj[8,:],90)],'ko--',lw=0.1)
plt.xticks([1,2,3,4],['Regular','Clustering','Rep supp','Conjunctive'])
#plt.ylabel('Preferred orientation (deg)')
plt.title('90 deg symmetry')

# correllieren unterschiede in symmmetry mit symmetry der trajectorie
from scipy.stats import pearsonr
fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(2,2,1)
ax.scatter(gcont[2,:],gtraj[2,:])
plt.title('60 deg symmetry')
plt.xlabel('Fluctuation')
plt.ylabel('Trajectory')
r,p = pearsonr(gcont[2,:],gtraj[2,:])
plt.text(0.65,0.7,'r='+str(np.round(r,2))+', p='+str(np.round(p,2)),transform=ax.transAxes)
ax = fig.add_subplot(2,2,2)
ax.scatter(gcont[0,:],gtraj[0,:])
plt.title('90 deg symmetry')
plt.xlabel('Fluctuation')
r,p = pearsonr(gcont[0,:],gtraj[0,:])
plt.text(0.65,0.7,'r='+str(np.round(r,2))+', p='+str(np.round(p,2)),transform=ax.transAxes)
#plt.ylabel('Trajectory')
ax = fig.add_subplot(2,2,3)
ax.scatter(gcl[2,:],gtraj[2,:])
#plt.title('60 deg symmetry')
plt.xlabel('Clustering')
plt.ylabel('Trajectory')
r,p = pearsonr(gcl[2,:],gtraj[2,:])
plt.text(0.65,0.7,'r='+str(np.round(r,2))+', p='+str(np.round(p,2)),transform=ax.transAxes)
ax = fig.add_subplot(2,2,4)
ax.scatter(gcl[0,:],gtraj[0,:])
r,p = pearsonr(gcl[0,:],gtraj[0,:])
plt.text(0.65,0.7,'r='+str(np.round(r,2))+', p='+str(np.round(p,2)),transform=ax.transAxes)
#plt.title('90 deg symmetry')
plt.xlabel('Clustering')

#%%
# one cell for first point in paper
sp = 10.
tort = 0.3
dt = 0.1
tmax=7200
phbins = 360
ox,oy = (0,0)
Amax = 20
grsc = 30
dx = 1
#oxreg,oyreg =
nrep=1
i=0
gconj = np.zeros((9,nrep))
gclust = np.zeros((9,nrep))
gclustold = np.zeros((9,nrep))
gclust_meanfr = np.zeros((phbins,nrep))
gclustold_meanfr = np.zeros((phbins,nrep))
Gconj1, Gnoconj1 = gridpop_conjcompare(ox, oy, phbins, Amax = Amax, grsc = grsc, dx = dx, propconj = 0.66, kappa = 50, jitter=0)
Gfluct, G = gridpop_conjcompare(ox, oy, phbins, Amax = Amax, grsc = grsc, dx = dx, propconj = 0.66, kappa = 50, jitter=0)
while i<nrep:
    print(i)
    #trajec = traj(dt,tmax,sp=sp,dphi = tort,pwlin=False)
    trajec = traj_control(dt,tmax,offs=(0,0),draw=False)
    meanfr_conjcl, meanfr_cl = apply_traj(Gconj1,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins), apply_traj(Gnoconj1,trajec,grsc=grsc, dx=dx, dt=dt, phbins=phbins)
    meanfr_oldc = gridpop(np.array([ox]), np.array([oy]), phbins, trajec, grsc=grsc, repsuppcell = False, repsupppop = False, conj = False)
    gclust_meanfr[:,i] = meanfr_cl
    gclustold_meanfr[:,i] = meanfr_oldc
    gclust[:,i] = get_hexsym(meanfr_cl,phbins=phbins)
    gclustold[:,i] = get_hexsym(meanfr_oldc,phbins=phbins)
    i+=1

fig,ax = plt.subplots(1,1,figsize=(12,12))
#plt.plot(np.linspace(0,360,phbins),gclustold_meanfr[:,0],'k-')
plt.plot(np.linspace(0,360,phbins),gclust_meanfr[:,0],'b-')
plt.plot(np.linspace(0,360,phbins),np.mean(gclust_meanfr,axis=1),'k--')
plt.plot(np.linspace(0,360,phbins),np.mean(gclust_meanfr,axis=1)-np.std(gclust_meanfr,axis=1),'k--')
plt.plot(np.linspace(0,360,phbins),np.mean(gclust_meanfr,axis=1)+np.std(gclust_meanfr,axis=1),'k--')
#plt.plot(np.linspace(0,360,phbins),np.mean(gclustold_meanfr,axis=1),'r--')
#plt.plot(np.linspace(0,360,phbins),np.mean(gclustold_meanfr,axis=1)-np.std(gclustold_meanfr,axis=1),'r--')
#plt.plot(np.linspace(0,360,phbins),np.mean(gclustold_meanfr,axis=1)+np.std(gclustold_meanfr,axis=1),'r--')
plt.text(0.1,0.95,'S6='+str(np.round(np.mean(gclust[2,:]),2))+' +- '+str(np.round(np.std(gclust[2,:]),2)),transform=ax.transAxes)
plt.text(0.1,0.9,'S4='+str(np.round(np.mean(gclust[0,:]),2))+' +- '+str(np.round(np.std(gclust[0,:]),2)),transform=ax.transAxes)
plt.text(0.5,0.95,'S6='+str(np.round(np.mean(gclustold[2,:]),2))+' +- '+str(np.round(np.std(gclustold[2,:]),2)),transform=ax.transAxes)
plt.text(0.5,0.9,'S4='+str(np.round(np.mean(gclustold[0,:]),2))+' +- '+str(np.round(np.std(gclustold[0,:]),2)),transform=ax.transAxes)
plt.plot(np.linspace(0,360,1000),np.mean(gclust_meanfr[:,0]) + gclust[2,0]*np.cos(2*np.pi *1/60 * np.linspace(0,360,1000) + gclust[-3,0]/30*np.pi),'k-')
plt.plot(np.linspace(0,360,1000),np.mean(gclust_meanfr) + np.mean(gclust[0,:])*np.cos(2*np.pi *1/90 * np.linspace(0,360,1000) + circmean(gclust[-1,:]/45*np.pi)),'r-')
plt.xlabel('Movement direction (deg)')
plt.ylabel('Firing rate (spk/s)')

signal = gclust_meanfr[:,0]
abs(np.sum(signal*np.exp(1j*1*np.linspace(0,2*np.pi,360,endpoint=False))))/sum(signal)

# try average meanfr for 60deg
meanfr_noise = np.mean(gclust_meanfr) + np.std(gclust_meanfr)*np.random.randn(*np.shape(gclust_meanfr))
#meanfr_noise = np.mean(gclustold_meanfr) + np.std(gclustold_meanfr)*np.random.randn(*np.shape(gclustold_meanfr))
gnoise = np.zeros((9,nrep))
for i in range(nrep):
    gnoise[:,i] = get_hexsym(meanfr_noise[:,i],phbins=phbins)
fig,ax = plt.subplots(1,1,figsize=(12,12))
plt.plot(np.linspace(0,360,phbins),np.mean(meanfr_noise,axis=1))
plt.plot(np.linspace(0,360,phbins),np.mean(gclust_meanfr,axis=1))
#plt.plot(np.linspace(0,360,phbins),np.mean(gclustold_meanfr,axis=1))
plt.plot(np.linspace(0,360,1000),np.mean(gclust_meanfr) + np.mean(gclust[2,:])*np.cos(2*np.pi *1/60 * np.linspace(0,360,1000)),'k-')
#plt.plot(np.linspace(0,360,1000),np.mean(gclustold_meanfr) + np.mean(gclustold[2,:])*np.cos(2*np.pi *1/60 * np.linspace(0,360,1000)),'k-')
plt.plot(np.linspace(0,360,1000),np.mean(meanfr_noise) + np.mean(gnoise[2,:])*np.cos(2*np.pi *1/60 * np.linspace(0,360,1000)),'k--')
plt.xlabel('Movement direction (deg)')
plt.ylabel('Firing rate (spk/s)')
plt.text(0.1,0.95,'S6='+str(np.round(np.mean(gclust[2,:]),2))+' +- '+str(np.round(np.std(gclust[2,:]),2)),transform=ax.transAxes)
plt.text(0.1,0.9,'S4='+str(np.round(np.mean(gclust[0,:]),2))+' +- '+str(np.round(np.std(gclust[0,:]),2)),transform=ax.transAxes)
plt.text(0.5,0.95,'S6='+str(np.round(np.mean(gclustold[2,:]),2))+' +- '+str(np.round(np.std(gclustold[2,:]),2)),transform=ax.transAxes)
plt.text(0.5,0.9,'S4='+str(np.round(np.mean(gclustold[0,:]),2))+' +- '+str(np.round(np.std(gclustold[0,:]),2)),transform=ax.transAxes)

"""
# align phases for meanfr, white noise control
gclust_meanfr_roll60 = gclust_meanfr.copy()
meanfr_noise_roll60 = meanfr_noise.copy()
for i in range(nrep):
    #gclust_meanfr_roll60[:,i] = np.roll(gclustold_meanfr[:,i],int(gclustold[-3,i]),axis=0)
    gclust_meanfr_roll60[:,i] = np.roll(gclust_meanfr[:,i],int(gclust[-3,i]),axis=0)
    meanfr_noise_roll60[:,i] = np.roll(meanfr_noise[:,i],int(gnoise[-3,i]),axis=0)
plt.figure()
plt.plot(np.linspace(0,360,phbins),np.mean(meanfr_noise_roll60,axis=1))
plt.plot(np.linspace(0,360,phbins),np.mean(gclust_meanfr_roll60,axis=1))
plt.plot(np.linspace(0,360,1000),np.mean(gclust_meanfr) + np.mean(gclust[2,:])*np.cos(2*np.pi *1/60 * np.linspace(0,360,1000)),'k-')
#plt.plot(np.linspace(0,360,1000),np.mean(gclustold_meanfr) + np.mean(gclustold[2,:])*np.cos(2*np.pi *1/60 * np.linspace(0,360,1000)),'k-')
plt.plot(np.linspace(0,360,1000),np.mean(meanfr_noise) + np.mean(gnoise[2,:])*np.cos(2*np.pi *1/60 * np.linspace(0,360,1000)),'k--')
plt.xlabel('Movement direction (deg)')
plt.ylabel('Firing rate (spk/s)')

# same thing for 90deg
gclust_meanfr_roll90 = gclust_meanfr.copy()
meanfr_noise_roll90 = meanfr_noise.copy()
for i in range(nrep):
    gclust_meanfr_roll90[:,i] = np.roll(gclust_meanfr[:,i],int(gclust[-1,i]),axis=0)
    #gclust_meanfr_roll90[:,i] = np.roll(gclustold_meanfr[:,i],int(gclustold[-1,i]),axis=0)
    meanfr_noise_roll90[:,i] = np.roll(meanfr_noise[:,i],int(gnoise[-1,i]),axis=0)
plt.figure()
plt.plot(np.linspace(0,360,phbins),np.mean(meanfr_noise_roll90,axis=1))
plt.plot(np.linspace(0,360,phbins),np.mean(gclust_meanfr_roll90,axis=1))
plt.plot(np.linspace(0,360,1000),np.mean(gclust_meanfr) + np.mean(gclust[0,:])*np.cos(2*np.pi *1/90 * np.linspace(0,360,1000)),'k-')
#plt.plot(np.linspace(0,360,1000),np.mean(gclustold_meanfr) + np.mean(gclustold[0,:])*np.cos(2*np.pi *1/90 * np.linspace(0,360,1000)),'k-')
plt.plot(np.linspace(0,360,1000),np.mean(meanfr_noise) + np.mean(gnoise[0,:])*np.cos(2*np.pi *1/90 * np.linspace(0,360,1000)),'k-')
plt.xlabel('Movement direction (deg)')
plt.ylabel('Firing rate (spk/s)')
"""

#%%
# two plots for to explain why the hexasymmetry even of a single cell is weak

# random walk, high temporal resolution
sp = 10.
tort = 1.
dt = 0.1
tmax = 120
x,y,di,t = traj(dt,tmax,sp=sp,dphi = tort)

plt.figure()
slope = np.tan(np.pi/3)
for yy in np.arange(min(y),max(y),np.sqrt(3)/2*30):
    plt.plot([min(x),max(x)],[yy,yy],'k')
for xx in np.arange(-2*max(abs(x)),2*max(abs(x)),30):
    plt.plot([-2/np.sqrt(3)*max(y)+xx,2/np.sqrt(3)*max(y)+xx],[slope*-2/np.sqrt(3)*max(y),slope*2/np.sqrt(3)*max(y)],'k')
plt.scatter(x,y,s=40,c=di,cmap='twilight_shifted',zorder=3)
cbar=plt.colorbar()
cbar.ax.set_ylabel('Movement direction (rad)', rotation=-90, va="bottom")
plt.xlabel('x (cm)')
plt.ylabel('y (cm)')
plt.axis([min(x),max(x),min(y),max(y)])

# piecewise linear trajectory
sp = 10.
tort = 1.
dt = 6
tmax = 120
dt2 = 0.1
x,y,di,t = traj(dt,tmax,sp=sp,dphi = tort)
t2 = np.arange(0,t[-1]+dt2,dt2)
fx = interpolate.interp1d(t, x)
fy = interpolate.interp1d(t, y)
x2 = fx(t2)
y2 = fy(t2)
di = np.repeat(di,np.diff(t)[0]/dt2)
plt.figure()
slope = np.tan(np.pi/3)
for yy in np.arange(min(y2),max(y2),np.sqrt(3)/2*30):
    plt.plot([min(x2),max(x2)],[yy,yy],'k')
for xx in np.arange(-2*max(abs(x2)),2*max(abs(x2)),30):
    plt.plot([-2/np.sqrt(3)*max(y)+xx,2/np.sqrt(3)*max(y)+xx],[slope*-2/np.sqrt(3)*max(y),slope*2/np.sqrt(3)*max(y)],'k')
plt.plot(x2,y2,'b.-')
plt.plot(x,y,'ro')
plt.xlabel('x (cm)')
plt.ylabel('y (cm)')
plt.axis([min(x),max(x),min(y),max(y)])


#%%
# create matrix of pairwise comparisons
def heatmap(data, row_labels, col_labels, ax=None, cbar_kw={}, cbarnorm=None, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, norm=cbarnorm,**kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-45, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    #ax.grid(which="minor", color="w", linestyle='-', linewidth=1)
    ax.axhline(4.5, linestyle='--', color='k')
    ax.axhline(9.5, linestyle='--', color='k')
    ax.axhline(14.5, linestyle='--', color='k')
    ax.axhline(19.5, linestyle='--', color='k')
    ax.axhline(24.5, linestyle='--', color='k')
    ax.axhline(29.5, linestyle='--', color='k')
    ax.axhline(34.5, linestyle='--', color='k')
    ax.axhline(39.5, linestyle='--', color='k')
    ax.axhline(44.5, linestyle='--', color='k')

    ax.axvline(4.5, linestyle='--', color='k')
    ax.axvline(9.5, linestyle='--', color='k')
    ax.axvline(14.5, linestyle='--', color='k')
    ax.axvline(19.5, linestyle='--', color='k')
    ax.axvline(24.5, linestyle='--', color='k')
    ax.axvline(29.5, linestyle='--', color='k')
    ax.axvline(34.5, linestyle='--', color='k')
    ax.axvline(39.5, linestyle='--', color='k')
    ax.axvline(44.5, linestyle='--', color='k')

    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar

def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            #kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            kw.update(color='black')
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts

def func(x, pos):
    return "{:.1f}".format(x).replace("0.", ".").replace("1.0", "")

def func2(x, pos):
    x2 = -np.log10(x)
    if x2>99 or x==1:
        return ""
    else:
        return "{:.0f}".format(x2)

#alldata = np.vstack((gpoi[:5,:],gcont2[:5,:],gcont[:5,:],gold[:5,:],grep[:5,:],gdiff[:5,:],gcl[:5,:],goldc[:5,:],gconj_sarg[:5,:],gconj[:5,:]))
alldata = np.zeros((10,50))
f = open("hexasymmetry_data.txt", "r")
for i,x in enumerate(f):
    if i>0:
        alldata[i-1,:] = np.array([float(elem) for elem in x.split(',')[:-1]])
f.close()

corr_matrix,p_matrix = stats.spearmanr(alldata)

# create t-test p-value matrix
pval_matrix = np.ones((50,50))
for i in range(50):
    for j in range(i):
        t,p = stats.ttest_rel(alldata[:,i],alldata[:,j])
        pval_matrix[i,j] = p
        pval_matrix[j,i] = p

labels = ['Poisson 4','Poisson 5','Poisson 6','Poisson 7','Poisson 8',
          'Fluct 4','Fluct 5','Fluct 6','Fluct 7','Fluct 8',
          'Regular 4','Regular 5','Regular 6','Regular 7','Regular 8',
          'Regular old 4','Regular old 5','Regular old 6','Regular old 7','Regular old 8',
          'Rep supp 4','Rep supp 5','Rep supp 6','Rep supp 7','Rep supp 8',
          'Rep supp diff 4','Rep supp diff 5','Rep supp diff 6','Rep supp diff 7','Rep supp diff 8',
          'Clust 4','Clust 5','Clust 6','Clust 7','Clust 8',
          'Clust old 4','Clust old 5','Clust old 6','Clust old 7','Clust old 8',
          'Conj Sarg 4','Conj Sarg 5','Conj Sarg 6','Conj Sarg 7','Conj Sarg 8',
          'Conj Doel 4','Conj Doel 5','Conj Doel 6','Conj Doel 7','Conj Doel 8']

# store for later use
"""
with open('hexasymmetry_data.txt', 'a') as f:
    for ent in labels[:-1]:
        f.write(ent + ', ')
    f.write(labels[-1] + '\n')
    f.close

with open('hexasymmetry_data.txt', 'a') as f:
    for x in alldata.T:
        for elem in x:
            f.write(str(elem)+', ')
        f.write(' \n')
"""

#fig, ((ax1, ax2)) = plt.subplots(1, 2, figsize=(20, 10))

# Spearman correlations
fig,ax1 = plt.subplots(1,1,figsize=(12,12))
im, _ = heatmap(corr_matrix, labels, labels, ax=ax1,
                cmap="bwr", vmin=-1, vmax=1,
                cbarlabel="Spearman corr.")

annotate_heatmap(im, valfmt=matplotlib.ticker.FuncFormatter(func), size=7)
plt.tight_layout()

# Spearman p-values
redsrev = matplotlib.cm.get_cmap('Reds_r')
fig2,ax2 = plt.subplots(1,1,figsize=(12,12))
im, _ = heatmap(p_matrix, labels, labels, ax=ax2,
                cmap=redsrev, vmin=1e-4, vmax=0.05,
                cbarnorm=matplotlib.colors.LogNorm(),
                cbarlabel="p value")
annotate_heatmap(im, valfmt=matplotlib.ticker.FuncFormatter(func2), size=7)
plt.tight_layout()

# pairwise comparisons t-test
redsrev = matplotlib.cm.get_cmap('Reds_r')
fig2,ax2 = plt.subplots(1,1,figsize=(12,12))
im, _ = heatmap(np.where(pval_matrix<1e-10,1e-10,pval_matrix), labels, labels, ax=ax2,
                cmap=redsrev, vmin=1e-4, vmax=0.05,
                cbarnorm=matplotlib.colors.LogNorm(),
                cbarlabel="p value")
annotate_heatmap(im, valfmt=matplotlib.ticker.FuncFormatter(func2), size=7)
plt.tight_layout()


plt.show()


##### warum fuer clustering 60deg und 90deg gleich? sehr komisch
# anzahl der spikes pro bin? richtungs-bin?
# phase dependence of hexasym clustering for 90deg

# fuer eine zelle, 1/6? -> fuer gcont ja, fuer gconj nein, da eher 3fach

# distal cues in Sargolini (white cue card in otherwise black enclosure) vs Doeller (lab environment visible to animal)
#%%
# measure hexasymmetry of the random-walk trajectory
dt=0.1
tmax=36000
sp = 2.
tort = 0.3
phbins = 360
x,y,di,t = traj(dt,tmax,sp=sp,dphi = tort) #np.zeros(int(tmax/dt)), sp*np.arange(0,int(tmax+dt),dt), np.pi/2*np.ones(int(tmax/dt)), np.arange(0,tmax+dt,dt)

direc_binned = np.linspace(-np.pi,np.pi,phbins+1)
fr_mean = np.zeros(phbins)
for id in range(len(direc_binned)-1):
    ind = (di>=direc_binned[id])*(di<direc_binned[id+1])
    fr_mean[id] = np.sum(ind)*phbins/len(di)

plt.figure()
plt.plot(180./np.pi*(direc_binned[:-1]+direc_binned[1:])/2., fr_mean)
plt.xlabel('Running direction (deg)')
plt.ylabel('Firing rate (a.u.)')
s60,s45,s90,s17,a,b,c = get_hexsym(fr_mean, phbins=360)
plt.title('s60 = '+str(np.round(s60,3)) + ';, s90 = ' + str(np.round(s90,3))+ ';, s45 = ' + str(np.round(s45,3)))

"""
# after rhombus processing
xr,yr = convert_to_square(*map_to_rhombus(x,y,grsc=grsc))
testdir = np.append(np.array([np.pi/4]), np.arctan2(np.diff(yr),np.diff(xr))[np.sqrt(np.diff(xr)**2 + np.diff(yr)**2)<15])

xedge,yedge = np.arange(0,grsc+dx,dx), np.arange(0,grsc+dx,dx)
diredge = np.pi/180 * np.linspace(-180,180,phbins+1) # dphi = 1 deg

sample = np.array(list(zip(xr,yr,di)))
H,xe = np.histogramdd(sample,[xedge,yedge,diredge],density=True)
H = np.swapaxes(H,0,1)#/np.mean(H)#*np.cumsum(np.sqrt(np.diff(x)**2+np.diff(y)**2)).max()/t.max() # scaled by speed
#*np.mean(np.diff(xedge))*np.mean(np.diff(yedge))*np.mean(np.diff(diredge))#*np.size(H) #now H has on average the entry 1
meanfr = np.sum(np.sum(H,axis=0),axis=0)*dx*dx*np.sum(fr_mean)*2*np.pi/360
# meanfr has integral=1 due to density=True, scaling it to the integral of fr_mean from above

plt.figure()
plt.plot(180./np.pi*(direc_binned[:-1]+direc_binned[1:])/2., meanfr)
plt.xlabel('Running direction (deg)')
plt.ylabel('Firing rate (a.u.)')
s60,s45,s90,s17 = get_hexsym(meanfr, phbins=360)
plt.title('H = '+str(np.round(s60,3)))
"""


#%% meansure hexasymmetry of hom. Poisson process
def generate_poisson(rate,max_t,delta):
    t = np.arange(delta,max_t, delta)
    avg_prob = 1 - np.exp(-rate * delta)
    rand_throws = np.random.uniform(size=t.shape[0])
    return t[avg_prob >= rand_throws]

sp = 1.
tort = 0.5
tmax = 720
dt = 0.001
rate = 1.34
"""
ncell = 100
spt = generate_poisson(ncell*rate,tmax,0.001)
x,y,di,t = traj(dt,tmax,sp=sp,dphi = tort)

spkdi = np.array([di[t==sptime] for sptime in spt])
phbins = 360
direc_binned = np.linspace(-np.pi,np.pi,phbins+1)
fr_mean_poi = np.zeros(phbins)
for id in range(len(direc_binned)-1):
    ind = (spkdi>=direc_binned[id])*(spkdi<direc_binned[id+1])
    fr_mean_poi[id] = np.sum(ind)*phbins/len(spkdi)*rate
gpoi = get_hexsym(fr_mean_poi, phbins=360)
print(gpoi[0])
"""
# depends on the number of cells?
x,y,di,t = traj(dt,tmax,sp=sp,dphi = tort)
rate = 1.34
ns = np.logspace(0,2,10)
g60 = np.zeros(len(ns))
g90 = np.zeros(len(ns))
for i,ncell in enumerate(ns):
    print(ncell)
    spt = generate_poisson(ncell*rate,tmax,0.001)
    spkdi = np.array([di[t==sptime] for sptime in spt])
    direc_binned = np.linspace(-np.pi,np.pi,phbins+1)
    fr_mean_poi = np.zeros(phbins)
    for id in range(len(direc_binned)-1):
        ind = (spkdi>=direc_binned[id])*(spkdi<direc_binned[id+1])
        fr_mean_poi[id] = np.sum(ind)*phbins/len(spkdi)*rate
    gpoisson = get_hexsym(fr_mean_poi, phbins=360)
    g60[i] = gpoisson[0]
    g90[i] = gpoisson[2]

plt.figure()
plt.plot(ns,g60,'b.-')
plt.plot(ns,g90,'r.-')
plt.xlabel('Number of cells')
plt.ylabel('Symmetry')
plt.legend(['60 deg','90 deg'],loc=1)

#%% adjust time constant of repetition suppression for maximum effect
dt=0.1
tmax=600
sp = 10.
tort = 0.4

ncell = 1
grsc = 30
ox,oy = np.zeros(shape=(2,ncell)) #np.random.rand(2,ncell)
dx = 1
phbins = 360
taus = np.linspace(1,10,2)
grep = np.zeros((4,len(taus)))
nrep = 100
for it,tau in enumerate(taus):
    print(tau)
    ii = 0
    sumhexsym = np.zeros(4)
    while ii<nrep:
        trajec = traj(dt,tmax,sp=sp,dphi = tort)
        meanfr_rep = gridpop(ox, oy, phbins, trajec, grsc=30,repsuppcell = True, repsupppop = False, tau_rep = tau, w_rep = 10., conj = False)
        sumhexsym += get_hexsym(meanfr_rep,phbins=360)
        ii += 1
    grep[:,it] = sumhexsym/nrep

plt.figure()
plt.plot(taus,grep[0,:],'o-')
plt.xlabel('Adaptation time constant (s)')
plt.ylabel('Hexasymmetry')

#%% something seems off about the trajectory and the grid - adjusting trajectory parameters here
dt=9
tmax=900
sp = 10.
tort = 0.3
xmax = 400
X,Y = np.meshgrid(np.linspace(-xmax,xmax,400),np.linspace(-xmax,xmax,400))
x,y,di,t = traj(dt,tmax,sp=sp,dphi = tort,pwlin=True)
fig1, ax1 = plt.subplots()
plt.pcolor(X,Y,grid(X,Y,grsc=30,offs=(0,0)))
ax1.set_aspect('equal')
plt.plot(x,y,'w-')

plt.figure()
plt.plot(t,grid(x,y,grsc=30,offs=(0,0)))


#%%
# investigate dependence on simulation duration
ncell = 200
grsc = 30
dx = 1
phbins = 360
ox,oy = np.random.rand(2,ncell)
tm = np.arange(300,12001,300)
gconj = np.zeros(len(tm))
gcont = np.zeros(len(tm))
Gconj1, Gnoconj1 = gridpop_conjcompare(ox, oy, phbins, N = ncell, grsc = grsc, dx = dx, propconj = 0.66, kappa = 50, jitter=0)
for it,tmax in enumerate(tm):
    gconj[it],gcont[it] = get_hexsym(Gconj1,Gnoconj1,sp=2,tort=0.1,dx=dx,phbins=phbins, dt=0.1,tmax=tmax)

# estimate hexasymmetry for perfect sampling
H = np.ones(np.shape(Gconj1))
meanfr_conj = np.sum(np.sum(Gconj1*H,axis=0),axis=0)
meanfr_noconj = np.sum(np.sum(Gnoconj1*H,axis=0),axis=0)
fr2_conj = np.tile(meanfr_conj,3) # tile to account for periodicity
fr2_noconj = np.tile(meanfr_noconj,3) # tile to account for periodicity
power_conj = 2./len(fr2_conj) * abs(np.fft.fft(fr2_conj))[:len(fr2_conj)//2]
power_noconj = 2./len(fr2_noconj) * abs(np.fft.fft(fr2_noconj))[:len(fr2_noconj)//2]
ff = np.linspace(0,1./(2.), 3*phbins//2)
gr60_conj = power_conj[np.argmin(abs(ff-1./60*360/phbins))] # pick index which is closest to 60deg
gr60_noconj = power_noconj[np.argmin(abs(ff-1./60*360/phbins))]

# plot result
plt.figure()
plt.plot(tm/60,gconj,'bo',tm/60,gcont,'go')
plt.plot(tm/60,gr60_conj*np.ones(len(tm)),'b--')
plt.plot(tm/60,gr60_noconj*np.ones(len(tm)),'g--')
plt.legend(['Conj','Control'])
plt.xlabel('Simulated time (min)')
plt.ylabel('Hexasymmetry')


#%%
# run only once for a given parameter set
ncell = 1000
grsc = 30
dx = 1
phbins = 360
tmax = 7200
ox,oy = np.random.rand(2,ncell)
Gconj1, Gnoconj1 = gridpop_conjcompare(ox, oy, phbins, N = ncell, grsc = grsc, dx = dx, propconj = 0.66, kappa = 50, jitter=0)
Gconj2, Gnoconj2 = gridpop_conjcompare(ox, oy, phbins, N = ncell, grsc = grsc, dx = dx, propconj = 0.66, kappa = 2.5, jitter=10)

oxc,oyc = np.random.vonmises(0,10,ncell)/2/np.pi + 0.3, np.random.vonmises(0,10,ncell)/2/np.pi + 0.4
Gclustconj, Gclust = gridpop_conjcompare(oxc, oyc, phbins, N = ncell, grsc = grsc, dx = dx, propconj = 0.66, kappa = 50, jitter=0)

# run 100 realizations of trajectory
gconj,gcont = np.array([get_hexsym(Gconj1,Gnoconj1,sp=2,tort=0.1,dx=dx,tmax=tmax) for i in range(100)]).T
gconj_weak, gcont_weak = np.array([get_hexsym(Gconj2,Gnoconj2,sp=2,tort=0.1,dx=dx,tmax=tmax) for i in range(100)]).T
gclustconj, gclust = np.array([get_hexsym(Gclustconj,Gclust,sp=2,tort=0.1,dx=dx,tmax=tmax) for i in range(100)]).T

plt.figure()
plt.plot([1,2],[gcont,gconj],'b',lw=0.1)
plt.scatter(np.ones(len(gcont)),gcont,c='b')
plt.scatter(2*np.ones(len(gcont)),gconj,c='b')
plt.plot([3,4],[gcont_weak,gconj_weak],'g',lw=0.1)
plt.scatter(3*np.ones(len(gcont_weak)),gcont_weak,c='g')
plt.scatter(4*np.ones(len(gcont_weak)),gconj_weak,c='g')
plt.plot([5,6],[gclust,gclustconj],'r',lw=0.1)
plt.scatter(5*np.ones(len(gclust)),gclust,c='r')
plt.scatter(6*np.ones(len(gclust)),gclustconj,c='r')
#plt.yscale('log')
plt.title('N=100 trajectories')
plt.ylabel('Hexasymmetry')
plt.xticks([1,2,3,4,5,6],['No conj','Conj Doeller','No conj','Conj Sargolini','Clustering','Conj+clustering'])

#%%
def gridpop_meanfr(rmax = 90, Amax = 1, bins = 500, phbins = 360, mode = 'randi', N = 100, path = 'linear', dphi = 0.1, clsize = 0.25, meanoff = (0, 0), conj = False, propconj = 1, kappa = 50., jitter = 0, kappacl = 10, repsuppcell = False, repsupppop = False, tau_rep = 5., w_rep = 1., speed = 10., plotres = False):

    if mode == 'clust':
        ox,oy = np.mod(np.linspace(meanoff[0]-clsize/2.,meanoff[0]+clsize/2.,N),1), np.mod(np.linspace(meanoff[1]-clsize/2.,meanoff[1]+clsize/2.,N),1)
        # just a linear stretch of phases, use meshgrid to get a small rhombus
    elif mode == 'clustcirc':
        # sample N cells from a circle of radius clsize around meanoff
        r = clsize/2.*np.sqrt(np.random.rand(N))
        phi = 2*np.pi*np.random.rand(N)
        ox,oy = np.mod(meanoff[0] + r*np.cos(phi),1), np.mod(meanoff[1] + r*np.sin(phi),1)
    elif mode == 'clustvm':
        ox, oy = np.random.vonmises(2*np.pi*(meanoff[0]-0.5), kappacl, N)/2./np.pi + 0.5, np.random.vonmises(2*np.pi*(meanoff[1]-0.5), kappacl, N)/2./np.pi + 0.5
    elif mode == 'uniform':
        ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
        ox = ox.reshape(1,-1)[0]
        oy = oy.reshape(1,-1)[0]
        N = int(np.sqrt(N))**2
    elif mode == 'randi':
        ox,oy = np.random.rand(N), np.random.rand(N)
    else:
        ox,oy = np.array([meanoff[0]]), np.array([meanoff[1]])

    oxr, oyr = convert_to_rhombus(ox, oy)
    if path == 'linear':
        if N == 1:
            r,phi = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins))
            X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)
            grids = Amax * grid(X,Y,offs=(oxr,oyr), rect=True)
            grids2 = grids.copy()
            """
            plt.figure()
            slope = np.tan(np.pi/3)
            for yy in np.arange(-300,301,np.sqrt(3)/2*30):
                plt.plot([-300,300],[yy,yy],'k')
            for xx in np.arange(-500,501,30):
                plt.plot([-2/np.sqrt(3)*300+xx,2/np.sqrt(3)*300+xx],[slope*-2/np.sqrt(3)*300,slope*2/np.sqrt(3)*300],'k')
            plt.scatter(X,Y,zorder=3,s=1)
            plt.axis([-rmax,rmax,-rmax,rmax])
            plt.xlabel('x (cm)')
            plt.ylabel('y (cm)')
            """

            # how does meanfr depend on bins ... Naomi showed that the different peaks in meanfr seem to have different heights for higher number of bins??
        else:
            r,phi,indoff = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins), np.arange(len(ox)))
            X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)

            # calculate integrated firing rate as a function of the movement angle
            grids = Amax * grid(X,Y,offs=(oxr,oyr))[:, :, 0, :]
            grids2 = grids.copy()

        if conj: # add head-direction tuning if wanted
            Nconj = int(propconj*N)
            #mu = np.random.randint(0,6,Nconj) * 60 # a multiple of 60deg
            mu = np.mod(np.random.randint(0, 6, Nconj) * 60 + jitter*np.random.randn(Nconj), 360)
            vonmi = np.exp(kappa*np.cos(np.pi/180*(np.linspace(0, 360, phbins)[:, None]-mu[None, :])))/special.i0(kappa)# (2*np.pi*special.i0(kappa))
            if N == 1 and Nconj == 1:
                grids = grids*vonmi
            else:
                grids[:, :, :Nconj] = grids[:, :, :Nconj]*vonmi[:, None, :] # change the first Nconj cells
        if repsuppcell:
            #speed = 1. # grid scale/s
            #tau_rep = 1. #s
            #w_rep = 3.
            #s = grids[0,:,0]

            tt = np.linspace(0,rmax/speed,bins)
            #aa = np.zeros(np.shape(grids))
            for idir in range(phbins):
                if N == 1:
                    v = adap_euler(grids[idir,:], tt, tau_rep, w_rep)
                    grids[idir,:] = v
                else:
                    for ic in range(N):
                        v = adap_euler(grids[idir, :, ic], tt, tau_rep, w_rep)
                        grids[idir, :, ic] = v
                        #aa[idir,:,ic] = a
            """
            plt.figure()
            plt.subplot(2,1,1)
            plt.plot(np.linspace(0,rmax/speed,bins),grids2[30,:,0])
            plt.plot(np.linspace(0,rmax/speed,bins),grids[30,:,0])
            plt.plot(np.linspace(0,rmax/speed,bins),aa[30,:,0])
            plt.title('30 deg')

            plt.subplot(2,1,2)
            plt.plot(np.linspace(0,rmax/speed,bins),grids2[60,:,0])
            plt.plot(np.linspace(0,rmax/speed,bins),grids[60,:,0])
            plt.plot(np.linspace(0,rmax/speed,bins),aa[60,:,0])
            plt.xlabel('Time (s)')
            plt.ylabel('Normalized firing rate')
            plt.title('60 deg')
            """
        if repsupppop:
            tt = np.linspace(0,rmax/speed,bins)
            meanpop_base = np.sum(grids,axis=2)
            meanpop = np.sum(grids,axis=2)
            for idir in range(phbins):
                v = adap_euler(meanpop[idir,:],tt,tau_rep,w_rep)
                meanpop[idir,:] = v
            meanfr = np.sum(meanpop, axis=1)/bins
            meanfr_base = np.sum(meanpop_base, axis=1)/bins
            meanfr = meanfr/np.mean(meanfr)*np.mean(meanfr_base)

        else:
            if N==1:
                meanfr_base = np.sum(grids2,axis=1)/bins
                meanfr = np.sum(grids,axis=1)/bins
                meanfr = meanfr/np.mean(meanfr)*np.mean(meanfr_base)
            else:
                meanfr_base = np.sum(np.sum(grids2,axis=1)/bins,axis=1)
                meanfr = np.sum(np.sum(grids,axis=1)/bins,axis=1)
                meanfr = meanfr/np.mean(meanfr)*np.mean(meanfr_base)
        gr2 = np.sum(grids, axis=2)
        gr60 = np.abs(np.sum(gr2 * np.exp(-6j*np.pi/180*phi[:, :, 0])))/np.size(gr2)
        gr60_path = np.abs(np.sum(np.exp(-6j*np.pi/180*phi[:, :, 0])))/np.size(gr2)
        gr0 = np.sum(gr2)/np.size(gr2)

    else: # random walk trajectory
        tmax = 9000
        dt = 0.1
        trajec = traj(dt,tmax,sp=speed,dphi = 5)
        if False: #repsuppcell or repsupppop:
            meanfr_base = gridpop(oxr,oyr,phbins,trajec,repsuppcell=False,repsupppop=False,tau_rep=tau_rep,w_rep=w_rep, conj = conj, propconj=propconj, kappa=kappa, jitter=jitter)
            meanfr, gr60, gr60_path, gr0 = gridpop(oxr,oyr,phbins,trajec,repsuppcell=repsuppcell,repsupppop=repsupppop,tau_rep=tau_rep,w_rep=w_rep, conj = conj, propconj=propconj, kappa=kappa, jitter=jitter)
            meanfr = meanfr/np.mean(meanfr)*np.mean(meanfr_base)
        else:
            meanfr, gr60, gr60_path, gr0 = gridpop(oxr,oyr,phbins,trajec,repsuppcell=repsuppcell,repsupppop=repsupppop,tau_rep=tau_rep,w_rep=w_rep, conj = conj, propconj=propconj, kappa=kappa, jitter=jitter)

    # detect 60deg periodicity, use DC as baseline
    # fr2 = np.tile(meanfr,3) # tile to account for periodicity
    # power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
    # ff = np.linspace(0,1./(2.), 3*phbins//2)

    # #plt.figure()
    # #plt.plot(ff,power)
    # #plt.xticks(np.linspace(0,ff[-1],10), [str(1./ii) for ii in np.linspace(0,ff[-1],10)])

    # gr60 = power[np.argmin(abs(ff-1./60*360/phbins))]# / np.mean(meanfr) # pick index which is closest to 60deg
    # gr90 = power[np.argmin(abs(ff-1./90*360/phbins))]
    # ph = np.angle(np.fft.fft(fr2))[:len(fr2)//2]
    # p60 = ph[np.argmin(abs(ff-1./60*360/phbins))]/2/np.pi*60
    # p90 = ph[np.argmin(abs(ff-1./90*360/phbins))]/2/np.pi*90

    #gr60_rel = gr60 / power[0] gr60
    #gr60_alt = meanfr[60]/mean(meanfr) # should be similar, but more local measure

    # plot example
    if plotres:
        plt.figure(figsize=(6,10))
        plt.subplot(3,1,1)
        plt.scatter(ox+0.5*oy,np.sqrt(3)/2*oy)
        plt.plot([0,1],[0,0],'k--')
        plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
        plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
        plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
        plt.xlim([-0.05,1.5])
        plt.ylim([-0.05,1])
        plt.xlabel('Grid phases')

        ax = plt.subplot(3,1,2)
        y_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(y_formatter)
        plt.plot(np.linspace(0,360,phbins),meanfr)
        plt.xticks(np.arange(0,361,60))
        plt.xlabel('Direction of movement (deg)')
        plt.ylabel('Integrated population firing rate (spk/s)')
        plt.subplot(3,1,3)
        plt.plot(ff,power)
        plt.arrow(1./60,max(power),0,-max(power)/2,head_length=max(power)/5)
        plt.xlim([0,0.1])
        plt.xlabel('Frequency (1/deg)')
        plt.ylabel('Power')
        plt.xticks([0,1/120,1/60,1/30,1/20,1/10],['0','1/120','1/60','1/30','1/20','1/10'])
        titlestr = ''
        if mode == 'clust':
            titlestr += '%i cells, clustered in square' %N
        elif mode == 'clustcirc':
            titlestr += '%i cells, clustered in a circle' %N
        elif mode == 'clustvm':
            titlestr += '%i cells, vm clustered' %N
        elif mode == 'uniform':
            titlestr += '%i cells, uniformly distributed' %N
        elif mode == 'randi':
            titlestr += '%i cells, randomly distributed' %N
        else:
            titlestr += '1 grid cell'

        if conj:
            titlestr += '; %d' %(propconj*100) + r'$\%$' + ' conjunctive'

        titlestr += '\n H = %f' %gr60
        plt.suptitle(titlestr)

    return gr60, gr60_path, gr0, gr60#gr90, p60, p90

#gr60 = gridpop_meanfr(rmax = 3, mode = 'clustvm', path = '', N=100, dphi = 0.1, speed = 10, kappacl = 10, conj=False, repsuppcell = False, tau_rep = 3.5, w_rep = 50, plotres = True)
#gr602 = gridpop_meanfr(rmax = 3, mode = 'uniform', path = '', dphi = 0.1, speed = 10, N = 1, kappacl = 5, conj=False, repsuppcell = False, tau_rep = 3.5, w_rep = 50, plotres = True)

#gr60_1 = gridpop_meanfr(rmax = 300, bins=500, mode = 'clust', clsize=0, path = 'linear', meanoff=(0.4,0.3), N=1, conj=False, repsuppcell = False, plotres = True)

#gr60_2 = gridpop_meanfr(rmax = 90, bins=1000, mode = 'clustvm', path = 'linear', N=100, conj=False, repsuppcell = False, plotres = True)
#gr60_2 = gridpop_meanfr(rmax = 90, bins=800, mode = 'clustvm', path = 'linear', N=100, conj=False, repsuppcell = False, plotres = True)
#gr60_2 = gridpop_meanfr(rmax = 90, bins=500, mode = 'clustvm', path = 'linear', N=100, conj=False, repsuppcell = False, plotres = True)
#gr60_3 = gridpop_meanfr(rmax = -3, bins=100, mode = 'uniform', path = '', N=100, conj=True, repsuppcell = False, plotres = True)

# conserve firing rate in direction plot
# trajectory zeigen

# mittlere aktivitaet constant halten in direction plot

#%%
# summary bar plot for the different hypotheses and trajectories
Ncells = 100
rep = 2
i = 0
h_cl_star = np.zeros((3, rep))
h_cl_rw = np.zeros((3, rep))
h_cl_pl = np.zeros((3, rep))
h_conj_star = np.zeros((3, rep))
h_conj_rw = np.zeros((3, rep))
h_conj_pl = np.zeros((3, rep))
h_rep_star = np.zeros((3, rep))
h_rep_rw = np.zeros((3, rep))
h_rep_pl = np.zeros((3, rep))
while i<rep:
    print(i)
    h_cl_star[:, i] = gridpop_meanfr(rmax=300, bins=1000, mode='clustvm', path='linear', N=Ncells, conj=False, repsuppcell = False, plotres = False)[:3]
    h_cl_rw[:, i] = gridpop_meanfr(rmax=300, bins=1000, mode='clustvm', path='', N=Ncells, conj=False, repsuppcell = False, plotres = False)[:3]
    h_conj_star[:, i] = gridpop_meanfr(rmax=300, bins=1000, mode='uniform', path='linear', N=Ncells, conj=True, repsuppcell = False, plotres = False)[:3]
    h_conj_rw[:, i] = gridpop_meanfr(rmax=300, bins=1000, mode='uniform', path='', N=Ncells, conj=True, repsuppcell = False, plotres = False)[:3]
    h_rep_star[:, i] = gridpop_meanfr(rmax=300, bins=1000, mode='uniform', path='linear', N=Ncells, conj=False, repsuppcell = True, plotres = False)[:3]
    h_rep_rw[:, i] = gridpop_meanfr(rmax=300, bins=1000, mode='uniform', path='', N=Ncells, conj=False, repsuppcell = True, plotres = False)[:3]

    phbins = 360
    tau_rep = 5.
    w_rep = 1.
    ox,oy = np.meshgrid(np.linspace(0, 1, int(np.sqrt(Ncells)), endpoint=False), np.linspace(0, 1, int(np.sqrt(Ncells)), endpoint=False))
    ox = ox.reshape(1,-1)[0]
    oy = oy.reshape(1,-1)[0]
    oxr,oyr = convert_to_rhombus(ox,oy)
    tmax = 720000
    dt = 30
    trajec = traj(dt, tmax, sp=10, dphi=20, pwlin=True)
    h_rep_pl[:, i] = gridpop(oxr, oyr, phbins, trajec, repsuppcell=True, tau_rep=tau_rep, w_rep=w_rep)[1:]
    # fr2 = np.tile(meanfr,3) # tile to account for periodicity
    # power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]# / np.mean(meanfr)
    # ff = np.linspace(0,1./(2.), 3*phbins//2)
    # h_rep_pl[i] = power[np.argmin(abs(ff-1./60*360/phbins))]
    
    # redo figures for 1024 cells
    
    
    # plt.figure()
    # plt.plot(meanfr)
    # plt.title(h_rep_pl[0])

    h_conj_pl[:, i] = gridpop(oxr, oyr, phbins, trajec, conj=True)[1:]
    # fr2 = np.tile(meanfr2,3) # tile to account for periodicity
    # power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]# / np.mean(meanfr2)
    # ff = np.linspace(0,1./(2.), 3*phbins//2)
    # h_conj_pl[i] = power[np.argmin(abs(ff-1./60*360/phbins))]

    meanoff = (0, 0) #(0.4,0.3)
    kappacl = 10
    ox, oy = np.random.vonmises(2*np.pi*(meanoff[0]-0.5), kappacl, Ncells)/2./np.pi + 0.5, np.random.vonmises(2*np.pi*(meanoff[1]-0.5), kappacl, Ncells)/2./np.pi + 0.5
    oxr,oyr = convert_to_rhombus(ox,oy)
    h_cl_pl[:, i] = gridpop(oxr, oyr, phbins, trajec)[1:]
    # fr2 = np.tile(meanfr3,3) # tile to account for periodicity
    # power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]# / np.mean(meanfr3)
    # ff = np.linspace(0,1./(2.), 3*phbins//2)
    # h_cl_pl[i] = power[np.argmin(abs(ff-1./60*360/phbins))]
    i+=1

from prettytable import PrettyTable

fulltable = PrettyTable(["", "A_6", "A_0", "A_6/A_0", "M_6"])
fulltable.add_row(["Conj star", str(np.round(np.mean(h_conj_star[0, :]), 0)), str(np.round(np.mean(h_conj_star[2, :]), 0)), str(np.round(np.mean(h_conj_star[0, :])/np.mean(h_conj_star[2, :]), 3)), str(np.round(np.mean(h_conj_star[1, :]), 3))])
fulltable.add_row(["Conj pl", str(np.round(np.mean(h_conj_pl[0, :]), 0)), str(np.round(np.mean(h_conj_pl[2, :]), 0)), str(np.round(np.mean(h_conj_pl[0, :])/np.mean(h_conj_pl[2, :]), 3)), str(np.round(np.mean(h_conj_pl[1, :]), 3))])
fulltable.add_row(["Conj rw", str(np.round(np.mean(h_conj_rw[0, :]), 0)), str(np.round(np.mean(h_conj_rw[2, :]), 0)), str(np.round(np.mean(h_conj_rw[0, :])/np.mean(h_conj_rw[2, :]), 3)), str(np.round(np.mean(h_conj_rw[1, :]), 3))])
fulltable.add_row(["Clus star", str(np.mean(np.round(h_cl_star[0, :]), 0)), str(np.round(np.mean(h_cl_star[2, :]), 0)), str(np.round(np.mean(h_cl_star[0, :])/np.mean(h_cl_star[2, :]), 3)), str(np.round(np.mean(h_cl_star[1, :]), 3))])
fulltable.add_row(["Clus pl", str(np.round(np.mean(h_cl_pl[0, :]), 0)), str(np.round(np.mean(h_cl_pl[2, :]), 0)), str(np.round(np.mean(h_cl_pl[0, :])/np.mean(h_cl_pl[2, :]), 3)), str(np.round(np.mean(h_cl_pl[1, :]), 3))])
fulltable.add_row(["Clus rw", str(np.round(np.mean(h_cl_rw[0, :]), 0)), str(np.round(np.mean(h_cl_rw[2, :]), 0)), str(np.round(np.mean(h_cl_rw[0, :])/np.mean(h_cl_rw[2, :]), 3)), str(np.round(np.mean(h_cl_rw[1, :]), 3))])
fulltable.add_row(["Rep star", str(np.round(np.mean(h_rep_star[0, :]), 0)), str(np.round(np.mean(h_rep_star[2, :]), 0)), str(np.round(np.mean(h_rep_star[0, :])/np.mean(h_rep_star[2, :]), 3)), str(np.round(np.mean(h_rep_star[1, :]), 3))])
fulltable.add_row(["Rep pl", str(np.round(np.mean(h_rep_pl[0, :]), 0)), str(np.round(np.mean(h_rep_pl[2, :]), 0)), str(np.round(np.mean(h_rep_pl[0, :])/np.mean(h_rep_pl[2, :]), 3)), str(np.round(np.mean(h_rep_pl[1, :]), 3))])
fulltable.add_row(["Rep rw", str(np.round(np.mean(h_rep_rw[0, :]), 0)), str(np.round(np.mean(h_rep_rw[2, :]), 0)), str(np.round(np.mean(h_rep_rw[0, :])/np.mean(h_rep_rw[2, :]), 3)), str(np.round(np.mean(h_rep_rw[1, :]), 3))])

print(fulltable)

import pickle
pickfile = open('/home/eric/Desktop/gridBOLD/circle_150.pkl', 'rb')
dat = pickle.load(pickfile)
pickfile.close()

Ncells = 1024
ox,oy = np.meshgrid(np.linspace(0, 1, int(np.sqrt(Ncells)), endpoint=False), np.linspace(0, 1, int(np.sqrt(Ncells)), endpoint=False))
ox = ox.reshape(1,-1)[0]
oy = oy.reshape(1,-1)[0]
oxr,oyr = convert_to_rhombus(ox,oy)
print(gridpop(oxr, oyr, phbins, dat, conj=True, propconj = 0.33, kappa = 50., jitter = 10)[1:])

# upload code to server, run 100x, look at statistics and produce summary figure

plt.figure()
plt.bar([0, 1, 2, 3.5, 4.5, 5.5, 7, 8, 9],
        [np.mean(h_conj_star[0, :]), np.mean(h_conj_pl[0, :]), np.mean(h_conj_rw[0, :]),
          np.mean(h_cl_star[0, :]), np.mean(h_cl_pl[0, :]), np.mean(h_cl_rw[0, :]),
          np.mean(h_rep_star[0, :]), np.mean(h_rep_pl[0, :]), np.mean(h_rep_rw[0, :])], color='b')
plt.plot(np.zeros(rep), h_conj_star[0, :], 'k.')
plt.plot(np.ones(rep), h_conj_pl[0, :], 'k.')
plt.plot(2*np.ones(rep), h_conj_rw[0, :], 'k.')
plt.plot(3.5*np.ones(rep), h_cl_star[0, :], 'k.')
plt.plot(4.5*np.ones(rep), h_cl_pl[0, :], 'k.')
plt.plot(5.5*np.ones(rep), h_cl_rw[0, :], 'k.')
plt.plot(7*np.ones(rep), h_rep_star[0, :], 'k.')
plt.plot(8*np.ones(rep), h_rep_pl[0, :], 'k.')
plt.plot(9*np.ones(rep), h_rep_rw[0, :], 'k.')
plt.xticks([0, 1, 2, 3.5, 4.5, 5.5, 7, 8, 9], ['star', 'p-l \n Conjunctive', 'rand',
                                                'star', 'p-l \n Clustering', 'rand',
                                                'star', 'p-l \n Repetition suppression', 'rand'])
plt.bar([0, 1, 2, 3.5, 4.5, 5.5, 7, 8, 9],
        [np.mean(h_conj_star[1, :]*h_conj_star[2, :]), np.mean(h_conj_pl[1, :]*h_conj_pl[2, :]), np.mean(h_conj_rw[1, :]*h_conj_rw[2, :]),
          np.mean(h_cl_star[1, :]*h_cl_star[2, :]), np.mean(h_cl_pl[1, :]*h_cl_pl[2, :]), np.mean(h_cl_rw[1, :]*h_cl_rw[2, :]),
          np.mean(h_rep_star[1, :]*h_rep_star[2, :]), np.mean(h_rep_pl[1, :]*h_rep_pl[2, :]), np.mean(h_rep_rw[1, :]*h_rep_rw[2, :])], color='b', edgecolor='k')
plt.ylabel('H')
plt.yscale('log')

# same figure for realistic parameter choices

# data = np.array([h_conj_star, h_conj_pl, h_conj_rw,
#                  h_cl_star, h_cl_pl, h_cl_rw,
#                  h_rep_star, h_rep_pl, h_rep_rw]).T
# with open('summary_bar_plot_3hyp3traj.txt', 'a') as f:
#     for x in data:
#         f.write(str(x)+'\n')


# phase dependence and radius dependence of clustering and repsupp

#%% size of the arena as an alternative hypothesis?
rmaxs = np.linspace(1,50,100)
gr60s = np.zeros(np.shape(rmaxs))
rep = 5
for ir, rmax in enumerate(rmaxs):
    print("Step %d" %ir)
    count = 0
    griddi = np.zeros(rep)
    while count<rep:
        if count%20 == 0:
            print("Rep %d" %count)
        griddi[count],dummy = gridpop_meanfr(rmax = rmax, mode = 'uniform', path = 'straight', N = 25, conj=False, repsuppcell = False, plotres = False)
        count+=1

    gr60s[ir] = np.mean(griddi[~np.isnan(griddi)])
plt.figure()
plt.plot(rmaxs,gr60s)
plt.xlabel('Radius of the environment (grid scales)')
plt.ylabel('Hexasymmetry')
plt.title('25 grid cells, uniform')

#%% plot a grid example, Fig. 1A
bins = 500
X,Y = np.meshgrid(np.linspace(-2,2,bins),np.linspace(-2,2,bins))
grids = grid(X,Y,offs=(0,0))
fig1, ax1 = plt.subplots()
ax1.set_aspect('equal')
ax1.contourf(X,Y,grids)
ax1.plot([0,1],[0,0],'w--')
ax1.plot([0,0.5],[0,np.sqrt(3)/2],'w--')
ax1.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'w--')
ax1.plot([1,1.5],[0,np.sqrt(3)/2],'w--')
plt.xticks([])
plt.yticks([])

#%%
# investigate phase dependence of 60deg modulation for a single grid cell
res = 101
x = np.linspace(0,1,res,endpoint=True)
y = np.linspace(0,1,res,endpoint=True)
gr60s = np.zeros((len(x),len(y)))
gr90s = np.zeros((len(x),len(y)))
ph60 = np.zeros((len(x),len(y)))
ph90 = np.zeros((len(x),len(y)))
for ix,xx in enumerate(x):
    print(xx)
    for iy,yy in enumerate(y):
        gr60,gr90,phf60,phf90 = gridpop_meanfr(rmax = 300, N=1, mode = '', path = 'linear', meanoff=(xx,yy), plotres = False)
        gr60s[ix,iy] = gr60
        ph60[ix,iy] = phf60
        ph90[ix,iy] = phf90
        gr90s[ix,iy] = gr90

X,Y = np.meshgrid(x,y)
Xc,Yc = convert_to_rhombus(Y,X)
fig = plt.figure(figsize=(16,10))
ax = fig.add_subplot(221)
pl1 = ax.pcolormesh(Xc,Yc,gr60s)
fig.colorbar(pl1)
ax.set_aspect('equal')
plt.title('60deg symmetry; <S60> = '+ str(np.round(np.mean(gr60s),4)))
plt.xlabel('Grid phase')
ax = fig.add_subplot(223)
pl3 = ax.pcolormesh(Xc,Yc,ph60,cmap='twilight_shifted',vmin=-30, vmax=30)
fig.colorbar(pl3)
ax.set_aspect('equal')
plt.title('Phase of 60deg periodicity')
plt.xlabel('Grid phase')
ax = fig.add_subplot(222)
pl2 = ax.pcolormesh(Xc,Yc,gr90s)
fig.colorbar(pl2)
ax.set_aspect('equal')
plt.title('90deg symmetry; <S90> = '+ str(np.round(np.mean(gr90s),4)))
plt.xlabel('Grid phase')
ax = fig.add_subplot(224)
pl3 = ax.pcolormesh(Xc,Yc,ph90,cmap='twilight_shifted',vmin=-45, vmax=45)
fig.colorbar(pl3)
ax.set_aspect('equal')
plt.title('Phase of 90deg periodicity')
plt.xlabel('Grid phase')

with open('phasedependence_gr60.txt', 'a') as f:
    for x in gr60s:
        f.write(str(x)+'\n')
with open('phasedependence_gr90.txt', 'a') as f:
    for x in gr90s:
        f.write(str(x)+'\n')
with open('phasedependence_ph60.txt', 'a') as f:
    for x in ph60:
        f.write(str(x)+'\n')
with open('phasedependence_ph90.txt', 'a') as f:
    for x in ph60:
        f.write(str(x)+'\n')



#%% clusters at different phases - runs for some minutes
x = np.linspace(0,1,21)
y = np.linspace(0,1,21)
gr60s = np.zeros((len(x),len(y)))
gr90s = np.zeros((len(x),len(y)))
for ix,xx in enumerate(x):
    for iy,yy in enumerate(y):
        print(ix,iy)
        gr60,gr90,gr60r = gridpop_meanfr(rmax = 3, N=25, mode = 'clustvm', meanoff=(xx,yy), plotres = False)
        gr60s[ix,iy] = gr60
        gr90s[ix,iy] = gr90

X,Y = np.meshgrid(x,y)
Xc,Yc = convert_to_rhombus(Y,X)
fig = plt.figure()
ax = fig.add_subplot(121)
pl1 = ax.pcolormesh(Xc,Yc,gr60s)
fig.colorbar(pl1)
ax.set_aspect('equal')
plt.title('60deg symmetry for a single grid cell')
plt.xlabel('Grid phase')
ax = fig.add_subplot(122)
pl2 = ax.pcolormesh(Xc,Yc,gr90s)
fig.colorbar(pl2)
ax.set_aspect('equal')
plt.title('90deg symmetry for a single grid cell')
plt.xlabel('Grid phase')

#%% vary number of cells in a cluster (for one given phase) and measure hexasymmetry - runs for some minutes
Ns = np.logspace(0,3,100)
gr60s = np.zeros(np.shape(Ns))
for iN,N in enumerate(Ns):
        print(iN)
        gr60s[iN] = gridpop_meanfr(rmax = 3, mode = 'clust', N = N, meanoff=(0.4,0.3), plotres = False)

plt.figure()
plt.plot(Ns,gr60s)
plt.xlabel('Number of cells in a cluster (constant cluster size)')
plt.ylabel('Fourier component at 60deg')

#%% vary cluster size, square clusters
clsizes = np.linspace(0,0.8,100)
gr60s = np.zeros(np.shape(clsizes))
for ic,clsize in enumerate(clsizes):
        print(ic)
        gr60s[ic] = gridpop_meanfr(rmax = 3, mode = 'clust', N = 100, clsize = clsize, meanoff=(0.4,0.3), plotres = False)

plt.figure()
plt.plot(clsizes,gr60s)
plt.xlabel('Cluster size (phase) - 100 grid cells')
plt.ylabel('Fourier component at 60deg')


#%% vary cluster size, but the clusters are now circles - should give the same result as for the squares above
clsizes = np.linspace(0,0.8,100)
gr60s = np.zeros(np.shape(clsizes))
for ic,clsize in enumerate(clsizes):
        print(ic)
        gr60s[ic] = gridpop_meanfr(rmax = 3, mode = 'clustcirc', N = 100, clsize = clsize, meanoff=(0.4,0.3), plotres = False)

plt.figure()
plt.plot(clsizes,gr60s)
plt.xlabel('Cluster size (phase) - 100 grid cells')
plt.ylabel('Fourier component at 60deg')

#%% vary cluster size for von Mises cluster - runs for a loooong time
kappas = np.linspace(0,50,50)
meangr = np.zeros(np.shape(kappas))
maxgr = np.zeros(np.shape(kappas))
mingr = np.zeros(np.shape(kappas))
rep = 50
for ik,kappa in enumerate(kappas):
        print(ik)
        count = 0
        sumgr = 0
        magr = 0
        migr = 100
        while count<rep:
            res = gridpop_meanfr(rmax = 3, mode = 'clustvm', N = 100, meanoff=(0.4,0.3), kappacl = kappa, plotres = False)
            sumgr += res
            magr = max(magr, res)
            migr = min(migr, res)
            count += 1
        meangr[ik] = sumgr/rep
        maxgr[ik] = magr
        mingr[ik] = migr

plt.figure()
plt.plot(kappas,meangr,'k')
plt.plot(kappas, maxgr, 'b--')
plt.plot(kappas, mingr, 'b--')
plt.xlabel('Kappa - 100 grid cells')
plt.ylabel('Fourier component at 60deg')

#%% ------------------------------------------
# investigate Richard's fluctuation hypothesis
Ns = np.logspace(0,2,100)
meangr = np.zeros(np.shape(Ns))
maxgr = np.zeros(np.shape(Ns))
mingr = np.zeros(np.shape(Ns))
meangr_rel = np.zeros(np.shape(Ns))
maxgr_rel = np.zeros(np.shape(Ns))
mingr_rel = np.zeros(np.shape(Ns))
rep = 200
for iN,N in enumerate(Ns):
        print(iN)
        count = 0
        sumgr = 0
        magr = 0
        migr = 100
        sumgr_rel = 0
        magr_rel = 0
        migr_rel = 100
        while count<rep:
            res, res_rel = gridpop_meanfr(rmax = 3,mode = 'randi',N = int(N), conj=False, plotres = False)
            sumgr += res
            magr = max(magr, res)
            migr = min(migr, res)
            sumgr_rel += res_rel
            magr_rel = max(magr_rel, res_rel)
            migr_rel = min(migr_rel, res_rel)
            count += 1
        meangr[iN] = sumgr/rep
        maxgr[iN] = magr
        mingr[iN] = migr
        meangr_rel[iN] = sumgr_rel/rep
        maxgr_rel[iN] = magr_rel
        mingr_rel[iN] = migr_rel

plt.figure()
plt.plot(Ns,meangr[meangr>0],'k')
plt.plot(Ns, maxgr[meangr>0], 'b--')
plt.plot(Ns, mingr[meangr>0], 'b--')
#plt.plot(Ns, meangr[0]/np.sqrt(Ns))
#plt.plot(Ns, 1.2*maxgr[0]/np.sqrt(Ns))
plt.legend(['Mean','Max','Min','sqrt(N)'])
plt.xlabel('Number of cells - random phases')
plt.ylabel('Hexasymmetry')


#%% introduce head-direction tuning (conjunctive cell hypothesis) - runs for a while
Ns = np.logspace(0,2,50)
gr60s_Ns = np.zeros(np.shape(Ns))
rep = 50
for iN,N in enumerate(Ns):
        print(iN)
        count = 0
        sumgr = 0
        while count<rep:
            sumgr += gridpop_meanfr(rmax = 3,mode = 'clustcirc',N = int(N), clsize= 0.1, meanoff=(0.4,0.3), conj=True, plotres = False)[0]
            count += 1
        gr60s_Ns[iN] = sumgr/rep

plt.figure()
plt.plot(Ns,gr60s_Ns)
plt.xlabel('N')
plt.ylabel('H')

#%%
# scaling laws for the different hypotheses ... runs for some minutes
Ns = np.logspace(0, 2, 20)
gr60s_cl = np.zeros(np.shape(Ns))
gr60s_cl2 = np.zeros(np.shape(Ns))
gr60s_conj = np.zeros(np.shape(Ns))
gr60s_fluct = np.zeros(np.shape(Ns))
gr60s_rep = np.zeros(np.shape(Ns))
rep = 100
for iN, N in enumerate(Ns):
    print(int(N))
    count = 0
    sumgr_cl = 0
    sumgr_cl2 = 0
    sumgr_conj = 0
    sumgr_fluct = 0
    sumgr_rep = 0
    while count<rep:
        sumgr_cl += gridpop_meanfr(rmax = 90, mode = 'clustvm', N = int(N), meanoff=(0, 0), plotres = False)[0]
        sumgr_cl2 += gridpop_meanfr(rmax = 90, mode = 'clustvm', N = int(N), kappacl = 100000, meanoff=(0, 0), plotres = False)[0]
        sumgr_conj += gridpop_meanfr(rmax = 90, mode = 'uniform', N = int(N), conj=True, kappa=40, jitter=1, plotres = False)[0]
        sumgr_fluct += gridpop_meanfr(rmax = 90, mode = 'randi', N = int(N), conj=False, plotres = False)[0]
        sumgr_rep += gridpop_meanfr(rmax = 90, mode = 'uniform', N = int(N), conj=False, repsuppcell = True, plotres = False)[0]
        count += 1
    gr60s_cl[iN] = sumgr_cl/rep
    gr60s_cl2[iN] = sumgr_cl2/rep
    gr60s_conj[iN] = sumgr_conj/rep
    gr60s_fluct[iN] = sumgr_fluct/rep
    gr60s_rep[iN] = sumgr_rep/rep

plt.figure()
plt.plot(Ns, gr60s_conj, 'o-')
plt.plot(Ns, gr60s_cl, 'o-')
plt.plot(Ns, gr60s_cl2, 'o-')
plt.plot(Ns, gr60s_fluct, 'o-')
plt.plot(Ns, gr60s_rep, 'o-')
plt.plot(Ns, np.sqrt(Ns), 'k--')
plt.plot(Ns, Ns, 'k--')
plt.plot(Ns, Ns**(3/2), 'k--')
plt.xlabel('N')
plt.ylabel('H')
plt.xscale('log')
plt.yscale('log')
plt.legend(['Conj', 'Clust', 'Clust perfect', 'Fluct', 'Rep supp'])


# same thing for random walks
# add phase dependence for rep supp in figure

#%% vary kappa and sigma of the head-direction modulation - no clustering
# for 10 000 cells now ... runs quite long
nn = 21
kappamax = 50
sigmamax = 20
kappas = np.linspace(0,kappamax,nn)
stds = np.linspace(0,sigmamax,nn)
gr60s = np.zeros((len(kappas),len(stds)))
for ik,kappa in enumerate(kappas):
    print(ik)
    for iis,std in enumerate(stds):
        gr60s[ik,iis], gr90, phase60,phase90 = gridpop_meanfr(rmax = -3,path='',mode = 'uniform',N = 1e4, conj=True, propconj = 1, kappa = kappa, jitter = std, plotres = False)

plt.figure()
plt.imshow(gr60s.T,origin='lower')
plt.xticks(np.linspace(0,nn-1,5), [str(i) for i in np.linspace(0,kappamax,5)])
plt.yticks(np.linspace(0,nn-1,5), [str(i) for i in np.linspace(0,sigmamax,5)])
plt.xlabel('Kappa')
plt.ylabel('sigma (deg)')
plt.title("100 grid cells, uniformly distributed phases")
plt.colorbar()
plt.scatter(2./kappamax*nn,10./sigmamax*nn,c='white')

# store data
with open('conjunctive_parameters_data_10000cells.txt', 'a') as f:
    for x in gr60s:
        for elem in x:
            f.write(str(elem)+', ')
        f.write(' \n')

#%%
# linear curve - too boring to show in figure
propconjs = np.linspace(0,1,100)
gr60s = np.zeros(np.shape(propconjs))
gr60s_rel = np.zeros(np.shape(propconjs))
for ip,propconj in enumerate(propconjs):
        print(ip)
        gr60s[ip], gr90, gr60s_rel[ip] = gridpop_meanfr(rmax = -3,path='',mode = 'uniform',N = 100, conj=True, propconj = propconj, kappa = 50, jitter = 0, plotres = False)

plt.figure()
plt.plot(propconjs,gr60s)
#plt.plot(propconjs,gr60s_rel)
plt.xlabel('Proportion of conjunctive cells')
plt.ylabel('Hexasymmetry')
plt.title("100 grid cells, uniformly distributed phases")

#a, b = gridpop_meanfr(rmax = 3,path='straight',mode = 'randi',N = 100, conj=False, propconj = 1, kappa = 50, jitter = 0, plotres = True)
#a, b = gridpop_meanfr(rmax = 3,path='',mode = 'randi',N = 100, conj=False, propconj = 1, kappa = 50, jitter = 0, plotres = True)


#%% Figure about conjunctive cells
# circular standard deviation instead of 1/kappa
# np.sqrt(-2 * np.log(np.sqrt(sum(Sin2)**2 + sum(Cos2)**2) / len(Rad2)))
# using Gauss sigma**2 = 1/kappa now

# use higher cell number -> 10^4
# vergleichbarkeit meanfr plots, mittlere feuerrate anpassen

# kappa auf der oberen achse zeigen

fs = 16
plt.figure(figsize=(20,10))
ax = plt.subplot(2,4,1)
X,Y = np.meshgrid(np.linspace(-50,50,1000),np.linspace(-50,50,1000))
gr = 20*grid(X,Y,grsc=30, angle=0, offs=np.array([0,0]))
plt.pcolor(X,Y,gr,vmin=0,vmax=20)
c = plt.colorbar(ticks=[0,10,20])
c.set_label('Firing rate (spk/s)',fontsize=fs)
c.ax.tick_params(labelsize=fs)
plt.arrow(0,0,15,np.sqrt(3)/2*30,color='r',length_includes_head=True,head_width = 5)
ax.set_aspect('equal')
plt.xlabel('x',fontsize=fs)
plt.ylabel('y',fontsize=fs)
plt.xticks([])
plt.yticks([])

ax2 = plt.subplot(2,4,2, projection='polar')
plt.rc('xtick', labelsize=fs)
plt.rc('ytick', labelsize=fs)
mu = 30
kappa = 50
direc = np.linspace(0,2*np.pi,360)
r = np.exp(kappa*np.cos(direc-np.pi/180.*mu))/(2*np.pi*special.i0(kappa))
plt.plot(direc,r/np.mean(r)*np.mean(gr),'k',lw=2)
plt.arrow(0,0,np.pi/3,15,color='r',width = 0.015,length_includes_head=True)
#ax2.set_rmax(2)
ax2.set_rticks([5, 10, 15, 20])
ax2.set_rlabel_position(-22.5)
ax2.grid(True)

ax3 = plt.subplot(3,2,2)
tmax = 3600
dt = 0.1
trajec = traj(dt,tmax,sp=10,dphi = 1)
N = 1e2
phbins = 360
ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
ox = ox.reshape(1,-1)[0]
oy = oy.reshape(1,-1)[0]
oxr, oyr = convert_to_rhombus(ox,oy)
meanfr1, gr60, gr60_path = gridpop(oxr,oyr,phbins,trajec,conj = True, propconj=1, kappa=40, jitter=1)
plt.plot(np.linspace(0,360,360), meanfr1, 'k')
plt.xticks([0,60,120,180,240,300,360],[])
plt.yticks(fontsize=fs)
#plt.xlabel('Movement direction (deg)',fontsize=fs)
#plt.ylabel('Total firing rate (spk/s)',fontsize=fs)
plt.title('n='+str(int(N))+' cells, kappa=40, sigma=1deg',fontsize=fs)
plt.text(280,1000,'H = '+str(np.round(gr60,1)),fontsize=fs)#,transform=ax.transAxes)
plt.ylim([0,1.1*max(meanfr1)])

ax4 = plt.subplot(3,2,4)
tmax = 3600
dt = 0.1
trajec = traj(dt,tmax,sp=10,dphi = 1)
N = 1e2
phbins = 360
ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
ox = ox.reshape(1,-1)[0]
oy = oy.reshape(1,-1)[0]
oxr, oyr = convert_to_rhombus(ox,oy)
meanfr2, gr60, gr60_path = gridpop(oxr,oyr,phbins,trajec,conj = True, propconj=1, kappa=30, jitter=15)
plt.plot(np.linspace(0,360,360), meanfr2, 'k')
plt.xticks([0,60,120,180,240,300,360],[])
plt.yticks(fontsize=fs)
#plt.xlabel('Movement direction (deg)',fontsize=fs)
#plt.ylabel('Total firing rate (spk/s)',fontsize=fs)
plt.title('n='+str(int(N))+' cells, kappa=30, sigma=15deg',fontsize=fs)
plt.text(280,500,'H = '+str(np.round(gr60,1)),fontsize=fs)#,transform=ax.transAxes)
plt.ylim([0,1.1*max(meanfr2)])


ax5 = plt.subplot(3,2,6)
tmax = 3600
dt = 0.1
trajec = traj(dt,tmax,sp=10,dphi = 1)
ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
ox = ox.reshape(1,-1)[0]
oy = oy.reshape(1,-1)[0]
oxr, oyr = convert_to_rhombus(ox,oy)
meanfr3, gr60, gr60_path = gridpop(oxr,oyr,phbins,trajec,conj = True, propconj=1, kappa=10, jitter=18)
plt.plot(np.linspace(0,360,360), meanfr3, 'k')
plt.xticks([0,60,120,180,240,300,360],fontsize=fs)
plt.yticks(fontsize=fs)
plt.xlabel('Movement direction (deg)',fontsize=fs)
plt.ylabel('Total firing rate (spk/s)',fontsize=fs)
plt.title('n='+str(int(N))+' cells, kappa=10, sigma=15deg', fontsize=fs)
plt.text(280,500,'H = '+str(np.round(gr60,1)),fontsize=fs)#,transform=ax.transAxes)
plt.ylim([0,1.1*max(meanfr3)])


# richtige sigmas im titel angeben


ax5 = plt.subplot(2,2,3)
nn = 21
kappamax = 50
sigmamax = 20
kappas = np.linspace(0,kappamax,nn)
stds = np.linspace(0,sigmamax,nn)
kappa_data = np.zeros((nn,nn))
f = open("/home/eric/Desktop/gridBOLD/conjunctive/conjunctive_parameters_data_10000cells.txt", "r")
for i,x in enumerate(f):
    kappa_data[i,:] = np.array([float(elem) for elem in x.split(',')[:-1]])
f.close()

K,S = np.meshgrid(np.sqrt(1/kappas)*180/np.pi,stds)
pax = plt.pcolor(K,S,kappa_data.T,vmin=0)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
plt.xlabel('HD tuning width (deg)',fontsize=fs) # sqrt(1/kappa)
plt.ylabel('Alignment jitter (deg)',fontsize=fs)
#cbar = plt.colorbar(ticks=[0,5,10])
cbar = plt.colorbar()
cbar.ax.tick_params(labelsize=fs)
cbar.set_label('Hexasymmetry',fontsize=fs)
plt.scatter(np.sqrt(1/10.)*180/np.pi,18.,c='white')
plt.scatter(np.sqrt(1/30.)*180/np.pi,15.,c='white')
plt.scatter(np.sqrt(1/40.)*180/np.pi,1.,c='white')
plt.axis([np.sqrt(1/max(kappas))*180/np.pi,20,0,20])
#plt.tight_layout()

# # maybe plot N vs H as inset - sqrt(N) for noise, N for for conjunctive
# data = np.zeros((10,3))
# f = open("/home/eric/Desktop/gridBOLD/conjunctive/conj_scalingcellnumber.txt", "r")
# for i,x in enumerate(f):
#     if x[0]!='N':
#         data[i-1,:] = np.array([float(elem) for elem in x.split(',')[:-1]])
# f.close()

# plt.figure()
# plt.plot(data[:,0],data[:,2])

#%%
# Figure about clustering hypothesis
from scipy.stats import vonmises
from mpl_toolkits.axes_grid1 import make_axes_locatable

fs= 16
kappacl = 10
meanoff = (0.,0.)
N = 1e2
ox, oy = np.random.vonmises(2*np.pi*(meanoff[0]-0.5), kappacl, int(N))/2./np.pi + 0.5, np.random.vonmises(2*np.pi*(meanoff[1]-0.5), kappacl, int(N))/2./np.pi + 0.5
fig = plt.figure(figsize=(20,10))

#ax = plt.subplot(4,5,1)
ax3 = plt.subplot(4,4,(1,2))
rmax = 300
bins = 1000
phbins = 360
Amax = 20
r,phi = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins))
X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)
oxr,oyr = convert_to_rhombus(ox,oy)
grids = Amax * grid(X,Y,grsc=30,offs=(oxr,oyr))
meanfr1 = np.sum(np.sum(grids,axis=1),axis=1)/bins
plt.plot(np.linspace(0,360,phbins),meanfr1,'k')
#plt.xlabel('Movement direction (deg)',fontsize=fs)
#plt.ylabel('Total firing \nrate (spk/s)',fontsize=fs)
plt.xticks([0,60,120,180,240,300,360],fontsize=fs)
plt.yticks(fontsize=fs)
plt.ylim([0,1.5*max(meanfr1)])
plt.text(0.2,0.8,'kappa = 10',fontsize=fs,transform=ax3.transAxes)

ax41 = inset_axes(ax3, width='15%', height='40%', loc=2)
plt.scatter(ox+0.5*oy,np.sqrt(3)/2*oy,s=5,c='k')
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
plt.xlim([-0.05,1.5])
plt.ylim([-0.05,1])
plt.xticks([])
plt.yticks([])
#plt.xlabel('Grid phases',fontsize=fs)
ax41.axis("off")
#plt.text(0.1,-0.2,'kappa = 10',fontsize=fs)
ax41.set_aspect('equal')

ax4 = inset_axes(ax3, width='20%', height='40%', loc=1)
plt.plot([0,0],[-1,1],'k')
plt.plot([-1,1],[0,0],'k')
plt.plot([-1/np.sqrt(2),1/np.sqrt(2)],[-1/np.sqrt(2),1/np.sqrt(2)],'k')
plt.plot([-1/np.sqrt(2),1/np.sqrt(2)],[1/np.sqrt(2),-1/np.sqrt(2)],'k')
ax4.set_aspect('equal')
plt.xticks([0])
plt.yticks([0])
ax4.yaxis.set_ticks_position("right")
plt.axis([-1.25,1.25,-1.25,1.25])
fr2 = np.tile(meanfr1,3) # tile to account for periodicity
power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
ff = np.linspace(0,1./(2.), 3*phbins//2)
gr60 = power[np.argmin(abs(ff-1./60*360/phbins))] # pick index which is closest to 60deg
plt.text(0.5,0.8,'H = '+str(np.round(gr60,1)),fontsize=fs,transform=ax3.transAxes)

ax5 = plt.subplot(4,4,(5,6))
phbins = 360
Amax = 20
tmax = 3600
dt = 0.1
part = 500
trajec = traj(dt,tmax,sp=8,dphi = 2)
meanfr2 = gridpop(oxr,oyr,phbins,trajec)
plt.plot(np.linspace(0,360,phbins),meanfr2,'k')
plt.xlabel('Movement direction (deg)',fontsize=fs)
plt.ylabel('Total firing \nrate (spk/s)',fontsize=fs)
plt.xticks([0,60,120,180,240,300,360],fontsize=fs)
plt.yticks(fontsize=fs)
plt.ylim([0,1.4*max(meanfr2)])

ax6 = inset_axes(ax5, width='15%', height='40%', loc=1)
plt.plot(trajec[0][:part],trajec[1][:part],'k')
ax6.set_aspect('equal')
plt.xticks([])
plt.yticks([])
fr2 = np.tile(meanfr2,3) # tile to account for periodicity
power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
ff = np.linspace(0,1./(2.), 3*phbins//2)
gr60 = power[np.argmin(abs(ff-1./60*360/phbins))] # pick index which is closest to 60deg
plt.text(0.5,0.8,'H = '+str(np.round(gr60,1)),fontsize=fs,transform=ax5.transAxes)

grsc = 30
ax7 = plt.subplot(4,4,7)
xtr,ytr,dirtr = trajec[0][:part], trajec[1][:part], trajec[2][:part]
mingridx = min(min(xtr),0)
maxgridx = max(max(xtr),1.5*grsc)
mingridy = min(min(ytr),0)
maxgridy = max(max(ytr),np.sqrt(3)/2*grsc)
X,Y = np.meshgrid(np.linspace(mingridx-10,maxgridx+10,1000),np.linspace(mingridy-10,maxgridy+10,1000))
gr = grid(X,Y,grsc=30, angle=0, offs=np.array([0,0]))
ax7.pcolor(X,Y,gr)
ax7.set_aspect('equal')
plt.xlabel('x',fontsize=fs)
plt.ylabel('y',fontsize=fs)
plt.xticks([])
plt.yticks([])
ax7.plot(xtr,ytr,'w')
tol = 5 * np.pi/180
ax7.scatter(xtr[abs(dirtr)<tol],ytr[abs(dirtr)<tol],c='r',zorder=5)
plt.plot([0,grsc],[0,0],'k--')
plt.plot([0,0.5*grsc],[0,np.sqrt(3)/2*grsc],'k--')
plt.plot([0.5*grsc,1.5*grsc],[np.sqrt(3)/2*grsc,np.sqrt(3)/2*grsc],'k--')
plt.plot([1*grsc,1.5*grsc],[0,np.sqrt(3)/2*grsc],'k--')
ax7.set_aspect('equal')

ax8 = plt.subplot(4,4,8)
allxtr,allytr = trajec[0][abs(trajec[2]<tol)], trajec[1][trajec[2]<tol]
allxr,allyr = convert_to_square(*map_to_rhombus(allxtr,allytr,grsc=grsc))
#ax = plt.gca()
dx = 0.01*grsc
xedge,yedge = np.arange(0,grsc+dx,dx), np.arange(0,grsc+dx,dx)
H,xe,ye = np.histogram2d(allxr,allyr,bins=(xedge,yedge),density=True)
H = H.T
#plt.scatter(ph1c+0.5*ph2c,np.sqrt(3)/2*ph2c,s=1,c='k')
xr,yr = convert_to_rhombus(*np.meshgrid((xe[:-1]+xe[1:])/2,(ye[:-1]+ye[1:])/2))
plt.plot([0,grsc],[0,0],'k--')
plt.plot([0,0.5*grsc],[0,np.sqrt(3)/2*grsc],'k--')
plt.plot([0.5*grsc,1.5*grsc],[np.sqrt(3)/2*grsc,np.sqrt(3)/2*grsc],'k--')
plt.plot([1*grsc,1.5*grsc],[0,np.sqrt(3)/2*grsc],'k--')
#plt.xticks([])
#plt.yticks([])
ax8.axis("off")
ax8.set_aspect('equal')
plt.xlabel('Grid phase',fontsize=fs)
im = ax8.pcolor(xr,yr,H)
divider = make_axes_locatable(ax8)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax,ticks=[H.min(),H.max()])
cb.ax.set_yticklabels(['min', 'max']) #cbar.ax.tick_params(labelsize=fs)
cb.set_label('Density',fontsize=fs)

res = 101
x = np.linspace(0,1,res,endpoint=True)
y = np.linspace(0,1,res,endpoint=True)
X,Y = np.meshgrid(x,y)
Xc,Yc = convert_to_rhombus(X,Y)

gr60s = np.array([])
f = open("phasedependence_gr60.txt", "r")
for i,x in enumerate(f):
    gr60s = np.append(gr60s, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
gr60s = gr60s.reshape((res,res))

ph60s = np.array([])
f = open("phasedependence_ph60.txt", "r")
for i,x in enumerate(f):
    ph60s = np.append(ph60s, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph60s = ph60s.reshape((res,res))

axx = fig.add_subplot(4,4,3)
ax = plt.gca()
im = ax.pcolormesh(Xc,Yc,N*gr60s)
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
ax.set_aspect('equal')
ax.axis('off')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax)
cb.set_label('H',fontsize=fs)


fig.add_subplot(4,4,4)
ax = plt.gca()
im = ax.pcolormesh(Xc,Yc,ph60s,cmap='twilight_shifted',vmin=-30, vmax=30)
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
ax.set_aspect('equal')
ax.axis('off')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax, ticks=[-30,30])
cb.set_label('Phase of\nhexasymmetry',fontsize=fs)

l = 200
ph1c = np.array([])
f = open("ph1c_gauss.txt", "r")
for i,x in enumerate(f):
    ph1c = np.append(ph1c, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph1c = ph1c.reshape((l,l))

plt.subplot(4,4,9)
ax = plt.gca()
im = ax.imshow(ph1c,origin='lower',cmap='twilight_shifted',vmin=0, vmax=1)
plt.xticks(np.linspace(0,200,4),['0','1','2','3'])
plt.yticks(np.linspace(0,200,4),['0','1','2','3'])
#plt.xlabel('x (mm)',fontsize=fs)
#plt.ylabel('y (mm)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax)
#cb.set_label('Grid phase (X)',fontsize=fs)

kernelc = np.array([])
f = open("kernel_gauss.txt", "r")
for i,x in enumerate(f):
    kernelc = np.append(kernelc, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
kernelc = kernelc.reshape((l,l))

plt.subplot(4,4,10)
plt.imshow(kernelc[50:150,50:150],origin='lower')
plt.xticks(np.linspace(0,100,4),['0','0.5','1.0','1.5'])
plt.yticks(np.linspace(0,100,4),['0','0.5','1.0','1.5'])
#plt.xlabel('x (mm)')
#plt.ylabel('y (mm)')
#plt.colorbar()

data = np.array([])
f = open("binned_phasedistance.txt", "r")
for i,x in enumerate(f):
    data = np.append(data, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
data = data.reshape((49,6))
binsc,meandiffc,stddiffc,bins_1,meandiff,stddiff = data.T

ax11 = plt.subplot(4,4,11)
plt.errorbar(binsc * 1000,meandiffc,stddiffc)
#plt.xlabel('Pairwise anatomical distance (um)',fontsize=fs)
#plt.ylabel('Pairwise phase \ndistance (2D)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
plt.xlim([0,500])
ax11.set_aspect(600)

ph2c = np.array([])
f = open("ph2c_gauss.txt", "r")
for i,x in enumerate(f):
    ph2c = np.append(ph2c, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph2c = ph2c.reshape((l,l))

ax2 = plt.subplot(4,4,12)
ax = plt.gca()
dx = 0.01
xedge,yedge = np.arange(0,1+dx,dx), np.arange(0,1+dx,dx)
H,xe,ye = np.histogram2d(np.reshape(ph1c,(1,-1))[0],np.reshape(ph2c,(1,-1))[0],bins=(xedge,yedge),density=True)
H = H.T
#plt.scatter(ph1c+0.5*ph2c,np.sqrt(3)/2*ph2c,s=1,c='k')
xr,yr = convert_to_rhombus(*np.meshgrid((xe[:-1]+xe[1:])/2,(ye[:-1]+ye[1:])/2))
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
#plt.xlim([-0.05,1.5])
#plt.ylim([-0.05,1])
#plt.xticks([])
#plt.yticks([])
ax2.axis("off")
ax2.set_aspect('equal')
plt.text(0.1,-0.2,'kappa = '+ str(np.round(vonmises.fit(2*np.pi*ph1c, fscale=1)[0],3)),fontsize=fs)
plt.xlabel('Grid phase',fontsize=fs)
im = ax.pcolor(xr,yr,H)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0)
cb = plt.colorbar(im, cax=cax, ticks=[H.min(),H.max()])
cb.ax.set_yticklabels(['min', 'max']) #cbar.ax.tick_params(labelsize=fs)
cb.set_label('Density',fontsize=fs)

ph1 = np.array([])
f = open("ph1_grid.txt", "r")
for i,x in enumerate(f):
    ph1 = np.append(ph1, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph1 = ph1.reshape((l,l))

plt.subplot(4,4,13)
ax = plt.gca()
im = ax.imshow(ph1,origin='lower',cmap='twilight_shifted',vmin=0, vmax=1)
plt.xticks(np.linspace(0,200,4),['0','1','2','3'])
plt.yticks(np.linspace(0,200,4),['0','1','2','3'])
plt.xlabel('x (mm)',fontsize=fs)
plt.ylabel('y (mm)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax)
cb.set_label('Grid phase (X)',fontsize=fs)

kernel = np.array([])
f = open("kernel_grid.txt", "r")
for i,x in enumerate(f):
    kernel = np.append(kernel, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
kernel = kernel.reshape((l,l))

plt.subplot(4,4,14)
plt.imshow(kernel[50:150,50:150],origin='lower')
plt.xticks(np.linspace(0,100,4),['0','0.5','1.0','1.5'])
plt.yticks(np.linspace(0,100,4),['0','0.5','1.0','1.5'])
plt.xlabel('x (mm)',fontsize=fs)
plt.ylabel('y (mm)',fontsize=fs)

ax12 = plt.subplot(4,4,15)
plt.errorbar(binsc * 1000,meandiff,stddiff)
plt.xlabel('Pairwise anatomical distance (um)',fontsize=fs)
plt.ylabel('Pairwise phase \ndistance (2D)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
plt.xlim([0,500])
ax12.set_aspect(600)


ph2 = np.array([])
f = open("ph2_grid.txt", "r")
for i,x in enumerate(f):
    ph2 = np.append(ph2, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph2 = ph2.reshape((l,l))

ax2 = plt.subplot(4,4,16)
ax = plt.gca()
dx = 0.01
xedge,yedge = np.arange(0,1+dx,dx), np.arange(0,1+dx,dx)
H,xe,ye = np.histogram2d(np.reshape(ph1,(1,-1))[0],np.reshape(ph2,(1,-1))[0],bins=(xedge,yedge),density=True)
H = H.T
#plt.scatter(ph1c+0.5*ph2c,np.sqrt(3)/2*ph2c,s=1,c='k')
xr,yr = convert_to_rhombus(*np.meshgrid((xe[:-1]+xe[1:])/2,(ye[:-1]+ye[1:])/2))
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
#plt.xlim([-0.05,1.5])
#plt.ylim([-0.05,1])
#plt.xticks([])
#plt.yticks([])
ax2.axis("off")
ax2.set_aspect('equal')
plt.text(0.1,-0.2,'kappa = '+ str(np.round(vonmises.fit(2*np.pi*ph1, fscale=1)[0],2)),fontsize=fs)
plt.xlabel('Grid phase',fontsize=fs)
im = ax.pcolor(xr,yr,H)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax, ticks=[H.min(),H.max()])
cb.ax.set_yticklabels(['min', 'max']) #cbar.ax.tick_params(labelsize=fs)
cb.set_label('Density',fontsize=fs)

plt.tight_layout()

#%% plot percentage of conjunctive cells vs hexasymmetry - phases clustered
propconjs = np.linspace(0,1,100)
gr60s = np.zeros(np.shape(propconjs))
for ip,propconj in enumerate(propconjs):
        print(ip)
        gr60s[ip] = gridpop_meanfr(rmax = 3,mode = 'clustvm',N = 9, clsize= 0.4, meanoff=(0.,0.), conj=True, propconj = propconj, plotres = False)

plt.figure()
plt.plot(propconjs,gr60s)
plt.xlabel('Proportion of conjunctive cells')
plt.ylabel('Fourier component at 60deg')
plt.title("9 grid cells, phases clustered")


gridpop_meanfr(rmax = 3,mode = 'uniform', N=100, clsize= 0.4, meanoff=(0.4,0.3), conj=True, propconj = 0.7, plotres = True)
gridpop_meanfr(rmax = 3,mode = 'uniform', N=100, clsize= 0.4, meanoff=(0.4,0.3), conj=True, propconj = 0.8, plotres = True)
gridpop_meanfr(rmax = 3,mode = 'uniform', N=100, clsize= 0.4, meanoff=(0.4,0.3), conj=True, propconj = 0.9, plotres = True)


#%%
gr60_lin,gr90,gr60rel = gridpop_meanfr(rmax = 300, mode = 'uniform', path = 'linear', N = 100, conj=False, repsuppcell = True, tau_rep = 5., w_rep = 1., speed = 10., plotres = True)
gr60_lin,gr90,gr60rel = gridpop_meanfr(rmax = 300, mode = 'uniform', path = '', N = 100, conj=False, repsuppcell = True, tau_rep = 5., w_rep = 1., speed = 10., plotres = True)

#%%
# Figure about repetition suppression
fs=16
Amax = 20
plt.figure(figsize=(20,8))
ax = plt.subplot(2,5,1)
X,Y = np.meshgrid(np.linspace(-50,50,1000),np.linspace(-50,50,1000))
gr = Amax*grid(X,Y,grsc=30, angle=0, offs=np.array([0,0]))
plt.pcolor(X,Y,gr,vmin=0,vmax=20)
c = plt.colorbar(ticks=[0,10,20])
c.set_label('Firing rate (spk/s)',fontsize=fs)
c.ax.tick_params(labelsize=fs)
plt.arrow(0,0,1.5*15,1.5*np.sqrt(3)/2*30,color='b',length_includes_head=True,head_width = 2)
plt.arrow(0,0,1.5*np.sin(np.pi/3)*30,1.5*np.cos(np.pi/3)*30,color='r',length_includes_head=True,head_width = 2)
ax.set_aspect('equal')
plt.xlabel('x',fontsize=fs)
plt.ylabel('y',fontsize=fs)
plt.xticks([])
plt.yticks([])

N = 1e4
tau_rep = 5.
w_rep = 1.
speed = 10
bins = 100 # 1000
rmax = 300
ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
ox = ox.reshape(1,-1)[0]
oy = oy.reshape(1,-1)[0]
oxr, oyr = convert_to_rhombus(ox,oy)
r,phi,indoff = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins), np.arange(len(ox)))
X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)
grids = Amax * grid(X,Y,offs=(oxr,oyr))[:,:,0,:]
grids2 = grids.copy()
tt = np.linspace(0,rmax/speed,bins)
for idir in range(phbins):
    for ic in range(N):
        v = adap_euler(grids[idir,:,ic],tt,tau_rep,w_rep)
        grids[idir,:,ic] = v

plt.subplot(2,5,(2,3))
plt.plot(tt,grids2[30,:,0],'r-')
plt.plot(tt,grids[30,:,0],'r--')
plt.plot(tt,grids2[60,:,0],'b-')
plt.plot(tt,grids[60,:,0],'b--')
plt.xlabel('Time (s)', fontsize=fs)
#plt.ylabel('Firing rate (spk/s)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
plt.xlim([0,17])

ax2 = plt.subplot(2,5,(4,5))
meanfr = np.sum(np.sum(grids,axis=1)/bins,axis=1)
plt.plot(np.linspace(0,360,phbins),meanfr,'k')
plt.xticks([0,60,120,180,240,300,360],[])
plt.yticks(fontsize=fs)
plt.ylabel('Total firing rate (spk/s)',fontsize=fs)
ax3 = inset_axes(ax2, width='20%', height='30%', loc=4)
plt.plot([0,0],[-1,1],'k')
plt.plot([-1,1],[0,0],'k')
plt.plot([-1/np.sqrt(2),1/np.sqrt(2)],[-1/np.sqrt(2),1/np.sqrt(2)],'k')
plt.plot([-1/np.sqrt(2),1/np.sqrt(2)],[1/np.sqrt(2),-1/np.sqrt(2)],'k')
ax3.set_aspect('equal')
plt.xticks([0])
plt.yticks([0])
plt.axis([-1.25,1.25,-1.25,1.25])
ax3.xaxis.set_ticks_position("top")
fr2 = np.tile(meanfr,3) # tile to account for periodicity
power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
ff = np.linspace(0,1./(2.), 3*phbins//2)
gr60 = power[np.argmin(abs(ff-1./60*360/phbins))] # pick index which is closest to 60deg
plt.text(0.51,0.1,'H = '+str(np.round(gr60,1)),fontsize=fs,transform=ax2.transAxes)

ax4 = plt.subplot(2,5,(9,10))
phbins = 360
tmax = 3600
dt = 0.1
trajec = traj(dt,tmax,sp=10,dphi = 5)
meanfr = gridpop(oxr,oyr,phbins,trajec,repsuppcell=True,tau_rep=5.,w_rep=1.)
plt.plot(np.linspace(0,360,phbins),meanfr,'k')
plt.xlabel('Movement direction (deg)',fontsize=fs)
plt.ylabel('Total firing rate (spk/s)',fontsize=fs)
plt.xticks([0,60,120,180,240,300,360],fontsize=fs)
plt.yticks(fontsize=fs)
ax5 = inset_axes(ax4, width='20%', height='30%', loc=4)
plt.plot(trajec[0][:100],trajec[1][:100],'k')
ax5.set_aspect('equal')
plt.xticks([])
plt.yticks([])
fr2 = np.tile(meanfr,3) # tile to account for periodicity
power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
ff = np.linspace(0,1./(2.), 3*phbins//2)
gr60 = power[np.argmin(abs(ff-1./60*360/phbins))] # pick index which is closest to 60deg
plt.text(0.51,0.9,'H = '+str(np.round(gr60,1)),fontsize=fs,transform=ax4.transAxes)

plt.subplot(2,5,6)
plt.plot(taus,gr60s_tau,'k-')
#plt.plot(taus,control60*np.ones(np.shape(taus)),'k--')
plt.xlabel('Adaptation time constant (s)', fontsize=fs)
plt.ylabel('Hexasymmetry', fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
#plt.legend(['Repetition suppression', 'No repetition suppression'], fontsize=fs)

plt.subplot(2,5,7)
plt.plot(ws,gr60s_ws,'k-')
plt.xlabel('Adaptation strength', fontsize=fs)
plt.ylabel('Hexasymmetry', fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)

plt.tight_layout()

#%%
# phase dependence of starting point for star-like runs?
N = 100
Amax = 20
tau_rep = 5.
w_rep = 1.
speed = 10
bins = 100
rmax = 300
grsc = 30
ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
ox = ox.reshape(1,-1)[0]
oy = oy.reshape(1,-1)[0]
oxr, oyr = convert_to_rhombus(ox,oy)
r,phi,indoff = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins), np.arange(len(ox)))

binx = 41
xoff, yoff = np.meshgrid(np.linspace(0,grsc,binx,endpoint=False), np.linspace(0,grsc,binx,endpoint=False))
xoffr_s,yoffr_s = convert_to_rhombus(xoff,yoff)
xoffr,yoffr = np.reshape(xoffr_s,(1,-1))[0], np.reshape(yoffr_s,(1,-1))[0]
gr60_offs = np.zeros(binx**2)
ph60_offs = np.zeros(binx**2)
for i in range(len(xoffr)):
    print(i)
    X,Y = r*np.cos(phi*np.pi/180)+xoffr[i], r*np.sin(phi*np.pi/180)+yoffr[i]
    grids = Amax * grid(X,Y,grsc=grsc,offs=(oxr,oyr))[:,:,0,:]
    #grids2 = grids.copy()
    tt = np.linspace(0,rmax/speed,bins)
    for idir in range(phbins):
        for ic in range(N):
            v = adap_euler(grids[idir,:,ic],tt,tau_rep,w_rep)
            grids[idir,:,ic] = v
    meanfr = np.sum(np.sum(grids,axis=1)/bins,axis=1)
    fr2 = np.tile(meanfr,3) # tile to account for periodicity
    power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
    ff = np.linspace(0,1./(2.), 3*phbins//2)
    gr60_offs[i] = power[np.argmin(abs(ff-1./60*360/phbins))]
    ph = np.angle(np.fft.fft(fr2))[:len(fr2)//2]
    ph60_offs[i] = ph[np.argmin(abs(ff-1./60*360/phbins))]/2/np.pi*60
gr60_offs = np.reshape(gr60_offs,(binx,binx))
ph60_offs = np.reshape(ph60_offs,(binx,binx))

plt.figure(figsize=(16,8))
ax1 = plt.subplot(1,2,1)
plt.pcolor(xoffr_s,yoffr_s,gr60_offs,vmin=0,vmax=80)
plt.colorbar()
ax1.set_aspect('equal')
ax2 = plt.subplot(1,2,2)
plt.pcolor(xoffr_s,yoffr_s,ph60_offs,cmap='twilight_shifted',vmin=-30,vmax=30)
plt.colorbar()
ax2.set_aspect('equal')

#%% piecewise linear walk and repetition suppression
N = 1e2
phbins = 360
tau_rep = 5.
w_rep = 1.
ox,oy = np.meshgrid(np.linspace(0,1,int(np.sqrt(N)),endpoint=False), np.linspace(0,1,int(np.sqrt(N)),endpoint=False))
ox = ox.reshape(1,-1)[0]
oy = oy.reshape(1,-1)[0]
oxr,oyr = convert_to_rhombus(ox,oy)
tmax = 36000
dt = 9
trajec = traj(dt, tmax, sp=10, dphi=5, pwlin=True)
meanfr = gridpop(oxr, oyr, phbins, trajec, repsuppcell=True, tau_rep=tau_rep, w_rep=w_rep)
plt.figure()
plt.plot(np.linspace(0,360,phbins),meanfr,'k')
fr2 = np.tile(meanfr,3) # tile to account for periodicity
power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
ff = np.linspace(0,1./(2.), 3*phbins//2)
print(power[np.argmin(abs(ff-1./60*360/phbins))])

#%% repetition suppression - 10000 cells
N = 1e4
phbins = 360
w_rep = 1.
taus = np.linspace(0.1,10,100)
gr60s_tau = np.zeros(np.shape(taus))
for it, tau in enumerate(taus):
    print(it)
    gr60s_tau[it], _, _, _ = gridpop_meanfr(rmax = 300, mode = 'uniform', path = 'linear', N = 100, conj=False, repsuppcell = True, tau_rep = tau, w_rep = 1., speed = 10., plotres = False)

control60, _, _, _ = gridpop_meanfr(rmax = 300, mode = 'uniform', path='linear', N = 100, conj=False, repsuppcell = False, speed = 10. , plotres = False)

plt.figure()
plt.plot(taus,gr60s_tau)
plt.plot(taus,control60*np.ones(np.shape(taus)),'k--')
plt.xlabel('Adaptation time constant (s)')
plt.ylabel('Hexasymmetry')
plt.legend(['Repetition suppression', 'No repetition suppression'])
#title('1 grid cell, phase (0.4,0.3)')
plt.title("10000 grid cells, phases uniformly distributed")



#%% repetition suppression
ws = np.linspace(0,100,20)
gr60s_ws = np.zeros(np.shape(ws))
for iw, w_rep in enumerate(ws):
    print(iw)
    gr60s_ws[iw], _, _, _ = gridpop_meanfr(rmax = 300, mode = 'uniform', path='linear', N = N, conj=False, repsuppcell = True, tau_rep = 5., w_rep = w_rep, speed = 10., plotres = False)

control60_ws, _, _, _ = gridpop_meanfr(rmax = 300, mode = 'uniform', path='linear', N = N, conj=False, repsuppcell = False, speed = 10. , plotres = False)

plt.figure()
plt.plot(ws,gr60s_ws)
plt.plot(ws,control60_ws*np.ones(np.shape(ws)),'k--')
plt.xlabel('Adaptation strength')
plt.ylabel('Hexasymmetry')
plt.legend(['Repetition suppression', 'No repetition suppression'])
#title('1 grid cell, phase (0.4,0.3)')
plt.title("10000 grid cells, phases uniformly distributed")

#%% repetition suppression
speeds = np.linspace(0.01,4.,100)
gr60s = np.zeros(np.shape(speeds))
for isp, speed in enumerate(speeds):
    print(isp)
    gr60s[isp] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 36, conj=False, repsuppcell = True, tau_rep = 3.5, w_rep = 50, speed = speed, plotres = False)

control60 = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 36, conj=False, repsuppcell = False, speed = speed, plotres = False)

plt.figure()
plt.plot(speeds,gr60s)
plt.plot(speeds,control60*np.ones(np.shape(speeds)),'k--')
plt.xlabel('Speed of the animal (grid scales/s)')
plt.ylabel('Hexasymmetry')
plt.legend(['Repetition suppression', 'No repetition suppression'])
plt.title('36 grid cells, uniform')
#title("100 grid cells, phases uniformly distributed")


#%% repetition suppression at the population level
taus = np.logspace(-1,1.5,25)
gr60s = np.zeros(np.shape(taus))
for it, tau in enumerate(taus):
    print(it)
    gr60s[it] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 36, conj=False, repsuppcell = False, repsupppop=True, tau_rep = tau, w_rep = 5., speed = 1., plotres = False)

control60 = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 36, conj=False, repsuppcell = False, repsupppop=False, speed = 1. , plotres = False)

plt.figure()
plt.plot(taus,gr60s)
plt.plot(taus,control60*np.ones(np.shape(taus)),'k--')
plt.xlabel('Adaptation time constant (s)')
plt.ylabel('Hexasymmetry')
plt.legend(['Repetition suppression', 'No repetition suppression'])
#title('1 grid cell, phase (0.4,0.3)')
plt.title("36 grid cells, phases uniformly distributed")

#%%
speeds = np.linspace(0.01,4.,100)
gr60s = np.zeros(np.shape(speeds))
gr60s9 = np.zeros(np.shape(speeds))
gr60s25 = np.zeros(np.shape(speeds))
gr60s64 = np.zeros(np.shape(speeds))
gr60s100 = np.zeros(np.shape(speeds))
for isp, speed in enumerate(speeds):
    print(isp)
    gr60s[isp] = gridpop_meanfr(rmax = 3, mode = '', N = 1, meanoff = (0.4,0.3), conj=False, repsuppcell = True, tau_rep = 3.5, w_rep = 50, speed = speed, plotres = False)
    gr60s9[isp] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 9, conj=False, repsuppcell = True, tau_rep = 3.5, w_rep = 50, speed = speed, plotres = False)
    gr60s25[isp] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 25, conj=False, repsuppcell = True, tau_rep = 3.5, w_rep = 50, speed = speed, plotres = False)
    gr60s64[isp] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 64, conj=False, repsuppcell = True, tau_rep = 3.5, w_rep = 50, speed = speed, plotres = False)
    gr60s100[isp] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 100, conj=False, repsuppcell = True, tau_rep = 3.5, w_rep = 50, speed = speed, plotres = False)

control60 = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 100, conj=False, repsuppcell = False, speed = speed, plotres = False)

plt.figure()
plt.plot(speeds,gr60s)
plt.plot(speeds,gr60s9)
plt.plot(speeds,gr60s25)
plt.plot(speeds,gr60s64)
plt.plot(speeds,gr60s100)
plt.plot(speeds,control60*np.ones(np.shape(speeds)),'k--')
plt.xlabel('Speed of the animal (grid scales/s)')
plt.ylabel('Hexasymmetry')
plt.legend(['Repetition suppression - 1 cell', 'Repetition suppression - 9 cells','Repetition suppression - 25 cells','Repetition suppression - 64 cells','Repetition suppression - 100 cells', 'No repetition suppression - 100 cells'])


#%% basic scenario to compare the three hypotheses
reps = 50
grid_clust = np.zeros(reps)
grid_conj1 = np.zeros(reps)
grid_conj2 = np.zeros(reps)
grid_rep = np.zeros(reps)
grid_fluc = np.zeros(reps)
grid_clust_rel = np.zeros(reps)
grid_conj1_rel = np.zeros(reps)
grid_conj2_rel = np.zeros(reps)
grid_rep_rel = np.zeros(reps)
grid_fluc_rel = np.zeros(reps)
for i in range(reps):
    print(i)
    grid_clust[i], grid_clust_rel[i] = gridpop_meanfr(rmax = -3, mode = 'clustvm', kappacl = 10, meanoff=(0.4,0.3), N = 100, path = '', dphi = 10./180*np.pi, speed = 10, conj=False, repsuppcell = False, plotres = False)
    grid_conj1[i], grid_conj1_rel[i] = gridpop_meanfr(rmax = -3, mode = 'uniform', N = 100, path = '', dphi = 10./180*np.pi, speed = 10, conj=True, propconj = 0.66, kappa = 3., jitter = 10., repsuppcell = False, plotres = False)
    grid_conj2[i], grid_conj2_rel[i] = gridpop_meanfr(rmax = -3, mode = 'uniform', N = 100, path = '', dphi = 10./180*np.pi, speed = 10, conj=True, propconj = 1., kappa = 50., jitter = 0., repsuppcell = False, plotres = False)
    grid_rep[i], grid_rep_rel[i] = gridpop_meanfr(rmax = -3, mode = 'uniform', N = 100, path = '', dphi = 10./180*np.pi, speed = 10, conj=False, repsuppcell = True, tau_rep = 3.5, w_rep = 50, plotres = False)
    grid_fluc[i], grid_fluc_rel[i] = gridpop_meanfr(rmax = -3, mode = 'uniform', N = 100, path = '', dphi = 10./180*np.pi, speed = 10, conj=False, repsuppcell = False, plotres = False)

plt.figure()
plt.yscale('log')
plt.violinplot(np.vstack([grid_fluc,grid_conj2, grid_conj1, grid_clust, grid_rep]).T,showmedians=True)
plt.ylabel('Hexasymmetry')
plt.xticks([1,2,3,4,5],['Fluct','Conj best', 'Conj Sargolini', 'Clustering', 'Rep supp'])

plt.figure()
plt.violinplot(np.vstack([grid_conj2_rel, grid_conj1_rel, grid_clust_rel, grid_rep_rel, grid_fluc_rel]).T,showmedians=True)
plt.ylabel('Hexasymmetry (normalized)')
plt.xticks([1,2,3,4,5],['Conj best', 'Conj Sargolini', 'Clustering', 'Rep supp','Fluct'])


# wie viele rhombi in einem modul? voxel ueberlapp mit modul ... rhombus nur angeschnitten?

#%% more complex trajectories
# influence of tortuosity
dangs = np.linspace(0.01,1,100)
gr60s_dphi = np.zeros(np.shape(dangs))
rep = 100
for id,dang in enumerate(dangs):
    print(id)
    count = 0
    griddi = np.zeros(rep)
    while count<rep:
        print(count)
        griddi[count] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 25, path = '', dphi = dang, speed = 10, conj=False, repsuppcell = False, plotres = False)
        count += 1
    gr60s_dphi[id] = np.mean(griddi[~np.isnan(griddi)])

plt.figure()
plt.plot(180/np.pi*dangs,gr60s_dphi)
plt.xlabel('Tortuosity (deg)')
plt.ylabel('Hexasymmetry')
plt.title("25 cells, uniform")


sps = np.linspace(1,100,100)
gr60s_sp = np.zeros(np.shape(sps))
rep = 100
for isp,sp in enumerate(sps):
    print(id)
    count = 0
    griddi = np.zeros(rep)
    while count<rep:
        print(count)
        griddi[count] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 25, path = '', dphi = 0.1, speed = sp, conj=False, repsuppcell = False, plotres = False)
        count += 1
    gr60s_sp[id] = np.mean(griddi[~np.isnan(griddi)])

plt.figure()
plt.plot(sps,gr60s_sp)
plt.xlabel('Speed (m/s)')
plt.ylabel('Hexasymmetry')

"""
#%% check the different hypotheses with complex trajectories
# 1, conjunctive cells
dangs = np.linspace(0.01,1,10)
gr60s_dphi = np.zeros(np.shape(dangs))
rep = 1
for id,dang in enumerate(dangs):
    print(id)
    count = 0
    griddi = np.zeros(rep)
    while count<rep:
        print(count)
        griddi[count] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 25, path = '', dphi = dang, speed = 10, conj=True, repsuppcell = False, plotres = False)
        count += 1
    gr60s_dphi[id] = np.mean(griddi[~np.isnan(griddi)])

plt.figure()
plt.plot(180/np.pi*dangs,gr60s_dphi)
plt.xlabel('Tortuosity (deg)')
plt.ylabel('Hexasymmetry')
plt.title("25 cells, uniform, conj")

#%%
# 2, clustering
dangs = np.linspace(0.01,1,10)
gr60s_dphi = np.zeros(np.shape(dangs))
rep = 1
for id,dang in enumerate(dangs):
    print(id)
    count = 0
    griddi = np.zeros(rep)
    while count<rep:
        print(count)
        griddi[count] = gridpop_meanfr(rmax = 3, mode = 'clustvm', N = 25, path = '', dphi = dang, speed = 10, conj=False, repsuppcell = False, plotres = False)
        count += 1
    gr60s_dphi[id] = np.mean(griddi[~np.isnan(griddi)])

plt.figure()
plt.plot(180/np.pi*dangs,gr60s_dphi)
plt.xlabel('Tortuosity (deg)')
plt.ylabel('Hexasymmetry')
plt.title("25 cells, clustered")

#%%
# 3, repetition suppression
dangs = np.linspace(0.01,1,100)
gr60s_dphi = np.zeros(np.shape(dangs))
rep = 100
for id,dang in enumerate(dangs):
    print(id)
    count = 0
    griddi = np.zeros(rep)
    while count<rep:
        print(count)
        griddi[count] = gridpop_meanfr(rmax = 3, mode = 'uniform', N = 25, path = '', dphi = dang, speed = 10, conj=False, repsuppcell = True, plotres = False)
        count += 1
    gr60s_dphi[id] = np.mean(griddi[~np.isnan(griddi)])

plt.figure()
plt.plot(180/np.pi*dangs,gr60s_dphi)
plt.xlabel('Tortuosity (deg)')
plt.ylabel('Hexasymmetry')
plt.title("25 cells, clustered")
"""

#%% plot tortuosity data from file written remotely
import csv
csvfile = "/home/eric/Desktop/gridBOLD/Tortuosity_3hypothesis.csv"
with open(csvfile) as csvfile:
    creader = csv.reader(csvfile)
    dat = np.array([line for line in creader])
    res = dat[1:]
    data = np.array(res[...,:], dtype = 'float')

plt.figure()
plt.yscale('log')
tort = data[:,0]
plt.plot(tort,data[:,1])
plt.fill_between(tort, data[:,1]-data[:,2], data[:,1]+data[:,2],alpha=0.5)
plt.plot(tort,data[:,3])
plt.fill_between(tort, data[:,3]-data[:,4], data[:,3]+data[:,4],alpha=0.5)
plt.plot(tort,data[:,5])
plt.fill_between(tort, data[:,5]-data[:,6], data[:,5]+data[:,6],alpha=0.5)
plt.legend(['Conjunctive','Cluster','Adaptation'],loc=[0.7,0.65])
plt.xlabel('Tortuosity (deg)')
plt.ylabel('Hexasymmetry')

#%% mit dem Wert der Tortuosity aus den Daten vergleichen
tort_sim = np.linspace(0,1,20)
tort_data = np.zeros(np.shape(tort_sim))
for it, torti in enumerate(tort_sim):
    x,y,direc,t = traj(0.001,1200,sp=2,dphi = torti, bound = 3)
    tort_data[it] = np.median(bm.circ_diff(direc[:-1],direc[1:]))*np.mean(np.diff(t))

plt.figure()
plt.plot(180/np.pi*tort_sim, tort_data,'.-')

# use rat trajectory for simulation? maybe later part if runs along boundary are too annoying

#%%
rep = 10

plt.figure()
i = 0
while i<10:
    x,y,dd,t = traj(0.001,10,sp=2,dphi = 0.1, bound = 3)
    plt.plot(x,y)
    i += 1

plt.figure()
i = 0
while i<10:
    x,y,dd,t = traj(1.,10,sp=2,dphi = 0.1, bound = 3)
    plt.plot(x,y)
    i += 1
