#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 17:06:10 2021

@author: eric
"""
import numpy as np
import matplotlib
matplotlib.use('Agg')
# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from scipy import special,stats,interpolate
import matplotlib.patches as mpatches
from matplotlib.ticker import ScalarFormatter

#from scipy.integrate import odeint

def grid(X, Y, grsc=30, angle=0, offs=np.array([0,0]), rect=False):
    sc = 1/grsc * 2./np.sqrt(3)
    if rect:
        rec = lambda x: np.where(x>0, x, 0)
        if np.size(offs) == 2:
            return rec(np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos(angle*np.pi/180)*(Y-grsc*offs[1]))) * rec(np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y-grsc*offs[1]))) * rec(np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y-grsc*offs[1])))
        else:
            assert len(offs[0]) == len(offs[1]), "ox and oy must have same length"
            return rec(
                np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos(angle*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   rec(
                np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   rec(
                np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))
    else:
        if np.size(offs) == 2:
            return (1 + np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos(angle*np.pi/180)*(Y-grsc*offs[1]))) * (1 + np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y-grsc*offs[1]))) * (1 + np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X-grsc*offs[0])+2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y-grsc*offs[1])))
        else:
            assert len(offs[0]) == len(offs[1]), "ox and oy must have same length"
            return (1 +
                np.cos(2*np.pi*sc*np.sin(angle*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos(angle*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   (1 +
                np.cos(2*np.pi*sc*np.sin((angle+60)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+60)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))*\
                   (1 +
                np.cos(2*np.pi*sc*np.sin((angle+120)*np.pi/180)*(X[:,:,None]-(grsc*offs[0])[None,None,:])+
                       2*np.pi*sc*np.cos((angle+120)*np.pi/180)*(Y[:,:,None]-(grsc*offs[1])[None,None,:])))

def convert_to_rhombus(x,y):
    return x+0.5*y,np.sqrt(3)/2*y

def convert_to_square(xr,yr):
    return xr-1/np.sqrt(3)*yr,2/np.sqrt(3)*yr

def map_to_rhombus(x,y,grsc=30):
    xr,yr = x-1/np.sqrt(3)*y,2/np.sqrt(3)*y
    xr, yr = np.mod(xr,grsc),np.mod(yr,grsc)
    xmod,ymod = xr+0.5*yr,np.sqrt(3)/2*yr
    return xmod,ymod

def traj(dt,tmax,sp=2,dphi = 0.5, bound = 3,pwlin=False):
    t = np.arange(0,tmax+dt,dt)
    x,y,direc = np.zeros((3,len(t)))
    olddir = np.pi/4
    for i in range(1,len(t)):
        sigma_theta = dphi;
        newdir = olddir + sigma_theta*np.sqrt(dt)*np.random.randn()
        dx = sp*dt*np.cos(newdir)
        dy = sp*dt*np.sin(newdir)
        x[i] = x[i-1] + dx
        y[i] = y[i-1] + dy
        olddir = np.arctan2(dy,dx)
        direc[i] = olddir

    if pwlin:
        dt2 = 0.1
        t2 = np.arange(0,t[-1]+dt2,dt2)
        fx = interpolate.interp1d(t, x)
        fy = interpolate.interp1d(t, y)
        x2 = fx(t2)
        y2 = fy(t2)
        direc2 = np.concatenate((np.zeros(1),np.repeat(direc[1:],int(dt/dt2))))
        return x2,y2,direc2,t2
    else:
        return x,y,direc,t

def adap_euler(s,tt,tau,w):
    '''
    Euler-solve a simple adaptation model for the repetition-suppression hypothesis
    '''
    v = np.zeros(np.shape(tt))
    a = np.zeros(np.shape(tt))
    v[0] = s[0]
    a[0] = 0
    dt = np.mean(np.diff(tt))
    for it in range(len(v)-1):
        #dv = (-w*a[it]+s[it]-v[it])/0.01*dt
        v[it+1] = max(s[it] - w*a[it],0)
        da = (v[it]-a[it])/tau*dt
        a[it+1] = a[it] + da
    """
    plt.figure()
    plt.plot(tt,s,tt,v)
    plt.legend(['No rep supp','Rep supp'])
    plt.xlabel('Time (s)')
    plt.ylabel('Normalized firing rate')
    """
    return np.where(v>0,v,0) #rectify result

def gridpop(ox, oy, phbins, trajec, Amax = 20, grsc = 30, repsuppcell = False, repsupppop = False, tau_rep = 3.5, w_rep = 50, conj = False, propconj = 0.66, kappa = 50, jitter=10,shufforient=False):
    """
    simulate a population of grid cells for a complex 2d trajectory
    """
    if type(ox)==int or type(ox)==float:
        N = 1
    else:
        N = len(ox)
    x,y,direc,t = trajec
    oxr,oyr = convert_to_rhombus(ox,oy)
    oxr,oyr = np.asarray(oxr), np.asarray(oyr)
    fr = np.zeros(np.shape(x))
    for n in range(N):
        if shufforient:
            ang = 360*np.random.rand()
            if repsuppcell:
                fr += adap_euler(Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n])), t, tau_rep, w_rep)
            elif conj: # not used together at this stage
                if n<= int(propconj*N):
                    mu = np.mod(ang + jitter*np.random.randn(),360) #np.mod(np.random.randint(0,6,1) * 60 + jitter*np.random.randn(), 360)
                    fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n])) * np.exp(kappa*np.cos(direc-np.pi/180.*mu))/(2*np.pi*special.i0(kappa))
                    # scheint nicht ok, immer noch normiert?
                else:
                    fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n]))
            else:
                fr += Amax*grid(x,y,grsc=grsc,angle=ang,offs=(oxr[n],oyr[n]))

        else:
            if repsuppcell:
                fr += adap_euler(Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n])), t, tau_rep, w_rep)
            elif conj: # not used together at this stage
                if n<= int(propconj*N):
                    mu = np.mod(np.random.randint(0,6,1) * 60 + jitter*np.random.randn(), 360)
                    fr += Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n])) * np.exp(kappa*np.cos(direc-np.pi/180.*mu))/(2*np.pi*special.i0(kappa))
                    # scheint nicht ok, immer noch normiert?
                else:
                    fr += Amax*grid(x,y,grsc=grsc,offs=(oxr[n],oyr[n]))
            else:
                fr += Amax*grid(x,y,grsc=grsc,angle=0, offs=(oxr[n],oyr[n]))

    if repsupppop:
        fr = adap_euler(fr, t, tau_rep, w_rep)

    if False:
        plt.figure()
        plt.scatter(x,y,s = 20, c = fr, edgecolors='face')

    #direc = np.mod(direc,2*np.pi)
    direc_binned = np.linspace(-np.pi,np.pi,phbins+1)
    fr_mean = np.zeros(phbins)
    for id in range(len(direc_binned)-1):
            ind = (direc>=direc_binned[id])*(direc<direc_binned[id+1])
            fr_mean[id] = np.sum(fr[ind])*phbins/len(fr)

    if False:
        plt.figure()
        plt.plot(180./np.pi*(direc_binned[:-1]+direc_binned[1:])/2., fr_mean)
        plt.xlabel('Running direction (deg)')
        plt.ylabel('Firing rate (a.u.)')

    return fr_mean, np.abs(np.sum(fr * np.exp(-6j*direc)))/np.size(direc), np.abs(np.sum(np.exp(-6j*direc)))/np.size(direc) #/np.abs(np.sum(np.exp(-6j*direc)))


# Figure about clustering hypothesis
# rhombus with clustered phases,
from scipy.stats import vonmises
from mpl_toolkits.axes_grid1 import make_axes_locatable

fs= 16
kappacl = 1
meanoff = (0.,0.)
N = 1e2
ox, oy = np.random.vonmises(2*np.pi*(meanoff[0]-0.5), kappacl, int(N))/2./np.pi + 0.5, np.random.vonmises(2*np.pi*(meanoff[1]-0.5), kappacl, int(N))/2./np.pi + 0.5
fig = plt.figure(figsize=(20,10))

#ax = plt.subplot(4,5,1)
ax3 = plt.subplot(5, 4, (1, 2))
rmax = 300
bins = 1000
phbins = 360
Amax = 20
r,phi = np.meshgrid(np.linspace(0,rmax,bins), np.linspace(0,360,phbins))
X,Y = r*np.cos(phi*np.pi/180), r*np.sin(phi*np.pi/180)
oxr,oyr = convert_to_rhombus(ox,oy)
grids = Amax * grid(X,Y,grsc=30,offs=(oxr,oyr))
gr2 = np.sum(grids, axis=2)
meanfr1 = np.sum(np.sum(grids,axis=1),axis=1)/bins
gr60_star = np.abs(np.sum(gr2 * np.exp(-6j*phi)))/np.size(phi)
gr60_path_star = np.abs(np.sum(np.exp(-6j*phi)))/np.size(phi)
plt.plot(np.linspace(0,360,phbins),meanfr1,'k')
#plt.xlabel('Movement direction (deg)',fontsize=fs)
#plt.ylabel('Total firing \nrate (spk/s)',fontsize=fs)
plt.xticks([0,60,120,180,240,300,360],fontsize=fs)
plt.yticks(fontsize=fs)
plt.ylim([0,1.5*max(meanfr1)])
plt.text(0.2,0.8,'kappa = 10',fontsize=fs,transform=ax3.transAxes)

from mpl_toolkits.axes_grid1.inset_locator import inset_axes

ax41 = inset_axes(ax3, width='15%', height='40%', loc=2)
plt.scatter(ox+0.5*oy,np.sqrt(3)/2*oy,s=5,c='k')
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
plt.xlim([-0.05,1.5])
plt.ylim([-0.05,1])
plt.xticks([])
plt.yticks([])
#plt.xlabel('Grid phases',fontsize=fs)
ax41.axis("off")
#plt.text(0.1,-0.2,'kappa = 10',fontsize=fs)
ax41.set_aspect('equal')

ax4 = inset_axes(ax3, width='20%', height='40%', loc=1)
plt.plot([0,0],[-1,1],'k')
plt.plot([-1,1],[0,0],'k')
plt.plot([-1/np.sqrt(2),1/np.sqrt(2)],[-1/np.sqrt(2),1/np.sqrt(2)],'k')
plt.plot([-1/np.sqrt(2),1/np.sqrt(2)],[1/np.sqrt(2),-1/np.sqrt(2)],'k')
ax4.set_aspect('equal')
plt.xticks([0])
plt.yticks([0])
ax4.yaxis.set_ticks_position("right")
plt.axis([-1.25,1.25,-1.25,1.25])
# fr2 = np.tile(meanfr1,3) # tile to account for periodicity
# power = 1./len(fr2) * abs(np.fft.fft(fr2))[:len(fr2)//2]
# ff = np.linspace(0,1./(2.), 3*phbins//2)
# gr60 = power[np.argmin(abs(ff-1./60*360/phbins))] # pick index which is closest to 60deg
grs_star = np.array([])
grs_path_star = np.array([])
i = 0
while i < 100:
    print(i)
    ox, oy = np.random.vonmises(2*np.pi*(meanoff[0]-0.5), kappacl, int(N))/2./np.pi + 0.5, np.random.vonmises(2*np.pi*(meanoff[1]-0.5), kappacl, int(N))/2./np.pi + 0.5
    oxr,oyr = convert_to_rhombus(ox,oy)
    grids = Amax * grid(X,Y,grsc=30,offs=(oxr,oyr))
    gr2 = np.sum(grids, axis=2)
    meanfr1 = np.sum(np.sum(grids,axis=1),axis=1)/bins
    gr60_star = np.abs(np.sum(gr2 * np.exp(-6j*phi)))/np.size(phi)
    gr60_path_star = np.abs(np.sum(np.exp(-6j*phi)))/np.size(phi)
    grs_star = np.hstack((grs_star, gr60_star))
    grs_path_star = np.hstack((grs_path_star, gr60_path_star))
    i += 1

plt.text(0.5,0.8,'H = '+str(np.round(np.median(grs_star), 1)),fontsize=fs,transform=ax3.transAxes)

ax5 = plt.subplot(5, 4, (9, 10))
phbins = 360
Amax = 20
tmax = 3600
dt = 0.1
part = 500
trajec = traj(dt,tmax,sp=8,dphi = 2)
meanfr2, gr60_rw, gr60_path_rw = gridpop(oxr,oyr,phbins,trajec)
plt.plot(np.linspace(0,360,phbins),meanfr2,'k')
plt.xlabel('Movement direction (deg)',fontsize=fs)
plt.ylabel('Total firing \nrate (spk/s)',fontsize=fs)
plt.xticks([0,60,120,180,240,300,360],fontsize=fs)
plt.yticks(fontsize=fs)
plt.ylim([0,1.4*max(meanfr2)])

ax6 = inset_axes(ax5, width='15%', height='40%', loc=1)
plt.plot(trajec[0][:part],trajec[1][:part],'k')
ax6.set_aspect('equal')
plt.xticks([])
plt.yticks([])

grs_rw = np.array([])
grs_path_rw = np.array([])
i = 0
while i < 100:
    print(i)
    trajec = traj(dt, tmax, sp=8, dphi=2)
    _, gr60_rw, gr60_path_rw = gridpop(oxr, oyr, phbins, trajec)
    grs_rw = np.hstack((grs_rw, gr60_rw))
    grs_path_rw = np.hstack((grs_path_rw, gr60_path_rw))
    i += 1

plt.text(0.5,0.8,'H = '+str(np.round(np.median(grs_rw), 1)),fontsize=fs,transform=ax5.transAxes)

grsc = 30
ax7 = plt.subplot(5, 4, 11)
xtr,ytr,dirtr = trajec[0][:part], trajec[1][:part], trajec[2][:part]
mingridx = min(min(xtr),0)
maxgridx = max(max(xtr),1.5*grsc)
mingridy = min(min(ytr),0)
maxgridy = max(max(ytr),np.sqrt(3)/2*grsc)
X,Y = np.meshgrid(np.linspace(mingridx-10,maxgridx+10,1000),np.linspace(mingridy-10,maxgridy+10,1000))
gr = grid(X,Y,grsc=30, angle=0, offs=np.array([0,0]))
ax7.pcolor(X,Y,gr)
ax7.set_aspect('equal')
plt.xlabel('x',fontsize=fs)
plt.ylabel('y',fontsize=fs)
plt.xticks([])
plt.yticks([])
ax7.plot(xtr,ytr,'w')
tol = 5 * np.pi/180
ax7.scatter(xtr[abs(dirtr)<tol],ytr[abs(dirtr)<tol],c='r',zorder=5)
plt.plot([0,grsc],[0,0],'k--')
plt.plot([0,0.5*grsc],[0,np.sqrt(3)/2*grsc],'k--')
plt.plot([0.5*grsc,1.5*grsc],[np.sqrt(3)/2*grsc,np.sqrt(3)/2*grsc],'k--')
plt.plot([1*grsc,1.5*grsc],[0,np.sqrt(3)/2*grsc],'k--')
ax7.set_aspect('equal')

ax_pw = plt.subplot(5, 4, (5, 6))
phbins = 360
Amax = 20
tmax = 72000
dt = 9
trajec = traj(dt, tmax, sp=8, dphi=2, pwlin=True)
meanfr3, gr60_pl, gr60_path_pl = gridpop(oxr, oyr, phbins, trajec)
plt.plot(np.linspace(0, 360, phbins), meanfr3, 'k')
plt.xlabel('Movement direction (deg)', fontsize=fs)
plt.ylabel('Total firing \nrate (spk/s)', fontsize=fs)
plt.xticks([0,60,120,180,240,300,360], fontsize=fs)
plt.yticks(fontsize=fs)
plt.ylim([0,1.4*max(meanfr3)])

part = 1000
ax_in = inset_axes(ax_pw, width='15%', height='40%', loc=1)
plt.plot(trajec[0][:part],trajec[1][:part],'k')
ax_in.set_aspect('equal')
plt.xticks([])
plt.yticks([])

grs_pl = np.array([])
grs_path_pl = np.array([])
i = 0
while i < 100:
    print(i)
    trajec = traj(dt, tmax, sp=8, dphi=2, pwlin=True)
    _, gr60_pl, gr60_path_pl = gridpop(oxr, oyr, phbins, trajec)
    grs_pl = np.hstack((grs_pl, gr60_pl))
    grs_path_pl = np.hstack((grs_path_pl, gr60_path_pl))
    i += 1

plt.text(0.5, 0.8, 'H = '+str(np.round(np.median(grs_pl), 1)),fontsize=fs,transform=ax_pw.transAxes)

ax8 = plt.subplot(5, 4, 12)
allxtr,allytr = trajec[0][abs(trajec[2]<tol)], trajec[1][trajec[2]<tol]
allxr,allyr = convert_to_square(*map_to_rhombus(allxtr,allytr,grsc=grsc))
#ax = plt.gca()
dx = 0.01*grsc
xedge,yedge = np.arange(0,grsc+dx,dx), np.arange(0,grsc+dx,dx)
H,xe,ye = np.histogram2d(allxr,allyr,bins=(xedge,yedge),density=True)
H = H.T
#plt.scatter(ph1c+0.5*ph2c,np.sqrt(3)/2*ph2c,s=1,c='k')
xr,yr = convert_to_rhombus(*np.meshgrid((xe[:-1]+xe[1:])/2,(ye[:-1]+ye[1:])/2))
plt.plot([0,grsc],[0,0],'k--')
plt.plot([0,0.5*grsc],[0,np.sqrt(3)/2*grsc],'k--')
plt.plot([0.5*grsc,1.5*grsc],[np.sqrt(3)/2*grsc,np.sqrt(3)/2*grsc],'k--')
plt.plot([1*grsc,1.5*grsc],[0,np.sqrt(3)/2*grsc],'k--')
#plt.xticks([])
#plt.yticks([])
ax8.axis("off")
ax8.set_aspect('equal')
plt.xlabel('Grid phase',fontsize=fs)
im = ax8.pcolor(xr,yr,H)
divider = make_axes_locatable(ax8)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax,ticks=[H.min(),H.max()])
cb.ax.set_yticklabels(['min', 'max']) #cbar.ax.tick_params(labelsize=fs)
cb.set_label('Density',fontsize=fs)

res = 101
x = np.linspace(0,1,res,endpoint=True)
y = np.linspace(0,1,res,endpoint=True)
X,Y = np.meshgrid(x,y)
Xc,Yc = convert_to_rhombus(X,Y)

gr60s = np.array([])
f = open("phasedependence_gr60.txt", "r")
for i,x in enumerate(f):
    gr60s = np.append(gr60s, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
gr60s = gr60s.reshape((res,res))

ph60s = np.array([])
f = open("phasedependence_ph60.txt", "r")
for i,x in enumerate(f):
    ph60s = np.append(ph60s, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph60s = ph60s.reshape((res,res))

axx = fig.add_subplot(5, 4, 3)
ax = plt.gca()
im = ax.pcolormesh(Xc,Yc,N*gr60s)
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
ax.set_aspect('equal')
ax.axis('off')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax)
cb.set_label('H',fontsize=fs)


fig.add_subplot(5, 4, 4)
ax = plt.gca()
im = ax.pcolormesh(Xc,Yc,ph60s,cmap='twilight_shifted',vmin=-30, vmax=30)
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
ax.set_aspect('equal')
ax.axis('off')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax, ticks=[-30,30])
cb.set_label('Phase of\nhexasymmetry',fontsize=fs)

l = 200
ph1c = np.array([])
f = open("ph1c_gauss.txt", "r")
for i,x in enumerate(f):
    ph1c = np.append(ph1c, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph1c = ph1c.reshape((l,l))

plt.subplot(5, 4, 13)
ax = plt.gca()
im = ax.imshow(ph1c,origin='lower',cmap='twilight_shifted',vmin=0, vmax=1)
plt.xticks(np.linspace(0,200,4),['0','1','2','3'])
plt.yticks(np.linspace(0,200,4),['0','1','2','3'])
#plt.xlabel('x (mm)',fontsize=fs)
#plt.ylabel('y (mm)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax)
#cb.set_label('Grid phase (X)',fontsize=fs)

kernelc = np.array([])
f = open("kernel_gauss.txt", "r")
for i,x in enumerate(f):
    kernelc = np.append(kernelc, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
kernelc = kernelc.reshape((l,l))

plt.subplot(5, 4, 14)
plt.imshow(kernelc[50:150,50:150],origin='lower')
plt.xticks(np.linspace(0,100,4),['0','0.5','1.0','1.5'])
plt.yticks(np.linspace(0,100,4),['0','0.5','1.0','1.5'])
#plt.xlabel('x (mm)')
#plt.ylabel('y (mm)')
#plt.colorbar()

data = np.array([])
f = open("binned_phasedistance.txt", "r")
for i,x in enumerate(f):
    data = np.append(data, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
data = data.reshape((49,6))
binsc,meandiffc,stddiffc,bins_1,meandiff,stddiff = data.T

ax11 = plt.subplot(5, 4, 15)
plt.errorbar(binsc * 1000,meandiffc,stddiffc)
#plt.xlabel('Pairwise anatomical distance (um)',fontsize=fs)
#plt.ylabel('Pairwise phase \ndistance (2D)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
plt.xlim([0,500])
ax11.set_aspect(600)

ph2c = np.array([])
f = open("ph2c_gauss.txt", "r")
for i,x in enumerate(f):
    ph2c = np.append(ph2c, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph2c = ph2c.reshape((l,l))

ax2 = plt.subplot(5, 4, 16)
ax = plt.gca()
dx = 0.01
xedge,yedge = np.arange(0,1+dx,dx), np.arange(0,1+dx,dx)
H,xe,ye = np.histogram2d(np.reshape(ph1c,(1,-1))[0],np.reshape(ph2c,(1,-1))[0],bins=(xedge,yedge),density=True)
H = H.T
#plt.scatter(ph1c+0.5*ph2c,np.sqrt(3)/2*ph2c,s=1,c='k')
xr,yr = convert_to_rhombus(*np.meshgrid((xe[:-1]+xe[1:])/2,(ye[:-1]+ye[1:])/2))
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
#plt.xlim([-0.05,1.5])
#plt.ylim([-0.05,1])
#plt.xticks([])
#plt.yticks([])
ax2.axis("off")
ax2.set_aspect('equal')
plt.text(0.1,-0.2,'kappa = '+ str(np.round(vonmises.fit(2*np.pi*ph1c, fscale=1)[0],3)),fontsize=fs)
plt.xlabel('Grid phase',fontsize=fs)
im = ax.pcolor(xr,yr,H)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0)
cb = plt.colorbar(im, cax=cax, ticks=[H.min(),H.max()])
cb.ax.set_yticklabels(['min', 'max']) #cbar.ax.tick_params(labelsize=fs)
cb.set_label('Density',fontsize=fs)

ph1 = np.array([])
f = open("ph1_grid.txt", "r")
for i,x in enumerate(f):
    ph1 = np.append(ph1, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph1 = ph1.reshape((l,l))

plt.subplot(5, 4, 17)
ax = plt.gca()
im = ax.imshow(ph1,origin='lower',cmap='twilight_shifted',vmin=0, vmax=1)
plt.xticks(np.linspace(0,200,4),['0','1','2','3'])
plt.yticks(np.linspace(0,200,4),['0','1','2','3'])
plt.xlabel('x (mm)',fontsize=fs)
plt.ylabel('y (mm)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax)
cb.set_label('Grid phase (X)',fontsize=fs)

kernel = np.array([])
f = open("kernel_grid.txt", "r")
for i,x in enumerate(f):
    kernel = np.append(kernel, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
kernel = kernel.reshape((l,l))

plt.subplot(5, 4, 18)
plt.imshow(kernel[50:150,50:150],origin='lower')
plt.xticks(np.linspace(0,100,4),['0','0.5','1.0','1.5'])
plt.yticks(np.linspace(0,100,4),['0','0.5','1.0','1.5'])
plt.xlabel('x (mm)',fontsize=fs)
plt.ylabel('y (mm)',fontsize=fs)

ax12 = plt.subplot(5, 4, 19)
plt.errorbar(binsc * 1000,meandiff,stddiff)
plt.xlabel('Pairwise anatomical distance (um)',fontsize=fs)
plt.ylabel('Pairwise phase \ndistance (2D)',fontsize=fs)
plt.xticks(fontsize=fs)
plt.yticks(fontsize=fs)
plt.xlim([0,500])
ax12.set_aspect(600)


ph2 = np.array([])
f = open("ph2_grid.txt", "r")
for i,x in enumerate(f):
    ph2 = np.append(ph2, np.array([float(elem) for elem in x.replace('[','').replace(']','').replace('\n','').split(' ') if elem!='']))
f.close()
ph2 = ph2.reshape((l,l))

ax2 = plt.subplot(5, 4, 20)
ax = plt.gca()
dx = 0.01
xedge,yedge = np.arange(0,1+dx,dx), np.arange(0,1+dx,dx)
H,xe,ye = np.histogram2d(np.reshape(ph1,(1,-1))[0],np.reshape(ph2,(1,-1))[0],bins=(xedge,yedge),density=True)
H = H.T
#plt.scatter(ph1c+0.5*ph2c,np.sqrt(3)/2*ph2c,s=1,c='k')
xr,yr = convert_to_rhombus(*np.meshgrid((xe[:-1]+xe[1:])/2,(ye[:-1]+ye[1:])/2))
plt.plot([0,1],[0,0],'k--')
plt.plot([0,0.5],[0,np.sqrt(3)/2],'k--')
plt.plot([0.5,1.5],[np.sqrt(3)/2,np.sqrt(3)/2],'k--')
plt.plot([1,1.5],[0,np.sqrt(3)/2],'k--')
#plt.xlim([-0.05,1.5])
#plt.ylim([-0.05,1])
#plt.xticks([])
#plt.yticks([])
ax2.axis("off")
ax2.set_aspect('equal')
plt.text(0.1,-0.2,'kappa = '+ str(np.round(vonmises.fit(2*np.pi*ph1, fscale=1)[0],2)),fontsize=fs)
plt.xlabel('Grid phase',fontsize=fs)
im = ax.pcolor(xr,yr,H)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(im, cax=cax, ticks=[H.min(),H.max()])
cb.ax.set_yticklabels(['min', 'max']) #cbar.ax.tick_params(labelsize=fs)
cb.set_label('Density',fontsize=fs)

plt.tight_layout()

plt.savefig('Figure3_clustering.png')
#plt.savefig('Figure3_clustering.eps', dpi=300)
plt.close()
